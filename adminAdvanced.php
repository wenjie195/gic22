<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$loanUidRows = getAdvancedSlip($conn, "WHERE status = 'PENDING' ");
$invoiceDetails = getInvoice($conn);
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Advance To Be Issue | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <?php if ($loanUidRows) {
    ?><h1 class="h1-title h1-before-border shipping-h1">Advance To Be Issue</h1>
      <div class="short-red-border"></div>
      <!-- This is a filter for the table result -->


      <!-- <select class="filter-select clean">
      	<option class="filter-option">Latest Shipping</option>
          <option class="filter-option">Oldest Shipping</option>
      </select> -->

      <!-- End of Filter -->
      <div class="clear"></div>

      <div class="width100 shipping-div2">
          <?php $conn = connDB();?>
              <table class="shipping-table">
                  <thead>
                      <tr>
                          <th class="th">NO.</th>
                          <th class="th">UNIT NO.</th>
                          <th class="th"><?php echo wordwrap("AGENT NAME",10,"</br>\n");?></th>
                          <th class="th">DATE</th>
                          <th class="th">TIME</th>
                          <th class="th">ACTION</th>

                          <!-- <th>INVOICE</th> -->
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                      // {
                          $orderDetails = getAdvancedSlip($conn, "WHERE status = 'PENDING'");
                          if($orderDetails != null)
                          {
                              for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                              {?>
                              <tr>
                                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td"><?php echo $cntAA + 1;?></td>
                                  <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                  <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getAgent(),15,"</br>\n");?></td>
                                  <td class="td"><?php echo date('d/m/Y', strtotime($orderDetails[$cntAA]->getDateCreated())) ?></td>
                                  <td class="td"><?php echo date('H:i a', strtotime($orderDetails[$cntAA]->getDateCreated())) ?></td>

                                  <td class="td">
                                      <form action="advancedSlip.php" method="POST">
                                        <a><button class="clean edit-anc-btn hover1 red-link"  type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Advanced Slip
                                              <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                              <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                          </button></a>
                                      </form>
                                  </td>

                              </tr>
                              <?php
                              }
                          }
                      //}
                      ?>
                  </tbody>
              </table><br>


      </div>

      <!-- <div class="three-btn-container">
      <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
        <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
      <!-- </div> -->
      <?php $conn->close();?><?php
  }else {
    ?><h2 style="text-align: center">No Loan Case Status Completed Found.</h2>
    <h2 style="text-align: center">Make Sure To Completed The Case Status Before Issue Advanced.</h2> <?php
  } ?>


</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>
