<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$username = $_SESSION['username'];

$conn = connDB();

$userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

  <!-- <h1 class="h1-title h1-before-border shipping-h1">Personal Sales (SPA Signed)</h1> -->
  <h2 class="h1-title"> Payroll  (Commission) | <a href="#" class="h1-title">Advance</a> | <a href="#" class="h1-title">Overriding Commission</a> | <a href="#" class="h1-title">Project Leader</a></h2>

  <div class="short-red-border"></div>


  
  <div class="clear"></div>

    <div class="width100 shipping-div2">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th">PROJECT NAME</th>
                        <th class="th">UNIT NO.</th>
                        <th class="th">TOTAL COMM.</th>
                        <th class="th">NO. OF CLAIMS</th>
                        <th class="th">1ST CLAIM (RM)</th>
                        <th class="th">1ST CLAIM ON</th>
                        <!-- <th class="th">2ND CLAIM (RM)</th>
                        <th class="th">2ND CLAIM ON</th>
                        <th class="th">3RD CLAIM (RM)</th>
                        <th class="th">3RD CLAIM ON</th>
                        <th class="th">TOTAL UNCLAIM (RM)</th> -->

                        <!-- <th class="th">BOOKING DATE</th>
                        <th class="th">NETT PRICE</th>
                        <th class="th">AGENT</th>
                        <th class="th">CASE STATUS</th> -->
           
                        <!-- <th class="th"><?php //echo wordwrap("NO.",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("PROJECT NAME",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("UNIT NO.",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("NETT PRICE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("AGENT",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("CASE STATUS",10,"</br>\n");?></th> -->

                    </tr>
                </thead>
                <tbody>
                    <?php
                        $conn = connDB();
                        // $orderDetails = getLoanStatus($conn," WHERE upline2 = '$userUsername' ");
                        $invoiceDetails = getInvoice($conn);
                        if($invoiceDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($invoiceDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <td class="td"><?php echo ($cntAA+1)?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getPurchaserName();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getPurchaserName();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getClaimsStatus();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getFinalAmount();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getClaimsDate();?></td>
                            </tr>
                            <?php
                            }
                        }
                    $conn->close();
                    ?>
                </tbody>
            </table><br>
    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>