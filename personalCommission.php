<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$username = $_SESSION['username'];

$conn = connDB();
$a = 0;
$adv = 0;
$comm1 = 0;
$comm2 = 0;
$comm3 = 0;
$comm4 = 0;
$comm5 = 0;
$userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();



$projectDetails = getProject($conn);
// $projectName = "";
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>My Payroll | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

  <h1 class="h1-title h1-before-border shipping-h1">My Payroll</h1>


  <div class="short-red-border"></div>
<h3 class="h1-title"> Personal Commission | <a href="overridingCommission.php" class="h1-title">Overriding Commission</a></h3>
  <div class="section-divider width100 overflow">

  <?php
  $conn = connDB();

//   $projectName = "";
  $projectName = "WHERE agent = '$userUsername' ";

  $projectDetails = getProject($conn);
  ?>

  <form class="" action="selected.php" method="post">
      <select id="sel_id" name="agentDashboard"  onchange="this.form.submit();" class="clean-select">
          <?php if (isset($_GET['name']))
          {
              if ($_GET['name'] == 'SHOW ALL')
              {
                // $projectName = "";
                // $projectName = "WHERE case_status = 'COMPLETED' ";
                  $projectName = "WHERE agent = '$userUsername' ";
                // $projectName = "WHERE agent = '$userUsername' and dateCreated = DESC";
              }
              else
              {
                  $type = $_GET['name'];
                  $types = urldecode("$type");
                  // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
                  $projectName = "WHERE project_name = '$types' and agent = '$userUsername' ";
              }


              // if ($_GET['name'] == 'SHOW ALL')
              // {
              //     // $projectName = "";
              //     // $projectName = "WHERE case_status = 'COMPLETED' ";
              //     // $projectName = "WHERE agent = '$userUsername' ";
              //     $projectName = "WHERE agent = '$userUsername' ORDER BY date_created DESC ";
              // }
              // elseif($_GET['name'] == 'latest')
              // {
              //     // $time = $_GET['timeline'];
              //     // $projectDateCreated = "";
              //     // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
              //     $projectName = "WHERE agent = '$userUsername' ORDER BY date_created DESC ";
              // }
              // elseif($_GET['name'] == 'oldest')
              // {
              //     // $time = $_GET['timeline'];
              //     // $projectDateCreated = "";
              //     // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
              //     $projectName = "WHERE agent = '$userUsername' ORDER BY date_created ASC ";
              // }
              // else
              // {
              //     $type = $_GET['name'];
              //     $types = urldecode("$type");
              //     // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
              //     $projectName = "WHERE project_name = '$types' and agent = '$userUsername' ";
              // }



              ?>
              <option value="">
                  <?php echo $_GET['name'] ?>
              </option>
              <option value="">--</option>
              <?php
          }
          else
          {
              ?>
              <option value="">   Choose Project  </option>
              <?php
          }
          ?>

          <?php if ($projectDetails)
          {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++)
          {
              ?>
                  <option value="<?php echo $projectDetails[$cnt]->getProjectName()?>">
                      <?php echo $projectDetails[$cnt]->getProjectName() ?>
                  </option>
              <?php
          }
          ?>
              <option value="SHOW ALL">   SHOW ALL    </option>
          <?php
          }
          $conn->close();
          ?>
      </select>
  </form>
  </div>

  <!-- <div class="section-divider width100 overflow">
    <form class="" action="selected3.php" method="post">
        <select id="sel_name" name="sel_name"  onchange="this.form.submit();" class="clean-select">
          <option value="latest">Latest</option>
          <option value="oldest">Oldest</option>
        </select>
    </form>
  </div> -->

  <!-- <div class="section-divider width100 overflow">
    <form class="" action="selected4.php" method="post">
        <select id="sel_name" name="sel_name"  onchange="this.form.submit();" class="clean-select">
          <option value="latest">Latest</option>
          <option value="oldest">Oldest</option>
        </select>
    </form>
  </div> -->

  <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table class="shipping-table">
            <thead>
                <tr>
                    <th class="th">NO.</th>

                    <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("TOTAL COMMISSION",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("ADVANCED COMMISSION (RM)",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("1ST CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("2ND CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("3RD CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("4TH CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("5TH CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("UNCLAIMED BALANCE (RM)",10,"</br>\n");?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                // {
                    $orderDetails = getLoanStatus($conn, $projectName);
                    if($orderDetails != null)
                    {
                        for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                        {
                          $advancedDetails = getAdvancedSlip($conn, "WHERE status = 'COMPLETED' AND loan_uid = ?", array("loan_uid"), array($orderDetails[$cntAA]->getLoanUid()), "s");
                          if ($advancedDetails) {
                            $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline = ?", array("loan_uid","upline"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgent()), "ss");
                          }


                          ?>
                        <tr>
                            <td class="td"><?php echo ($cntAA+1)?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                            <td class="td"><?php echo $spaPriceValue = number_format($orderDetails[$cntAA]->getAgentComm(), 2); ?></td>
                            <?php if ($advancedDetails && $advancedDetails[0]->getDateUpdated()) {
                              $a = $orderDetails[$cntAA]->getAgentComm() - $advancedDetails[0]->getAmount();
                              ?>

                              <td class="td"><?php echo number_format($advancedDetails[0]->getAmount(), 2); ?></td><?php
                              ?> <td> <form action="advancedSlip.php" method="POST">
                                <input type="hidden" name="loan_uid" value="<?php echo $advancedDetails[0]->getLoanUid(); ?>">
                                <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $advancedDetails[0]->getLoanUid();?>"><?php echo date('d-m-Y', strtotime($advancedDetails[0]->getDateUpdated()))  ?>
                                      <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                      <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                  </button></a>
                              </form></td><?php
                          if ($commissionDetails) {
                            for ($cntBB=0; $cntBB <count($commissionDetails) ; $cntBB++) {
                              if ($commissionDetails[$cntBB]->getDetails() == '1st Commission' && $commissionDetails[$cntBB]->getDateUpdated()) {
                              ?><td class="td"><?php echo $comm1 =  $commissionDetails[$cntBB]->getCommission(); ?></td><?php
                              ?> <td> <form action="commission.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $commissionDetails[$cntBB]->getID(); ?>">
                                <input type="hidden" name="project_name" value="<?php echo $commissionDetails[$cntBB]->getProjectName(); ?>">
                                <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($commissionDetails[$cntBB]->getDateUpdated()))  ?>
                                      <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                      <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                  </button></a>
                              </form></td><?php
                            }else {
                              ?><td></td>
                                <td></td> <?php
                            }
                            if ($commissionDetails[$cntBB]->getDetails() == '2nd Commission' && $commissionDetails[$cntBB]->getDateUpdated()) {
                              ?><td class="td"><?php echo $comm2 =  $commissionDetails[$cntBB]->getCommission(); ?></td><?php
                              ?> <td> <form action="commission.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $commissionDetails[$cntBB]->getID(); ?>">
                                <input type="hidden" name="project_name" value="<?php echo $commissionDetails[$cntBB]->getProjectName(); ?>">
                                <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($commissionDetails[$cntBB]->getDateUpdated()))  ?>
                                      <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                      <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                  </button></a>
                              </form></td><?php
                            }else {
                              ?><td></td>
                                <td></td> <?php
                            } if ($commissionDetails[$cntBB]->getDetails() == '3rd Commission' && $commissionDetails[$cntBB]->getDateUpdated()) {
                              ?><td class="td"><?php echo $comm3 = $commissionDetails[$cntBB]->getCommission(); ?></td><?php
                              ?> <td> <form action="commission.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $commissionDetails[$cntBB]->getID(); ?>">
                                <input type="hidden" name="project_name" value="<?php echo $commissionDetails[$cntBB]->getProjectName(); ?>">
                                <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($commissionDetails[$cntBB]->getDateUpdated()))  ?>
                                      <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                      <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                  </button></a>
                              </form></td><?php
                            }else {
                              ?><td></td>
                                <td></td> <?php
                            }
                            if ($commissionDetails[$cntBB]->getDetails() == '4th Commission' && $commissionDetails[$cntBB]->getDateUpdated()) {
                              ?><td class="td"><?php echo $comm4 =  $commissionDetails[$cntBB]->getCommission(); ?></td><?php
                              ?> <td> <form action="commission.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $commissionDetails[$cntBB]->getID(); ?>">
                                <input type="hidden" name="project_name" value="<?php echo $commissionDetails[$cntBB]->getProjectName(); ?>">
                                <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($commissionDetails[$cntBB]->getDateUpdated()))  ?>
                                      <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                      <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                  </button></a>
                              </form></td><?php
                            }
                            else {
                              ?><td></td>
                                <td></td>  <?php
                            }if ($commissionDetails[$cntBB]->getDetails() == '5th Commission' && $commissionDetails[$cntBB]->getDateUpdated()) {
                              ?><td class="td"><?php echo $comm5 =  $commissionDetails[$cntBB]->getCommission(); ?></td><?php
                              ?> <td> <form action="commission.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $commissionDetails[$cntBB]->getID(); ?>">
                                <input type="hidden" name="project_name" value="<?php echo $commissionDetails[$cntBB]->getProjectName(); ?>">
                                <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($commissionDetails[$cntBB]->getDateUpdated()))  ?>
                                      <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                      <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                  </button></a>
                              </form></td><?php
                            }else {
                              ?><td></td>
                                <td></td>  <?php
                            }
                            $a = $orderDetails[$cntAA]->getAgentComm() - $adv - $comm1 - $comm2 - $comm3 - $comm4 - $comm5;
                          }
                        }else {
                          ?><td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td> <?php
                        }
                        ?>
                        <td><?php echo number_format($a, 2); ?></td>
                        </tr>
                        <?php
                      }else {
                        ?><td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <?php
                          if (!$a) {
                          ?><td><?php echo  number_format($orderDetails[$cntAA] ->getAgentComm(), 2); ?></td> <?php
                        }else {
                          ?><td></td> <?php
                        }

                      }

                  }

                }
                //}
                ?>
            </tbody>
        </table><br>
    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
