<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE username = ? ",array("username"),array($_SESSION['username']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
#underline {
    border-bottom: solid 1px black;
}
</style>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://gic.asia/editPassword.php" />
    <meta property="og:title" content="Reset Password | GIC" />
    <title>Reset Password | GIC</title>
    <meta property="og:description" content="GIC" />
    <meta name="description" content="GIC" />
    <meta name="keywords" content="GIC, etc">
    <link rel="canonical" href="https://gic.asia/editPassword.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">

<?php  include 'allAdminHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <form class="edit-profile-div2" action="utilities/editPasswordFunction.php" method="POST">
        <h1 class="Edit Password" style="margin-top: 50px">Edit Password</h1>

        <table class="edit-profile-table password-table">
        	<tr class="profile-tr">

                <td class="profile-td1">Current Password</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">                                   
                    <input name="editPassword_current" id="editPassword_current" class="clean edit-profile-input login-input password-input" type="password" required>
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_current_img">
                    </span>
                </td>
            </tr>
        	<tr class="profile-tr">
                <td class="profile-td1">New Password</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input name="editPassword_new" id="editPassword_new" class="clean edit-profile-input login-input password-input" type="password" required>
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_new_img">
                    </span>
                </td>
            </tr>            
        	<tr class="profile-tr">
                <td class="profile-td1">Retype New Password</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input name="editPassword_reenter" id="editPassword_reenter" class="clean edit-profile-input login-input password-input" type="password" required>
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_reenter_img">
                    </span>
                </td>
            </tr>          
        </table>
        <button class="confirm-btn text-center white-text clean black-button">Save</button>
    </form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Current Password Doesn't Match  <br>  Pls Retry ！";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "New Password Must Be More Than Five Letters";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "New Password & Retype New Password Doesn't Match  <br>  Pls Retry ！";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Server Down  <br>  Pls Retry Again Later ！";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Successfully Reset Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
  viewPassword( document.getElementById('editPassword_current_img'), document.getElementById('editPassword_current'));
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('editPassword_new'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('editPassword_reenter'));
</script>

</body>
</html>