<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/EditHistory.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$projectName = "";
// $sql = "select pdf from loan_status";
// $result = mysqli_query($conn, $sql);
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Booking Form Upload | GIC" />
    <title>Edit History | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Edit History</h1>

    <div class="short-red-border"></div>

    <div class="section-divider width100 overflow">
		<?php $projectDetails = getProject($conn); ?>

    <form class="" action="selected.php" method="post">
      <select id="sel_id" name="historyIndividual"  onchange="this.form.submit();" class="clean-select">
        <?php if (isset($_GET['name'])) {
          if ($_GET['name'] == 'SHOW ALL') {
            $projectName = "";
          }else {
            $type = $_GET['name'];
            $types = urldecode("$type");
            $projectName = "AND project_name = '$types'";
          }
          ?><option value=""><?php echo $_GET['name'] ?></option>
          <option value="">Choose Project</option><?php
        }else {
          ?><option value="">Choose Project</option><?php
        } ?>

        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }

              }
              ?><option value="SHOW ALL">SHOW ALL</option><?php
            } ?>

      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
    </form>


    </div>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->
    <!-- End of Filter -->


    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th">PROJECT NAME</th>
                        <th class="th">UNIT NO.</th>
                        <th class="th">DETAILS</th>
                        <th class="th">DATA EDIT</th>
                        <th class="th">EDIT TO</th>
                        <th class="th">USERNAME</th>
                        <th class="th">DATE MODIFIED</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $editHistoryDetails = getEditHistory($conn, "ORDER BY date_created DESC" );
                        // $editHistoryDetails = getLoanStatus($conn);
                        if($editHistoryDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($editHistoryDetails) ;$cntAA++)
                            { $loanUidHistory = $editHistoryDetails[$cntAA]->getLoanUid();
                              $loanDetails = getLoanStatus($conn,"WHERE loan_uid = '$loanUidHistory' $projectName");
                              if ($loanDetails) {
                                ?>
                              <tr>
                                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td"><?php echo $cntAA + 1?></td>
                                  <td class="td"><?php echo $loanDetails[0]->getProjectName();?></td>
                                  <td class="td"><?php echo $loanDetails[0]->getUnitNo();?></td>
                                  <td class="td"><?php echo $editHistoryDetails[$cntAA]->getDetails();?></td>
                                  <td class="td"><?php echo $editHistoryDetails[$cntAA]->getDataBefore() ?></td>
                                  <td class="td"><?php echo $editHistoryDetails[$cntAA]->getDataAfter() ?></td>
                                  <td class="td"><?php echo $editHistoryDetails[$cntAA]->getUsername() ?></td>
                                  <td class="td"><?php echo date('d-m-Y',strtotime($editHistoryDetails[$cntAA]->getDateCreated())) ?></td>

                              </tr>
                              <?php
                              }

                            }
                        }
                    //}
                    ?>
                </tbody>
            </table><br>


    </div>


    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
