<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/EditHistory.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$projectName = "";
// $sql = "select pdf from loan_status";
// $result = mysqli_query($conn, $sql);
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Booking Form Upload | GIC" />
    <title>Agent Info | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Agent Info</h1>

    <div class="short-red-border"></div>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->
    <!-- End of Filter -->


    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th">AGENT FULLNAME</th>
                        <th class="th">AGENT USERNAME</th>
                        <th class="th">IC NO</th>
                        <th class="th">BIRTH MONTH</th>
                        <th class="th">CONTACT NO</th>
                        <th class="th">E-MAIL</th>
                        <th class="th">BANK NAME</th>
                        <th class="th">BANK NO.</th>
                        <th class="th">ADDRESS</th>
                        <th class="th">UPLINE1</th>
                        <th class="th">UPLINE2</th>
                        <th class="th">STATUS</th>
                        <!-- <th class="th">DATE CREATED</th> -->
                        <th class="th">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $agentDetails = getUser($conn, "WHERE user_type = 3 ORDER BY date_created DESC" );
                        // $agentDetails = getLoanStatus($conn);
                        if($agentDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($agentDetails) ;$cntAA++){
                                ?>
                              <tr>
                                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td"><?php echo $cntAA + 1?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getFullName();?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getUsername();?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getIcNo() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getBirthday() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getPhoneNo() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getEmail() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getBankName() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getBankAccountNo() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getAddress();?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getUpline1() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getUpline2() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getStatus() ?></td>
                                  <!-- <td class="td"><?php //echo date('d-m-Y',strtotime($agentDetails[$cntAA]->getDateCreated())) ?></td> -->
                                  <td class="td">
                                      <form action="editAgent.php" method="POST">
                                          <button class="clean edit-anc-btn hover1" type="submit" name="id" value="<?php echo $agentDetails[$cntAA]->getId();?>">
                                              <img src="img/edit.png" class="edit-announcement-img hover1a" alt="edit Agent" title="edit Agent">
                                              <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="edit Agent" title="edit Agent">
                                          </button>
                                      </form>
                                  </td>
                              </tr>
                              <?php


                            }

                    }
                    ?>
                </tbody>
            </table><br>


    </div>


    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
