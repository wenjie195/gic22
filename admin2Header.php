<div class="headerline"></div>
<div class="width100 same-padding header-div">
	<a href="adminDashboard.php"><div class="logo"><img src="img/logo.png" class="logo-size"></div></a>
	<div class="right-menu">
		<div class="dropdown hover1">
				<a  class="menu-right-margin dropdown-btn">
						<img src="img/user-black.png" class="gic-menu-icon hover1a" alt="Agent Info" title="Agent Info">
						<img src="img/user-black.png" class="gic-menu-icon hover1b" alt="Agent Info" title="Agent Info">
					</a>
					<div class="dropdown-content yellow-dropdown-content">
					<p class="dropdown-p"><a href="addAgent.php"  class="dropdown-a">Add Agent</a></p>
			<p class="dropdown-p"><a href="editAgentInfo.php"  class="dropdown-a">Edit Agent Info</a></p>
					</div>
		</div>
       	<a href="invoiceRecord.php"  class="menu-right-margin hover1">
        	<img src="img/invoice-coin.png" class="hover1a gic-menu-icon" alt="Invoice Record" title="Invoice Record">
            <img src="img/invoice-coin2.png" class="hover1b gic-menu-icon" alt="Invoice Record" title="Invoice Record">
        </a>
   		<a href="admin2Product.php"  class="menu-right-margin hover1">
        	<img src="img/invoice-only.png" class="hover1a gic-menu-icon" alt="Loan Status Invoice" title="Loan Status">
            <img src="img/invoice-only2.png" class="hover1b gic-menu-icon" alt="Loan Status Invoice" title="Loan Status">
        </a>
    	<a href="logout.php" class="hover1">
        	<img src="img/logout.png" class="hover1a gic-menu-icon" alt="logout" title="logout">
            <img src="img/logout2.png" class="hover1b gic-menu-icon" alt="logout" title="logout">
        </a>
    </div>
</div>
<div class="clear"></div>
