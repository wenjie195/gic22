<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$claimStatus = "";

$unitNo = $_POST['unitt'];   // department id

$loanDetails = getLoanStatus($conn, "WHERE unit_no = ?", array("unit_no"), array($unitNo), "s");
$totalDevAmt = $loanDetails[0]->getTotalDeveloperComm();
$projectName = $loanDetails[0]->getProjectName();
$projectDetails = getProject($conn, "WHERE project_name =?", array("project_name"), array($projectName), "s");
$projectClaims = $projectDetails[0]->getProjectClaims();

$totalDevAmtFinal = $totalDevAmt / $projectClaims;

if (!$loanDetails[0]->getClaimAmt1st()) {
  $claimStatus = 1;
}elseif (!$loanDetails[0]->getClaimAmt2nd()) {
  $claimStatus = 2;
}elseif (!$loanDetails[0]->getClaimAmt3rd()) {
  $claimStatus = 3;
}elseif (!$loanDetails[0]->getClaimAmt4th()) {
  $claimStatus = 4;
}elseif (!$loanDetails[0]->getClaimAmt5th()) {
  $claimStatus = 5;
}
   // department id
  // $sql = "SELECT totaldevelopercomm FROM loan_status WHERE unit_no='$unitNo'";
  //
  // $result = mysqli_query($conn,$sql);
  //
  // $loanDetails = array();
  //
  // while( $row = mysqli_fetch_array($result) ){
  //     $unit = $row['totaldevelopercomm'] / $projectClaims;


      $total[] = array("totaldevelopercomm" => $totalDevAmtFinal, "claim_status" => $claimStatus);


  // }



  // encoding array to json format
  echo json_encode($total);


 ?>
