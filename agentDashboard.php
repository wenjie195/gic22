<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$username = $_SESSION['username'];

$conn = connDB();

$userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();

$projectDetails = getProject($conn);
// $projectName = "";
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

  <h1 class="h1-title h1-before-border shipping-h1">My Performance</h1>


  <div class="short-red-border"></div>
<h3 class="h1-title"> Personal Sales | <a href="personalOverriding.php" class="h1-title">Personal Overriding</a></h3>
  <div class="section-divider width100 overflow">

  <?php
  $conn = connDB();

//   $projectName = "";
  $projectName = "WHERE agent = '$userUsername' ";

  $projectDetails = getProject($conn);
  ?>

  <form class="" action="selected.php" method="post">
      <select id="sel_id" name="agentDashboard"  onchange="this.form.submit();" class="clean-select">
          <?php if (isset($_GET['name']))
          {
              if ($_GET['name'] == 'SHOW ALL')
              {
                // $projectName = "";
                // $projectName = "WHERE case_status = 'COMPLETED' ";
                  $projectName = "WHERE agent = '$userUsername' ";
                // $projectName = "WHERE agent = '$userUsername' and dateCreated = DESC";
              }
              else
              {
                  $type = $_GET['name'];
                  $types = urldecode("$type");
                  // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
                  $projectName = "WHERE project_name = '$types' and agent = '$userUsername' ";
              }


              // if ($_GET['name'] == 'SHOW ALL')
              // {
              //     // $projectName = "";
              //     // $projectName = "WHERE case_status = 'COMPLETED' ";
              //     // $projectName = "WHERE agent = '$userUsername' ";
              //     $projectName = "WHERE agent = '$userUsername' ORDER BY date_created DESC ";
              // }
              // elseif($_GET['name'] == 'latest')
              // {
              //     // $time = $_GET['timeline'];
              //     // $projectDateCreated = "";
              //     // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
              //     $projectName = "WHERE agent = '$userUsername' ORDER BY date_created DESC ";
              // }
              // elseif($_GET['name'] == 'oldest')
              // {
              //     // $time = $_GET['timeline'];
              //     // $projectDateCreated = "";
              //     // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
              //     $projectName = "WHERE agent = '$userUsername' ORDER BY date_created ASC ";
              // }
              // else
              // {
              //     $type = $_GET['name'];
              //     $types = urldecode("$type");
              //     // $projectName = "WHERE project_name = '$types' and case_status = 'COMPLETED' ";
              //     $projectName = "WHERE project_name = '$types' and agent = '$userUsername' ";
              // }



              ?>
              <option value="">
                  <?php echo $_GET['name'] ?>
              </option>
              <option value="">--</option>
              <?php
          }
          else
          {
              ?>
              <option value="">   Choose Project  </option>
              <?php
          }
          ?>

          <?php if ($projectDetails)
          {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++)
          {
              ?>
                  <option value="<?php echo $projectDetails[$cnt]->getProjectName()?>">
                      <?php echo $projectDetails[$cnt]->getProjectName() ?>
                  </option>
              <?php
          }
          ?>
              <option value="SHOW ALL">   SHOW ALL    </option>
          <?php
          }
          $conn->close();
          ?>
      </select>
  </form>
  </div>

  <!-- <div class="section-divider width100 overflow">
    <form class="" action="selected3.php" method="post">
        <select id="sel_name" name="sel_name"  onchange="this.form.submit();" class="clean-select">
          <option value="latest">Latest</option>
          <option value="oldest">Oldest</option>
        </select>
    </form>
  </div> -->

  <!-- <div class="section-divider width100 overflow">
    <form class="" action="selected4.php" method="post">
        <select id="sel_name" name="sel_name"  onchange="this.form.submit();" class="clean-select">
          <option value="latest">Latest</option>
          <option value="oldest">Oldest</option>
        </select>
    </form>
  </div> -->

  <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table class="shipping-table">
            <thead>
                <tr>
                    <th class="th">NO.</th>
                    <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?></th>
                    <!-- <th>STOCK</th> -->
                    <th class="th">IC</th>
                    <th class="th">CONTACT</th>
                    <th class="th">E-MAIL</th>
                    <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                    <th class="th">SQ FT</th>
                    <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?></th>
                    <th class="th">PACKAGE</th>
                    <th class="th">DISCOUNT</th>
                    <th class="th">REBATE</th>
                    <th class="th"><?php echo wordwrap("EXTRA REBATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?></th>
                    <th class="th">AGENT</th>
                    <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?></th>

                    <th class="th">REMARK</th>
                    <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?></th>
                    <th class="th">LAWYER</th>
                    <th class="th"><?php echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?></th>
                    <th class="th">CASH BUYER</th>
                    <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?></th>
                    <th class="th"><?php echo wordwrap("EVENT PERSONAL",10,"</br>\n");?></th>
                    <th class="th">RATE</th>
                    <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                // {
                    $orderDetails = getLoanStatus($conn, $projectName);
                    if($orderDetails != null)
                    {
                        for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                        {?>
                        <tr>
                            <td class="td"><?php echo ($cntAA+1)?></td>
                            <!-- <td class="td"><?php echo $orderDetails[$cntAA]->getId();?></td> -->
                            <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getUnitNo());?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getPurchaserName());?></td>
                            <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getIc());?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getContact());?></td>

                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getEmail());?></td>
                            <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getSqFt();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getSpaPrice();?></td> -->

                            <!-- show , inside value -->
                            <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                            <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getExtraRebate();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getNettPrice();?></td>
                            <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                            <!-- show , inside value -->
                            <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                            <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                            <?php $totalDevComm = $orderDetails[$cntAA]->getTotalDeveloperComm();?>
                            <td class="td"><?php echo $totalDevCommValue = number_format($totalDevComm, 2); ?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                            <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRemark();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRate();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getAgentComm();?></td> -->

                            <?php $agentComm = $orderDetails[$cntAA]->getAgentComm();?>
                            <td class="td"><?php echo $agentCommValue = number_format($agentComm, 2); ?></td>
                        </tr>
                        <?php
                        }
                    }
                //}
                ?>
            </tbody>
        </table><br>
    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
