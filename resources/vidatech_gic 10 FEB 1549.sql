-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2020 at 07:55 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_gic`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_project`
--

CREATE TABLE `admin_project` (
  `id` int(50) NOT NULL,
  `unit_no` varchar(50) NOT NULL,
  `purchaser_name` varchar(50) NOT NULL,
  `ic` int(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `booking_date` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1),
  `sq_ft` int(50) NOT NULL,
  `spa_price` int(50) NOT NULL,
  `package` varchar(11) DEFAULT NULL,
  `rebate` varchar(255) DEFAULT NULL,
  `nettprice` varchar(255) DEFAULT NULL,
  `totaldevelopercomm` varchar(255) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `loanstatus` varchar(255) DEFAULT NULL,
  `remark` varchar(255) NOT NULL,
  `form_Collected` varchar(255) NOT NULL,
  `bank_approved` varchar(255) NOT NULL,
  `lo_signed_date` varchar(255) NOT NULL,
  `la_signed_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_project`
--

INSERT INTO `admin_project` (`id`, `unit_no`, `purchaser_name`, `ic`, `contact`, `email`, `booking_date`, `sq_ft`, `spa_price`, `package`, `rebate`, `nettprice`, `totaldevelopercomm`, `agent`, `loanstatus`, `remark`, `form_Collected`, `bank_approved`, `lo_signed_date`, `la_signed_date`) VALUES
(1, '12-08', 'Khoo Eng Liang & Law Mei Ling', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Steve', 'MBB: Prepare docs.\nCancelled on 1-11-17(Wed). Refund on 14-11-17.', '', 'Yes', '', '', ''),
(2, '16-05', 'Pheh Guan Choon', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 473,850.00 ', ' 11,846.25 ', 'Jocelyn', 'MBB: Approved 80%. Need amend loan.\nAmB: Shawn follow up.\nRHB: Approved 80%. Pending acceptance.\nPBB: Pending approval on amendment of price.\nCancelled on 26-2-18. Forfeit RM 1k. Sent to lawyer on 1-3-18. Refund on 7-3-18.', '', 'Yes', '', '', ''),
(3, '16-19', 'Hong Guo Haur', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 398,034.00 ', ' 9,950.85 ', 'Steve', 'PBB: Cancel purchase.\nMBB: Received docs. High DSR.\nCancellation sent on 14-5-18. Refunded on 3-7-18.', '', 'Yes', '', '', ''),
(4, '12-16', 'Nia Jun Hau & Ooi Cher Li', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '19%', ' 514,179.90 ', ' 12,854.50 ', 'Steve', 'PBB: Loan approved at 70%. Customer declined.\nMBB: Received docs. High credit card high DSR.\nAmB: Shawn follow up.\nRHB: Comm burst. No joint applicant. Rejected.\nSent to lawyer on 2-2-18. **refund 6500 Resend on 9-3-18. Refund on 20-3-18.', '', 'Yes', '', '', ''),
(5, '12-11', 'Tan Suok Cheng & Yap Boon Pen', 680206, '016-4426850\n012-7859699', '', '0000-00-00 00:00:00.0', 364, 491, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Aric', 'AmB James: Approved 66%. LO signed on 12-12-17. SPA signed on 8/1/18. Another sign on 12-2-18.', '', 'Yes', 'AmB', '12/12/2017', '8/01/2018'),
(6, '16-10', 'Michael Tang Tung Hong', 881220, '012-3104900', '', '0000-00-00 00:00:00.0', 437, 568, '', '19%', ' 483,148.80 ', ' 12,078.72 ', 'Jocelyn', 'PBB Air Itam: Approved 70%. Will sign LO tgt with 16-10 & 16-13A.\nSPA signed on 15-12-17(Fri). LA signed on 2-4-18.', '', 'Yes', 'PBB', '', ''),
(7, '16-09', 'Tan Chau Siang & Tee Geok Keng', 0, '', '', '0000-00-00 00:00:00.0', 426, 553, '', '19%', ' 483,148.80 ', ' 12,078.72 ', 'Alvin', 'PBB: Pending wife\'s docs.\nMBB: Rejected.\nAmB: Shawn follow up.\nRHB: Still pending some complete doc. DSR burst (pending 2017 income tax).\nResubmit income tax, only can process on March.\nCancelled on 24-4-18. Refunded on 3-7-18.', '', 'Yes', '', '', ''),
(8, '16-08', 'Tan Chau Koon & Too Yen Nee', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Alvin', 'PBB: Pending wife\'s docs.\nMBB: Rejected.\nAmB: Shawn follow up.\nRHB: Still pending some complete doc. DSR burst (pending 2017 income tax).\nResubmit income tax, only can process on March.\nCancelled on 24-4-18. Refunded on 3-7-18.', '', 'Yes', '', '', ''),
(9, '16-03', 'Teoh Jiun Shiong & Teoh Ah Pee', 801108, '85-362686118', 'danny.teoh80@gmail.com', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 471,744.00 ', ' 11,793.60 ', 'Alvin', 'RHB: Approved 85%. LO & SPA signed on 17-3-18.', '', 'Yes', 'RHB', '17/03/2018', '17/03/2018'),
(10, '16-06', 'Ooi Ah Hong', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 508,177.80 ', ' 12,704.45 ', 'Alvin', 'MBB: Received docs. High DSR.\nRHB: No reply.\nOnly want PBB. Will try RHB. Cancelled on 20-3-18. Cancellation sent to lawyer on 31-3-18. Refunded on 21-5-18.', '', 'Yes', '', '', ''),
(11, '16-11', 'Leim Siew Soon', 771025, '013-4316456', '', '0000-00-00 00:00:00.0', 364, 491, '', '19%', ' 514,179.90 ', ' 12,854.50 ', 'Aric', 'Suki: SPA signed on 27-2-18. Want apply loan on 27-9-18. Cannot stamping, special case.', '', 'Yes', '', '', ''),
(12, '16-18', 'Leim Siew Soon', 771025, '013-4316456', '', '0000-00-00 00:00:00.0', 394, 531, '', '19%', ' 500,175.00 ', ' 12,504.38 ', 'Aric', 'Suki: SPA signed on 27-2-18. Want apply loan on 27-9-18. Cannot stamping, special case.', '', 'Yes', '', '', ''),
(13, '16-07', 'Firstmech Industries Sdn Bhd', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 508,177.80 ', ' 12,704.45 ', 'Alvin', 'MBB: Rejected.\nAmB: Shawn follow up.\nRHB: Docs collected. In process.\nPBB: Not called yet as selling price amended.\nCancelled on 25-1-18. RHB alr submitted. Cancelled on 26-2-18. Sent to lawyer on 1-3-18. 6500 Resend on 9-3-18. Refund on 20-3-18.', '', 'Yes', '', '', ''),
(14, '12-05', 'Low Choon Piao & Ng See Tien', 730903, '019-4572618\n019-4586819', 'cplow1898@gmail.com\ntkbj_stay@yahoo.com', '0000-00-00 00:00:00.0', 494, 617, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Kenny', 'MBB: Approved 80%. LO & SPA signed on 15-1-18.', '', 'Yes', 'MBB', '15/01/2018', '15/01/2018'),
(15, '12-09', 'Tan Boon Wah & Siew Shuk Ming', 680972, '017-4746660', 'boonwtan@yahoo.com', '0000-00-00 00:00:00.0', 426, 553, '', '19%', ' 460,161.00 ', ' 11,504.03 ', 'Loy', 'PBB: Approved 80%. LO signed on 16-11-17(Thu). SPA signed on 19-1-18.', '', 'Yes', 'PBB', '', ''),
(16, '16-16', 'Eileen Tan Sze Wey', 810929, '012-4070981', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 460,161.00 ', ' 11,504.03 ', 'Sharon', 'MBB: Approved 85%. LO signed on 22-12-17. SPA signed on 20-1-18.', '', 'Yes', 'MBB', '', ''),
(17, '16-01', 'Koay Chiew Bok', 870411, '012-4233161 (sis)', '', '0000-00-00 00:00:00.0', 508, 627, '', '19%', ' 398,034.00 ', ' 9,950.85 ', 'Jordan', 'MBB: Approved RM 500k. LO signed on 14-11-17(Tue). SPA signed on 15-1-18.', '', 'Yes', 'MBB', '', ''),
(18, '16-17', 'Khoo May Shan', 810626, '017-2997773(sister)', '', '0000-00-00 00:00:00.0', 451, 586, '', '19%', ' 448,578.00 ', ' 11,214.45 ', 'Sharon', 'MBB: Approved 85%. LO & SPA signed on 20-1-18.', '', 'Yes', 'MBB', '20/01/2018', '20/01/2018'),
(19, '17-08', 'Goh Boon Ooi\n**Chg unit frm 16-12', 740108, '016-4954998', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 514,179.90 ', ' 12,854.50 ', 'Jolin', 'PBB: Loan submitted earlier. Follow up with Mr Goh.\nAmB: Docs received, processing.\nRHB: Income not enough. Consider wanna join in another borrower or not.\nPBB: Pending docs.\nCancelled on 26-1-18. Sent to lawyer on 1-3-18. Resend 6800 on 27-6-18. Refunded', '', 'Yes', '', '', ''),
(20, '16-13', 'Chuah Chia Ling', 861217, '012-4010477 (Ms Lim)', '', '0000-00-00 00:00:00.0', 450, 585, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Sharon', 'RHB: Approved 85%. LO & SPA signed on 20-2-18.', '', 'Yes', 'RHB', '20/02/2018', '20/02/2018'),
(21, '13A-11', 'Andrew Tan Chong Wei\n**Chg unit frm 16-15', 850516, '016-5243278', '', '0000-00-00 00:00:00.0', 364, 491, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Sharon', 'MBB: Approved 80%. LO & SPA signed on 15-1-18.', '', 'Yes', 'MBB', '15/01/2018', '15/01/2018'),
(22, '16-20', 'Goay Chee Keong', 750317, '019-4209989', '', '0000-00-00 00:00:00.0', 514, 634, '', '19%', ' 430,839.00 ', ' 10,770.98 ', 'Alvin', 'RHB: Approved 85%. LO signed on 2-2-18. SPA signed on 19-2-18.', '', 'Yes', 'RHB', '', ''),
(23, '12-07', 'Seng Chong Kheen', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 430,839.00 ', ' 10,770.98 ', 'Aric', 'Suki: MBB approved 85%. - RHB in process.\nAmB approved 75% (rate 4.9%).\nRHB: Outstation.\nPBB: Overseas.\nCancelled on 1-2-18. Cancellation received on 5-3-18. Sent to lawyer on 9-3-18. Refund on 20-3-18.', '', 'Yes', '', '', ''),
(24, '12-02', 'Bold Excel Services Sdn Bhd', 800522, '012-4720099', 'jason@boldexcel.com', '0000-00-00 00:00:00.0', 466, 596, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Alvin', 'MBB: Approved 90%. LO signed on 28-12-17(Thu). SPA signed on 28-2 & 12-3-18.', '', 'Yes', 'MBB', '', ''),
(25, '16-02', 'Bold Excel Services Sdn Bhd\n0124937099 SK Loh ', 800522, '012-4720099', 'jason@boldexcel.com', '0000-00-00 00:00:00.0', 466, 596, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Alvin', 'MBB: Approved 90%. LO signed on 28-12-17(Thu). SPA signed on 28-2 & 12-3-18.', '', 'Yes', 'MBB', '', ''),
(26, '16-3A', 'Home Sweet Home Builders (P) S/B', 660726, '012-4988721\n012-4918721', 'ysmiang@gmail.com\nyockkuai@gmail.com', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Alvin', 'MBB: Approved 85%. LO signed on 18-12-17(Mon). SPA signed on 27-2-18.', '', 'Yes', 'MBB', '', ''),
(27, '16-13A', 'Lau Kiang Hong & Lim Ai Lek', 0, '', '', '0000-00-00 00:00:00.0', 448, 582, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Louise', 'Cancelled on 22-11-17. Refund on 27-11-17.', '', 'Yes', '', '', ''),
(28, '12-01', 'Ooi Kooi Hwa', 0, '', '', '0000-00-00 00:00:00.0', 508, 627, '', '19%', ' 448,578.00 ', ' 11,214.45 ', 'Jolin', 'MBB: Prepare docs, doing hardware business.\nAmB: Preparing docs.\nRHB: Rejected, high commitment after add docs.\nPBB: Docs needed whatsapped to customer.\nCancelled on 5-2-18. Sent to lawyer on 27-2-18. 6500 Resend on 9-3-18. Refund on 20-3-18.', '', 'Yes', '', '', ''),
(29, '12-20', 'Chee Her Kheng & Sim Chap Sin', 0, '', '', '0000-00-00 00:00:00.0', 514, 634, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jess', 'MBB: Want to cancel.\nAmB: Considering. Cancelled on 13-11-17. Refund on 27-11-17.', '', 'Yes', '', '', ''),
(30, '12-3A', 'Ng Lim Seong', 0, '', '', '0000-00-00 00:00:00.0', 494, 617, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jolin', 'MBB: Need ask wife, busy.\nAmB: Follow up, msg drop.\nRHB: Still in the mid prepare doc.\nPBB: Pending amendment of purchase price. \nCancelled on 5-2-18. Sent to lawyer on 1-3-18. *6500 Resend on 9-3-18. Refund on 19-3-18.', '', 'Yes', '', '', ''),
(31, '17-01', 'Tan Soo Hwang', 620809, '012-4229377', '', '0000-00-00 00:00:00.0', 508, 627, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Jolin', 'Cash Buyer.\nSPA signed on 4-1-18(Thu).', '', 'Yes', 'Cash', '', ''),
(32, '17-07', 'Soo Eng How & Tang Sing Yi\n**Chg unit frm 17-3A on', 770310, '010-8873595', 'frederick_soo@hotmail.com', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 500,175.00 ', ' 12,504.38 ', 'Jessica', 'RHB: Approved 85%. LO signed on 15-2-18. SPA signed on 23-2-18.', '', 'Yes', 'RHB', '', ''),
(33, '13A-10', 'WT Jovest Sdn Bhd', 730330, '012-4322055', '', '0000-00-00 00:00:00.0', 437, 568, '', '19%', ' 448,578.00 ', ' 11,214.45 ', 'Jocelyn', 'MBB: Approved 85%. LO signed on 22-4-18.\nSPA signed on 30-4-18. LA signed on 27-8-18.', '', 'Yes', 'MBB', '', ''),
(34, '12-10', 'Ngu Siew Hiong', 530311, '019-8867037', '', '0000-00-00 00:00:00.0', 437, 568, '', '19%', ' 483,148.80 ', ' 12,078.72 ', 'Jocelyn', 'PBB: CASH BUYER.\nSPA signed on 15-12-17(Fri).', '', 'Yes', 'PBB', '', ''),
(35, '17-10', 'Confast Mobile Sdn Bhd', 0, '', '', '0000-00-00 00:00:00.0', 364, 491, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Eddie', 'MBB: Approved 85%. LO signed on 16-1-18. SPA signed on 28-2-18. LA signed on April.', '', 'Yes', 'MBB', '', ''),
(36, '17-09', 'Loh QinHui', 920624, '013-5847725', 'qinhuijyn@yahoo.com', '0000-00-00 00:00:00.0', 426, 553, '', '19%', ' 514,179.90 ', ' 12,854.50 ', 'Jolin', 'MBB: Approved 85%. LO signed on 11-12-17(Mon). SPA signed on 3-1-18(Wed).', '', 'Yes', 'MBB', '', ''),
(37, '17-19', 'Lim See Peng', 770311, '012-4513846', '', '0000-00-00 00:00:00.0', 514, 634, '', '19%', ' 474,903.00 ', ' 11,872.58 ', 'Brian', 'Own banker. Brian follow up.\nMBB: Approved 80%. LO signed on 19-12-17.\nSPA signed on 5-1-18.', '', 'Yes', 'MBB', '', ''),
(38, '15-08', 'Lim Peng Yik', 910826, '019-4246826', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Brian', 'Own banker. Brian follow up.\nMBB: Approved 80%. LO signed on 19-12-17.\nSPA signed on 5-1-18.', '', 'Yes', 'MBB', '', ''),
(39, '17-18', 'Teoh Kee Kong & Chew Seow Leng', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 473,850.00 ', ' 11,846.25 ', 'Jocelyn', 'PBB Own banker.\nMBB: Currently don\'t want maybank.\nRHB: Contacted, customer request to call back.\nCancelled on 19-12-17. Sent to lawyer on 2-2-18. refund 6800 Resend on 9-3-18. Refund on 20-3-18.', '', 'Yes', '', '', ''),
(40, '12-18', 'Clement Pang Keat Yoong', 771212, '012-4022358', '', '0000-00-00 00:00:00.0', 394, 531, '', '19%', ' 471,744.00 ', ' 11,793.60 ', 'Joe', 'PBB: Loan approved. LO signed on 13-12-17(Wed). SPA sign on 12-1-18.', '', 'Yes', 'PBB', '', ''),
(41, '17-17', 'Yeong Chun Gai & Tham Choon Seong', 831217, '012-5918077', '', '0000-00-00 00:00:00.0', 394, 531, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Aric', 'Suki: MBB approved 85%. LO signed on 27-12-17. SPA signed on 18-1-18.', '', 'Yes', 'MBB', '', ''),
(42, '12-19', 'Toh Lay Ai & Eng Weng Kong', 740108, '016-4954998', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 444,366.00 ', ' 11,109.15 ', 'Jocelyn', 'PBB: Approved 85%. LO signed on 20-1-18. SPA signed on 10-2-18.', '', 'Yes', 'PBB', '', ''),
(43, '12-17', 'Lim Seng Khoon & Tan Mung Yau', 750404, '012-4739045\n012-4397502', 'sengkhoon@yahoo.com\njacqueline_tmy@hotmail.com', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Kenny', 'MBB: Approved 85%. LO & SPA sign on 10-1-18.', '', 'Yes', 'MBB', '10/01/2018', '10/01/2018'),
(44, '17-06', 'Lian Chee Wei & Hew Pei Yu', 830219, '016-4474336\n012-4364336', 'jennifer.hew18@gmail.com', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 473,850.00 ', ' 11,846.25 ', 'Jolin', 'RHB: Approved 85%. LO signed on 14-1-18. SPA signed on 21-2-18.', '', 'Yes', 'RHB', '', ''),
(45, '13-17', 'Lim Ei Leen & Teoh Chyuan Huah', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 444,366.00 ', ' 11,109.15 ', 'Alvin', 'PBB: Rejected.\nMBB: High commitment, decline.\nRHB: Submitted, pending approval.\nCancelled on 12-1-18. Sent to lawyer on 18-1-18. Refund on 19-1-18.', '', 'Yes', '', '', ''),
(46, '13A-09', 'Chan Wee Ee', 0, '', '', '0000-00-00 00:00:00.0', 426, 553, '', '19%', ' 444,366.00 ', ' 11,109.15 ', 'Chanel', 'PBB: Unable to get through. Chanel was informed.\nMBB: High commitment, decline.\nRHB: DSR burst. Cancel purchase. Rejection letter to be issue.\nCancelled on 28-12-17. Refund on 19-1-18.', '', 'Yes', '', '', ''),
(47, '13-08', 'Lim Boon See', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Joe', 'MBB: High commitment, no one can joint, ask MBB issue rejection letter 29/1/18\nRHB: DSR burst. Still pending add doc support to fight (discuss with husband).\nCancelled on 5-3-18. Sent to lawyer on 9-3-18. Refund on 22-3-18.', '', 'Yes', '', '', ''),
(48, '13-07', 'Soo Peik Thoo\n**chg unit from 13-03 on 22-12-17', 670904, '016-4155372', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 430,839.00 ', ' 10,770.98 ', 'Jolin', 'MBB Own Banker: Approved 70%. LO signed on 15-2-18. SPA signed on 7-2-18. LA signed on 23-3-18.', '', 'Yes', 'MBB', '', ''),
(49, '16-12', 'Andrew Yap JunLin', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 398,034.00 ', ' 9,950.85 ', 'Louise', 'PBB: Received mother\'s docs. Processing.\nMBB: Prepare docs.\nRHB: Pending customer doc & reply. (012-4113808)\nCancelled on 6-3-18. Refund on 22-3-18.', '', 'Yes', '', '', ''),
(50, '13-06', 'Lim Beng Suat & Chuah Tin Poay', 690413, '012-4541767\n014-3381767', '', '0000-00-00 00:00:00.0', 494, 617, '', '19%', ' 460,161.00 ', ' 11,504.03 ', 'Aric', 'CASH BUYER. SPA signed on 5-1-18.', '', 'Yes', 'Cash', '', ''),
(51, '13-09', 'Ong Yit Chuin', 0, '', '', '0000-00-00 00:00:00.0', 426, 553, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Clover', 'PBB: Approved 80%. LO signed on 13-12-17(Wed). SPA signed on 18-12-17(Mon). LA signed on 23-3-18.', '', 'Yes', 'PBB', '', ''),
(52, '17-02', 'Chan May Ann & Lee Chan Wai', 870208, '010-2212625\n017-6863300', 'maynn87@gmail.com\nchanwai87@gmail.com', '0000-00-00 00:00:00.0', 466, 596, '', '19%', ' 514,179.90 ', ' 12,854.50 ', 'Bertha', 'PBB: Approved 75%. LO signed on 25-5-18.\nSPA signed on 30-1-18. LA signed on 28-10-18.', '', 'Yes', 'PBB', '', ''),
(53, '13A-07', 'Goh Siew Hoon', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Charles', 'CASH BUYER. Booking fees RM 500 forfeited.', '', 'Yes', '', '', ''),
(54, '13-20', 'Ooi Cheik Hong & Phan Sew Leng', 0, '', '', '0000-00-00 00:00:00.0', 514, 634, '', '19%', ' 444,366.00 ', ' 11,109.15 ', 'Jojo', 'MBB: Docs submitted.\nRHB: High DSR.\nPBB Kulim: Rejected.\nCancelled on 26-1-18. Sent to lawyer on 1-3-18. Refund on 7-3-18.', '', 'Yes', '', '', ''),
(55, '17-16', 'Tan Bee Chin', 841126, '016-4777628', '', '0000-00-00 00:00:00.0', 451, 586, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Jocelyn', 'RHB: Approved 80%. LO signed on 19-1-18. SPA signed on 2-2-18.', '', 'Yes', 'RHB', '', ''),
(56, '17-11', 'Teh Chee Jye', 881121, '017-4759475', '', '0000-00-00 00:00:00.0', 448, 583, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jocelyn', 'MBB: Approved 85%. LO & SPA signed on 21-2-18.', '', 'Yes', 'MBB', '21/02/2018', '21/02/2018'),
(57, '17-12', 'Kek Hean Hooi', 861013, '016-4048274', '', '0000-00-00 00:00:00.0', 450, 585, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jocelyn', 'MBB: Approved 85%. LO & SPA signed on 21-2-18.', '', 'Yes', 'MBB', '21/02/2018', '21/02/2018'),
(58, '16-13A', 'Tang Mee Ling', 830108, '019-2596621', 'meelingt2001@yahoo.com', '0000-00-00 00:00:00.0', 448, 582, '', '19%', ' 471,744.00 ', ' 11,793.60 ', 'Jocelyn', 'PBB Air Itam: Approved 85%. Want to sign tgt with 15-11 & 16-10 .\nSPA signed on 15-12-17(Fri). LA signed on 2-4-18.', '', 'Yes', 'PBB', '', ''),
(59, '13A-08', 'Ooi Guat Imm', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Jess', 'MBB: High commitment, decline.\nRHB: Doc collected. DSR burst.\nPBB: High commitment. Unable to process.\nCancelled on 20-12-17. Sent to lawyer on 4-1-18. Refund on 19-1-18.', '', 'Yes', '', '', ''),
(60, '13-13A', 'Ong Kiam Ling', 800408, '012-5520300', '', '0000-00-00 00:00:00.0', 422, 548, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Jordan', 'Own RHB banker. Jordan flw up.\nRHB: Approved 85%. LO signed on 5-1-18. sign on 26-1-18.', '', 'Yes', 'RHB', '', ''),
(61, '13A-12', 'Tang Hoay Nee', 880506, '014-9102262\n65-91229563', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jocelyn', 'MBB: Approved 85%. LO & SPA signed on 14-2-18.', '', 'Yes', 'MBB', '21/02/2018', '21/02/2018'),
(62, '13-13', 'Tea Chii Hwa & Chong Ching Fen', 0, '', '', '0000-00-00 00:00:00.0', 450, 585, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Sharon', 'MBB: Pending wife docs.\nRHB: Rejected. High commitment.\nPBB: Pending approval on amendment of price/documents\nCancelled on 22-1-18. Sent to lawyer on 2-2-18. Refund on 22-2-18.', '', 'Yes', '', '', ''),
(63, '12-15', 'Ang Bok Lai', 800109, '012-5107099', '', '0000-00-00 00:00:00.0', 422, 548, '', '19%', ' 430,839.00 ', ' 10,770.98 ', 'Jessica', 'PBB Parit Buntar: Approved 85%. LO signed on 24-1-18. SPA signed on 19-2-18.', '', 'Yes', 'PBB', '', ''),
(64, '13-15', 'Revelyn Tan Lian Poh ', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '19%', ' 483,148.80 ', ' 12,078.72 ', 'Jojo', 'OWN BANKER(Outsource).\nCCRIS. Need to check with Bank Negara\'s status. If serious need to wait one month only can submit.\nCancelled on 8-1-18. Sent to lawyer on 1-3-18. Refund on 7-3-18.', '', 'Yes', '', '', ''),
(65, '12-08', 'Tan Boon Keat & Chow Shiau Lee', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Amy', 'RHB: Doc collected. - Pending AUM. DSR burst.\nPBB: Pending approval on amendment of price.\nCancelled on 7-3-18. Sent to lawyer on 9-3-18. Refund on 8-3-18.', '', 'Yes', '', '', ''),
(66, '13-18', 'Lim Yaw Siang', 740305, '012-4958878', '', '0000-00-00 00:00:00.0', 394, 531, '', '19%', ' 448,578.00 ', ' 11,214.45 ', 'Sharon', 'MBB: Approved 80%. LO & SPA signed on 22-12-17(Fri). LA signed on 6-2-18.', '', 'Yes', 'MBB', '14/02/2018', '14/02/2018'),
(67, '15-11', 'Tang Tung Ling', 771113, '019-8867037', '', '0000-00-00 00:00:00.0', 364, 491, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jocelyn', 'PBB Air Itam: Approved 85%. Want to sign tgt with 16-13A & 16-10.\nSPA signed on 15-12-17(Fri). LA signed on 13-4-18.', '', 'Yes', 'PBB', '', ''),
(68, '15-10', 'WT Jovest Sdn Bhd', 730330, '012-4322055', '', '0000-00-00 00:00:00.0', 437, 568, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jocelyn', 'MBB: Approved 85%. LO signed on 22-4-18.\nSPA signed on 30-4-18. LA signed on 27-8-18.', '', 'Yes', 'MBB', '', ''),
(69, '17-03', 'Tye Sook Chien & Teh Siew Kee', 710101, '012-4556860', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Jonlo', 'RHB: Approved 85%. LO & SPA signed on 9-3-18.', '', 'Yes', 'RHB', '9/03/2018', '9/03/2018'),
(70, '12-20', 'Lim Ming Chyan & Ong Chiew Leng', 770120, '012-5984357', '', '0000-00-00 00:00:00.0', 514, 634, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jonlo', 'RHB: Approved 85%. LO signed on 22-3-18. SPA signed on 19-4-18.', '', 'Yes', 'RHB', '', ''),
(71, '17-13A', 'Lim Heng Seng &\nLim Hong Gark \n*Chg name frm Yeoh ', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 448,578.00 ', ' 11,214.45 ', 'Kimberlyn', 'MBB: Approved 70%. LO signed on 22-3-18. Cancelled on 4-4-18. Pending MBB cancel LO. Full refund.\nRefunded on 19-6-18.', '', 'Yes', '', '', ''),
(72, '13-15', 'Tay Khee Pim', 841221, '012-5102435', '', '0000-00-00 00:00:00.0', 422, 548, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jonlo', 'PBB: Approved 80%. LO signed on 2-3-18. SPA sign on 9-3-18.', '', 'Yes', 'PBB', '', ''),
(73, '17-18', 'Tay Yek Chiew & Lim Pei Chen', 830226, '019-3273683', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Jonlo', 'PBB: Approved 80%. LO signed on 2-3-18. SPA signed on 13-3-18.', '', 'Yes', 'PBB', '', ''),
(74, '15-06', 'Tan Yenn Chuan & Tan Yenn Wen', 851220, '012-4763898', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Sharon', 'MBB: Approved 84%. LO signed on 17-3-18. SPA signed on 3-4-18.', '', 'Yes', 'MBB', '', ''),
(75, '15-05', 'Pung It Zhang & Lau Shwu Pyng', 731109, '012-4721829\n012-5028835', 'ericpung@gmail.com', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Charles', 'RHB: Approved 80%. LO signed on 1-3-18. SPA signed on 2-3-18.', '', 'Yes', 'RHB', '', ''),
(76, '17-13A', 'Gan Huei Thing', 0, '', '', '0000-00-00 00:00:00.0', 448, 582, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Jojo', 'MBB: Loan decline, high commitment.\nRHB: DSR burst.\nPBB: Pending docs.  Cancelled on 26-2-18. Sent to lawyer on 1-3-18. Refund on 7-3-18.', '', 'Yes', '', '', ''),
(77, '17-16', 'Gan Wai Leong & Clarissa Yip Wei Ning', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jojo', 'MBB: Loan decline, high commitment.\nRHB: Under processing.\nPBB: Pending docs. Cancelled on 26-2-18. Sent to lawyer on 1-3-18. Refund on 7-3-18.', '', 'Yes', '', '', ''),
(78, '16-19', 'Koay Chong Keat', 800623, '012-4663208', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Jonlo', 'Jonathan follow up.\nRHB: Approved. LO signed on 30-3-18. SPA signed on 6-4-18.', '', 'Yes', 'RHB', '', ''),
(79, '17-05', 'Beh Teik Zhen', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 444,366.00 ', ' 11,109.15 ', 'Aric', 'Booking form sent to RHB KH & MBB on 26-2-18.\nRHB: Mostly cant do, income tax declare too low.\nMBB: Loan declined, DSR exceed and no further docs provide. Pending letter. Refunded on 3-7-18.', '', 'Yes', '', '', ''),
(80, '16-05', 'Yeap Chuen Hoong\n**chg unit frm 15-07', 821010, '012-4935262\n016-4151498 (wife)', 'ychyeap@yahoo.com', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 483,148.80 ', ' 12,078.72 ', 'Charles', 'MBB: Approved 84%. LO signed on 3-3-18. SPA signed on 13-3-18.', '', 'Yes', 'MBB', '', ''),
(81, '15-18', 'Lim Siang Joo & Ung Lee Ean', 0, '', '', '0000-00-00 00:00:00.0', 394, 531, '', '19%', ' 474,903.00 ', ' 11,872.58 ', 'Aric', 'RHB: Doc collected but income tax checking now. Cancellation sent to dev on 8-6-18. Refunded on 3-7-18.', '', 'Yes', '', '', ''),
(82, '13-02', 'Goh Sheau Mei & Kok Yoek Ying', 711220, '012-4846162\n012-4236162', '', '0000-00-00 00:00:00.0', 466, 596, '', '19%', ' 473,850.00 ', ' 11,846.25 ', 'Jessica', 'RHB: Approved 85%. LO signed on 10-3-18. SPA signed on 11-3-18.', '', 'Yes', 'RHB', '', ''),
(83, '13-19', 'Ong Xin Ci', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 473,850.00 ', ' 11,846.25 ', 'Charles', 'MBB: Decline, money changer business, fail scoring.\nCharles flw up with PBB Farlim. Cancelled. Cancellation sent on 12-4-18. Refunded on 21-5-18.', '', 'Yes', '', '', ''),
(84, '13A-09', 'See Lay Theng & Tang Lip San', 781101, '012-5996882', '', '0000-00-00 00:00:00.0', 426, 553, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Jocelyn', 'RHB: Approved 80%. LO signed on 22-3-18. SPA signed on 28-3-18.', '', 'Yes', 'RHB', '', ''),
(85, '13A-08', 'Ch\'ng Phaik Kiang', 691124, '011-10992022', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 471,744.00 ', ' 11,793.60 ', 'Jolin', 'RHB: Approved 85%. LO signed on 21-3-18. SPA signed on 2-4-18.', '', 'Yes', 'RHB', '', ''),
(86, '13A-08', 'Chng Paik See', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jolin', 'Booking form pass to RHB Evelyn & MBB on 19-2-18.\nCancelled on 26-2-18. Cancellation sent to lawyer on 3-4-18. resend on 4-5-18. Refunded on 18-5-18.', '', 'Yes', '', '', ''),
(87, '13-19', 'Wong Choon Yeong\nChg unit frm 17-3A on 4-4-18', 810608, '016-4183698\n016-4422957', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Charles', 'MBB: Approved 85%. LO signed on 16-5-18. SPA signed on 22-5-18.', '', 'Yes', 'MBB', '', ''),
(88, '17-08', 'Khoo Chia Poh & H\'ng Chong Keat', 770807, '014-9042246', '', '0000-00-00 00:00:00.0', 521, 638, '6.5% 3y + 9', '19%', ' 430,839.00 ', ' 10,770.98 ', 'Charles', 'RHB: Approved 75%. LO signed on 26-6-18. SPA signed on 17-7-18.\nRemark: Request receipt post to customer address.', '', 'Yes', 'RHB', '', ''),
(89, '15-09', 'Leong Kar Ngah', 731209, '012-4404789', '', '0000-00-00 00:00:00.0', 426, 553, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Sharon', 'RHB: Approved 80%. LO signed on 29-3-18. SPA signed on 23-4-18.\nRemark: Request receipt post to customer address.', '', 'Yes', 'RHB', '', ''),
(90, '15-07', 'Ho Tsao Wei', 770919, '012-7901160', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Sharon', 'RHB: Approved 85%. LO signed on 27-3-18. SPA sign on 5-4-18.', '', 'Yes', 'RHB', '', ''),
(91, '13-12', 'Ng Phek Leng', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 500,175.00 ', ' 12,504.38 ', 'Erik', 'MBB Han:\nRHB: Customer would like to cancel purchase. Cancelled on 30-3-18. Cancellation sent on 20-4-18. Resend on 4-5-18. Refunded on 21-4-18.', '', 'Yes', '', '', ''),
(92, '15-15', 'Sim Boon Kee & Chan Siak Fong', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 444,366.00 ', ' 11,109.15 ', 'Jordan', 'RHB: Customer decided cancel purchase. Cancelled on 12-3-18. Cancellation sent to lawyer on 31-3-18. Resend on 4-5-18. Refunded on 18-5-18.', '', 'Yes', '', '', ''),
(93, '16-15', 'Lee Kean Heong & Chang Weng Fang', 771107, '017-4770711\n013-4288911', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' -   ', ' -   ', 'Steve', 'RHB: Approved 85%.  LO signed on 15-4-18. SPA signed on 20-4-18. NEW loan 80% sign on 14-5-18. Sub-LO.', '', 'Yes', 'RHB', '15/04/2018', '20/04/2018'),
(94, '13-17', 'Low Me Me', 840219, '012-5819057', '', '0000-00-00 00:00:00.0', 423, 549, '', '19%', ' 516,962.25 ', ' 12,924.06 ', 'Jonlo', 'Jonathan Flw up.\nRHB: Approved 80%. LO signed on 3-4-18. SPA signed on 12-4-18.', '', 'Yes', 'RHB', '3/04/2018', '12/04/2018'),
(95, '16-07', 'Teh Siew Ang', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '19%', ' 460,161.00 ', ' 11,504.03 ', 'Erik', 'RHB: Only 1 applicant with salary ok. The rest 2 ppl self-employed week bank statement. \nCancellation sent to inma on 8-6-18. Refunded on 29-6-18.', '', 'Yes', '', '', ''),
(96, '17-15', 'Wong Mun Leong', 801115, '019-5530018', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Erik', 'MBB Han: Approved 85%. LO & SPA signed on 4-4-18.', '', 'Yes', 'MBB', '4/04/2018', '4/04/2018'),
(97, '13-16', 'Toon Meng Fong', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '19%', ' 471,744.00 ', ' 11,793.60 ', 'Charles', 'MBB: High commitment, no joint applicant. Reject letter to be issue.\nRHB: Doc email not clear, need to rearrange to meet up collect. Cancelled on 23-3-18. Cancellation sent on 12-4-18. Refunded on 21-5-18.', '', 'Yes', '', '', ''),
(98, '15-02', 'Ng Sze Yuen', 0, '', '', '0000-00-00 00:00:00.0', 466, 596, '', '19%', ' 445,419.00 ', ' 11,135.48 ', 'Charles', 'MBB: Loan decline, high commitment.\nRHB: Rejected - week income doc. Cancelled on 23-3-18. Cancellation sent on 12-4-18. Refunded on 21-5-18.', '', 'Yes', '', '', ''),
(99, '15-17', 'Ooi Swee Hiok', 0, '', '', '0000-00-00 00:00:00.0', 451, 586, '', '19%', ' 448,578.00 ', ' 11,214.45 ', 'Charles', 'MBB: Customer said she is too busy.\nRHB: Will resubmit the loan under both joint name\nCancellation sent to Inma on 20-8-18. Refunded on 17-10-18.', '', 'Yes', '', '', ''),
(100, '15-13', 'Hong Chiew Peng & Lim Tutt Leong', 700824, '012-4091513', '', '0000-00-00 00:00:00.0', 450, 585, '', '19%', ' 398,034.00 ', ' 9,950.85 ', 'Charles', 'RHB: Approved 85%. LO signed on 28-3-18. SPA signed on 12-4-18.', '', 'Yes', 'RHB', '28/03/2018', '12/04/2018'),
(101, '13-13', 'Wyro ICSR Trading', 830424, '019-4542457', '', '0000-00-00 00:00:00.0', 450, 585, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Louise', 'Booking form sent to banker on 12-3-18.\nMBB Alex: Approved 85%. LO signed on 19-4-18. SPA signed on 17-5-18.', '', 'Yes', 'MBB', '19/04/2018', '17/05/2018'),
(102, '17-13A', 'Law Poh Hiang\n**Chg unit frm 13A-19 on 15-5-18', 751206, '012-4623553', '', '0000-00-00 00:00:00.0', 449, 583, '', '19%', ' 472,797.00 ', ' 11,819.93 ', 'Jonlo', 'Jonathan Flw up.\nMBB: Approved 85%. LO & SPA signed on 23-5-18.', '', 'Yes', 'MBB', '23/05/2018', '23/05/2018'),
(103, '17-13', 'Chua Meow Sze & Lim Kean Aun', 831123, '012-4618703', 'cathrine_chua@hotmail.com', '0000-00-00 00:00:00.0', 448, 582, '', '17%', ' 483,392.00 ', ' 12,084.80 ', 'Eunice', 'RHB: Approved 85%. LO signed on 21-4-18. SPA signed on 15-5-18.', '', 'Yes', 'RHB', '21/04/2018', '15/05/2018'),
(104, '13A-07', 'Goh Hee Soon & Lum Wai Mun', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '17%', ' 529,726.75 ', ' 13,243.17 ', 'Eunice', 'RHB: DSR burst. Cancel purchase. Reject letter issue.\nMBB: High commitment, cancel purchase.\nCancellation sent to lawyer on 2-4-18. Refund on 22-3-18.', '', 'Yes', '', '', ''),
(105, '13A-06', 'Leong Bee Yuh', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '17%', ' 529,726.75 ', ' 13,243.17 ', 'Sharon', 'MBB: Loan decline, high commitment.\nRHB: DSR burst - unable to add in joint borrower.\nCancellation sent on 12-4-18. Resend on 4-5-18. Refunded on 18-5-18.', '', 'Yes', '', '', ''),
(106, '12-12', 'Khaw Keong Yeow\n**Chg unit frm 12-08 on 15-5-18\n**', 0, '', '', '0000-00-00 00:00:00.0', 394, 531, '', '17%', ' 441,477.00 ', ' 11,036.93 ', 'Jolin', 'RHB: Cancel purchase - change to Grand Ion Majestic & doc collected.\nMBB: customer change small unit, pending latest documents\nCancelled on 27-6-18. send inma on 12-7-18. Refunded on 2-8-18.', '', 'Yes', '', '', ''),
(107, '15-16', 'Ng Cheng Oo', 580415, '012-4778413', '', '0000-00-00 00:00:00.0', 449, 583, '', '17%', '#REF!', '#REF!', 'Sharon', 'RHB: Approved RM400k (cust request). LO signed on 7-4-18. SPA signed on 20-4-18.', '', 'Yes', 'RHB', '7/04/2018', '20/04/2018'),
(108, '16-12', 'Teoh Seang Hooi & Lim Joo Mey', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '17%', '#REF!', '#REF!', 'Charles & Jojo', 'MBB: 1st customer say prepare doc, further follow up no answer call & return SMS at all\nRHB: Still pending documents. Cancelled on 27-3-18. Cancellation sent on 12-4-18. Refunded on 18-5-18.', '', 'Yes', '', '', ''),
(109, '13-03', 'LWL Realty Sdn Bhd', 0, '', '', '0000-00-00 00:00:00.0', 494, 617, '', '17%', '#REF!', '#REF!', 'Jef & Alvin', 'Booking form sent to banker on 12-3-18.\nMBB: SME- Alex, customer got CCRIS problem. Will settle next week. Cancelled on 5-6-18. Sent to Inma on 5-7-18. Refunded on 17-7-18.', '', 'Yes', '', '', ''),
(110, '12-16', 'Teh Guek Cheng', 610420, '016-5162746', 'guekchengteh@gmail.com', '0000-00-00 00:00:00.0', 422, 548, '6.5% 3y + 9', '17%', ' 455,338.00 ', ' 11,383.45 ', 'Sharon', 'Cash Buyer.\nMBB: Loan RM100k. LO signed on 9-6-18. SPA signed on 21-6-18.', '', 'Yes', 'MBB', '9/06/2018', '21/06/2018'),
(111, '12-12', 'Low Ching An', 0, '', '', '0000-00-00 00:00:00.0', 0, 0, '', '17%', ' -   ', ' -   ', 'Apple', 'Refunded 6.8k by GIC on 30-5-18.', '', 'Yes', '', '', ''),
(112, '16-06', 'Lim Bee Huah', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '17%', ' 529,726.75 ', ' 13,243.17 ', 'Lester', '**Only paid 1k b.fees.\nMBB Chloe: pending further documents for appeal\nRHB: Cannot do (SPA owner NO income). Cancellation sent to Inma on 11-10-18. Refunded on 16-10-18.', '', 'Yes', '', '', ''),
(113, '19-10', 'Yeap Hooi Bin', 0, '', '', '0000-00-00 00:00:00.0', 437, 568, '', '17%', ' 471,523.00 ', ' 11,788.08 ', 'Sharon', '**Only paid 1k b.fees. Sharon sent to Han & logan.\nMBB Han: Cannot loan. Already bought Batu Kawan.\nRHB: DSR burst, NO joint borrower, NO AUM.\nCancellation sent to dev on 8-6-18. Refunded on 19-6-18.', '', 'Yes', '', '', ''),
(114, '16-12', 'Chang Chun Seong & Chang Yean Nee', 740825, '012-3776711', '', '0000-00-00 00:00:00.0', 449, 583, '6.5% 3y + 9', '17%', ' 484,471.00 ', ' 12,111.78 ', 'Marcus', '**Only paid 1k b.fees.\nRHB Evelyn: Approved 85%. LO signed on 7-6-18. SPA signed on 10-7-18.', '', 'Yes', 'RHB', '7/06/2018', '10/07/2018'),
(115, '13A-13A', 'Shanmugam A/L Ariakoundan\n**Chg unit frm 19-18 on ', 0, '', '', '0000-00-00 00:00:00.0', 448, 582, '3y m.fees', '17%', ' 483,392.00 ', ' 12,084.80 ', 'Yeap', '**Only paid 1k b.fees.\nMBB: Don?t want to submit, he prefer other banks?\nRHB: CTOS - pending settlement for motorbike.\nCancelled on 2-7-18. Refunded on 23-7-18.', '', 'Yes', '', '', ''),
(116, '19-19', 'Teh Chong Ling *no m.fees', 810311, '016-4145520', 'cl_teh168@hotmail.com', '0000-00-00 00:00:00.0', 423, 549, '6.5% 3y + 9', '17%', ' 456,417.00 ', ' 11,410.43 ', 'Marcus', 'RHB: Approved 85%. LO signed on 5-6-18. SPA signed on 20-6-18.', '', 'Yes', 'RHB', '5/06/2018', '20/06/2018'),
(117, '19-09', 'Tan Yen Chin & Choo Fang\n  * Paid 7k', 690220, '012-4529867', '', '0000-00-00 00:00:00.0', 426, 553, '3 yrs m.fee', '17%', ' 459,654.00 ', ' 11,491.35 ', 'Jocelyn', 'RHB Elaine: Approved 85%. LO signed on 29-4-18. SPA signed on 5-5-18.', '', 'Yes', 'RHB', '29/04/2018', '5/05/2018'),
(118, '19-11', 'MH Multipack Sdn Bhd * Paid 7k', 0, '', '', '0000-00-00 00:00:00.0', 364, 491, '', '17%', ' 407,862.00 ', ' 10,196.55 ', 'Kenny', 'PBB: Approved 70%. Forfeit 7k. Cancelled on 18-6-18.', '', 'Yes', '', '', ''),
(119, '15-15', 'Khor Fent Phing', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '17%', ' 484,471.00 ', ' 12,111.78 ', 'Erik', 'RHB: Accept MBB offer.\nMBB: low income, need joint borrower, still pending reply from customer, agent pls help\nCancelled on 6-6-18. send cwm. Forfeited 1k. 31-12-18', '', 'Yes', '', '', ''),
(120, '17-13A', 'Lean Phooi Leng', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '17%', ' 484,471.00 ', ' 12,111.78 ', 'Steve', 'RHB: Customer said no need loan yet. Will contact if interest.\nMBB: pending documents/ might cancel purchase-agent pls help.\nCancelled on 15-5-18. Refunded 780 on 24-10-18.', '', 'Yes', '', '', ''),
(121, '13A-15', 'Ushaa Nair A/P Pannir Selvam', 831024, '012-4564585\n012-4714771', '', '0000-00-00 00:00:00.0', 449, 583, '3y m.fees', '17%', ' 484,471.00 ', ' 12,111.78 ', 'Erik', 'MBB: Approved 85%. LO signed on 8-5-18. SPA signed on 16-5-18.', '', 'Yes', 'MBB', '8/05/2018', '16/05/2018'),
(122, '15-13A', 'Wong Sze Ling & Ong Chun How', 0, '', '', '0000-00-00 00:00:00.0', 448, 582, '3y m.fees', '17%', ' 483,392.00 ', '', 'Loy', 'RHB: DSR burst - advice to cancel CC (customer don?t want - ask try MBB 1st).\nMBB: Declined. Cancellation sent to inma on 30-7-18. Refunded on 8-8-18. Refunded by CWM.', '', 'Yes', '', '', ''),
(123, '15-02', 'Tan Hock Keong', 680122, '012-4319586', 'wisdomgrade@gmail.com', '0000-00-00 00:00:00.0', 466, 596, '6.5% 3y + 9', '17%', ' 495,078.40 ', ' 17,327.74 ', 'Joanne', 'Only paid 1k b.fees.\nMBB: Approved 90%. LO signed on 17-5-18. SPA signed on 7-6-18.', '', 'Yes', 'MBB', '17/05/2018', '7/06/2018'),
(124, '16-09', 'Fong Fui Leng', 670713, '012-5796356', '', '0000-00-00 00:00:00.0', 426, 553, '6.5% 3y + 9', '17%', ' 459,654.00 ', ' 16,087.89 ', 'Sharon', 'RHB: Approved 70%. LO signed on 28-6-18. SPA signed on 14-7-18.', '', 'Yes', 'RHB', '28/06/2018', '14/07/2018'),
(125, '16-08', 'Ong Chin Sean & Tiew Lay Fang', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '17%', ' 529,726.75 ', '', 'Jessica', '**Only paid 1k b.fees. \nRHB: Cancel purchase for VOS. Change to Grand Ion Majestic. Already inform agent.\nCancelled on 5-6-18. send inma on 25-7-18. Refunded on 13-9-18. RM780.', '', 'Yes', '', '', ''),
(126, '17-05', 'Chan Choi Keng & Simon Choong', 780105, '012-5197340\n012-5197540', '', '0000-00-00 00:00:00.0', 521, 638, '6.5% 3y + 9', '17%', ' 529,726.75 ', ' 23,837.70 ', 'Sharon', 'MBB: Approved . SPA signed on 1-6-18.', '', 'Yes', 'MBB', '', ''),
(127, '19-18', 'Teh Ay Fen', 0, '', '', '0000-00-00 00:00:00.0', 394, 531, '', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Jocelyn', 'RHB: No answer. Agent will follow up by own. \nCancelled on 5-6-18. Cancellation sent to Inma on 20-12-18. Refunded on 21-12-18.', '', 'Yes', '', '', ''),
(128, '19-03', 'Ooi Pi Tom & Lee Wan Leen', 870407, '017-5144678\n017-4839778', '', '0000-00-00 00:00:00.0', 521, 638, '7% 6y + 6y ', '13%', ' 555,255.75 ', ' 24,986.51 ', 'Eunice', 'RHB: Approved 85%. LO signed on 20-6-18. SPA signed on 13-7-18. (EUNICE)', '', 'Yes', 'RHB', '20/06/2018', '13/07/2018'),
(129, '19-08', 'Lin Shu Huat & Yeak Mei Ling', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '15%', ' 542,491.25 ', ' 24,412.11 ', 'Sharon', 'RHB: In the mid prepare documents.\nCancellation sent to Inma on 22-6-18. Refunded on 29-6-18.', '', 'Yes', '', '', ''),
(130, '19-06', 'Seah Chin Sen & Yin Woon Li', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '15%', ' 542,491.25 ', ' 24,412.11 ', 'Aric', 'RHB: In the mid prepare documents.\nMBB: Resign last month and currently unemployed, he have some financial issue.?Cancellation sent to Inma on 17-10-18. Refunded on 12-12-18.', '', 'Yes', '', '', ''),
(131, '19-20', 'Khor Soo Kiang', 570519, '016-4157280', '', '0000-00-00 00:00:00.0', 514, 634, '7% 6y + 6y ', '15%', ' 539,571.50 ', ' 24,280.72 ', 'Eunice', 'MBB: Approved 80%. LO signed on 11-6-18. SPA signed on 26-6-18. Got LOF advance letter.', '', 'Yes', 'MBB', '11/06/2018', '26/06/2018'),
(132, '19-05', 'Loh Chan Choong & Tan Seow Mei', 800119, '016-5557748\n016-5571841', 'winsonlcc@yahoo.com', '0000-00-00 00:00:00.0', 521, 638, '7% 6y + 6y ', '15%', ' 542,491.25 ', ' 24,412.11 ', 'Sharon', 'MBB: Approved 80%. SPA signed on 4-6-18. Got LOF advance letter.', '', 'Yes', 'MBB', '', ''),
(133, '19-15', 'Goh Su Hua', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '3k rebate f', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Amy', 'RHB: In the mid prepare docs. Oversea.\nMBB: collect docs from customer 20-8-18. Cancelled on 27-9-18. Cancellation sent to Inma on 5-10-18. Refunded on 17-10-18.', '', 'Yes', '', '', ''),
(134, '19-13', 'Tan Yee Kee\n**Chg Name frm Sn\'g Kok Kiang on 29-8-', 0, '', '', '0000-00-00 00:00:00.0', 450, 585, '', '13%', ' 508,950.00 ', ' 22,902.75 ', 'Jeffrey', 'MBB: Approved 70%. Still pending decision. Cancellation sent to Inma on 3-10-18. Refunded on 18-10-18.', '', 'Yes', '', '', ''),
(135, '13A-19', 'Kiew Chai Fung\n**Chg name frm Joseph Goh Ka Kee', 880823, '016-3228513', '', '0000-00-00 00:00:00.0', 423, 549, '7% 6y + 6y ', '13%', ' 478,413.00 ', ' 21,528.59 ', 'Sharon', 'MBB: Approved 85%. LO signed on 13-6-18. SPA signed on 22-6-18.', '', 'Yes', 'MBB', '13/06/2018', '22/06/2018'),
(136, '19-10', 'Fow Jun Ee', 0, '', '', '0000-00-00 00:00:00.0', 437, 568, '', '13%', ' 494,247.00 ', ' 22,241.12 ', 'Jocelyn', 'RHB: Approved 70%. Don?t want margin 70%.\nPending MBB. Cancelled on 26-6-18. Cancellation sent to Inma on 21-9-18. Refunded on 17-10-18.', '', 'Yes', '', '', ''),
(137, '13-05', 'Siah Zi Heng', 0, '', '', '0000-00-00 00:00:00.0', 494, 617, '3y m.fees', '17%', ' 512,525.00 ', ' 23,063.63 ', 'Ai Pheng', '**Only paid 1k b.fees.\nRHB: Docs collected.\nMBB: Declined. Cancelled on 27-9-18. Cancellation sent to Inma on 21-12-18. Refunded on 3-1-19. RM 800.', '', 'Yes', '', '', ''),
(138, '15-18', 'Sathisilen A/L Manogaran', 0, '', '', '0000-00-00 00:00:00.0', 394, 531, '', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Jessica', 'RHB: Rejected. High commitment.\nMBB Han: \nCancellation sent to inma on 26-6-18. Refunded on 29-6-18.', '', 'Yes', '', '', ''),
(139, '17-3A', 'Lim Siew Li', 830305, '013-4383388', '', '0000-00-00 00:00:00.0', 521, 638, '7% 6y + 6y ', '15%', ' 542,491.25 ', ' 24,412.11 ', 'Eunice', 'MBB: Approved 75%. LO signed on 26-7-18. SPA signed on 28-9-18. Got LOF advance letter.', '', 'Yes', 'MBB', '26/07/2018', '28/09/2018'),
(140, '19-11', 'Lim Xi Wei', 870620, '012-4053059', '', '0000-00-00 00:00:00.0', 364, 491, '7% 6y + 6y ', '13%', ' 427,518.00 ', ' 19,238.31 ', 'Aric', 'MBB: Approved 85%. LO signed on 3-7-18. SPA signed on 9-7-18.', '', 'Yes', 'MBB', '3/07/2018', '9/07/2018'),
(141, '19-02', 'Cheng Kooi Eng', 0, '', '', '0000-00-00 00:00:00.0', 466, 596, '', '13%', ' 518,937.60 ', ' 23,352.19 ', 'Lester', 'Cancelled on 17-7-18. Cancellation sent to Inma on 15-8-18. Refunded on 17-10-18.', '', 'Yes', '', '', ''),
(142, '19-18', 'Tan Kwan Beng & Yeoh Siew Lee', 0, '', '', '0000-00-00 00:00:00.0', 394, 531, '', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Marcus', 'RHB Evelyn & MBB Joey passed by Marcus.\nRHB: Rejected. Cancellation sent to inma on 25-7-18. Refunded on 13-9-18.', '', 'Yes', '', '', ''),
(143, '15-15', 'Phuah Chong Tean & Ang Gaik Ling', 780319, '016-4772266', '', '0000-00-00 00:00:00.0', 449, 583, '7% 6y + 6y ', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Jocelyn', 'Cash Buyer. loan @ 2019\nSPA signed on 20-7-18.', '', 'Yes', 'Cash', '', ''),
(144, '19-18', 'Ch\'ng Koe Loon\n**Chg unit frm 19-10 on 18-7-18', 820429, '012-4290718', '', '0000-00-00 00:00:00.0', 394, 531, '7% 6y + 6y ', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Jocelyn', 'RHB: Approved 70%. LO signed on 6-8-18. SPA signed on 21-9-18.\n*RM 85,753.00 Down payment installment 2 years, quarterly pay.', '', 'Yes', 'RHB', '6/08/2018', '21/09/2018'),
(145, '15-03', 'Har You Hong & Cheong Siew Hoon', 551109, '010-7609107', '', '0000-00-00 00:00:00.0', 521, 620, '7% 6y + 6y ', '13%', ' 539,595.75 ', ' 24,281.81 ', 'Jessica', 'Cash Buyer. SPA signed on 3-7-18.\n** Need to confirm the payment to dev. Around September. Working in Aus., need time to prepare.', '', 'Yes', 'Cash', '', ''),
(146, '15-18', 'Lee Jin Yue & Lim Fu Yi\n**Chg unit frm 13-16', 870922, '017-5018853', '', '0000-00-00 00:00:00.0', 394, 531, '7% 6y + 6y ', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Jocelyn', 'MBB: Approved 85%. LO & SPA signed on 23-9-18.', '', 'Yes', 'MBB', '23/09/2018', '23/09/2018'),
(147, '13-12', 'Edwin Jerome Yap', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Yeap', 'RHB: Income w/o business registration. Only can offer 70%. Customer insist go for 85%. \nMBB: Declined Cancelled on 1-8-18.\nCancellation sent to Inma on 20-8-18. Refunded on 17-10-18.', '', 'Yes', '', '', ''),
(148, '19-12', 'Ooi Chin Chin', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Jocelyn', 'MBB: Debt too high. We ask for joint but Cust request rejection letter.\nRHB: Pending docs. Cancelled on 1-8-18.  Cancellation sent to Inma on 27-9-18. Refunded on 4-12-18.', '', 'Yes', '', '', ''),
(149, '20-10', 'Kang Cheah Seng & Lee Bee Lie', 0, '', '', '0000-00-00 00:00:00.0', 437, 568, '', '13%', ' 494,247.00 ', ' 22,241.12 ', 'Kenny', 'RHB: Pending doc.\nCancelled on 28-8-18. Cancellation sent to Inma on 4-9-18. Refunded on 17-10-18.', '', 'Yes', '', '', ''),
(150, '15-13A', 'Shahunthala Devi A/P Ramachandran', 0, '', '', '0000-00-00 00:00:00.0', 448, 582, '', '13%', ' 506,688.00 ', ' 22,800.96 ', 'Jocelyn', 'Cancelled on 19-9-18. Cancellation sent to Inma on 12-11-18. Refunded on 13-12-18.', '', 'Yes', '', '', ''),
(151, '19-16', 'Hiew Wei Soon', 860909, '012-4049963', '', '0000-00-00 00:00:00.0', 449, 583, '7% 6y + 9y ', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Yeap', 'RHB: Approved 85%. LO signed on 28-9-18. SPA signed on 4-10-18.', '', 'Yes', 'RHB', '28/09/2018', '4/10/2018'),
(152, '12-12', 'Hiew Wei Soon', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '13%', ' 507,819.00 ', '', 'Yeap', 'Forfeit 1k, without reject letter. Cancellation sent to Inma on 13-12-18. Refunded on 20-12-18.', '', 'Yes', '', '', ''),
(153, '20-11', 'Ng Chee Kang', 900702, '012-4838278', '', '0000-00-00 00:00:00.0', 437, 568, '', '13%', ' 494,247.00 ', ' 22,241.12 ', 'Kang', 'RHB Own banker: Approved 80%. LO signed on 19-9-18. SPA signed on 30-9-18.', '', 'Yes', 'RHB', '19/09/2018', '30/09/2018'),
(154, '12-13', 'Ang Khai Ian', 880910, '016-4222096\n+65-93822423', 'kenneth_5385@hotmail.com', '0000-00-00 00:00:00.0', 450, 585, '7% 3y + 9y ', '13%', ' 508,950.00 ', ' 22,902.75 ', 'Jessica', 'MBB: Approved 85%. LO & SPA signed on 29-10-18.', '', 'Yes', 'MBB', '29/10/2018', '29/10/2018'),
(155, '16-08', 'Goh James Leon & Tio Chia Fong', 0, '', '', '0000-00-00 00:00:00.0', 521, 620, '18k SPA', '13%', ' 539,595.75 ', ' 24,281.81 ', 'Eunice', 'Cancelled on 19-9-18. Cancellation sent to Inma on 20-9-18. Refunded on 4-12-18.', '', 'Yes', '', '', ''),
(156, '12-08', 'Tan Yak Swee', 0, '', '', '0000-00-00 00:00:00.0', 521, 620, '18k SPA', '13%', ' 539,595.75 ', ' 24,281.81 ', 'Jimmy', 'Cancelled on 25-9-18. Cancellation sent to Inma on 4-10-18. Refunded on 13-12-18.', '', 'Yes', '', '', ''),
(157, '16-06', 'Loo Poh Gaik', 0, '', '', '0000-00-00 00:00:00.0', 521, 620, '', '13%', ' 539,595.75 ', ' 24,281.81 ', 'Louise', 'Cancelled on 17-10-18. Cancellation sent to Inma on 12-11-18. Refunded on 13-12-18.', '', 'Yes', '', '', ''),
(158, '19-02', 'Cheam Lee Keow', 840201, '012-5677329', '', '0000-00-00 00:00:00.0', 466, 596, '', '13%', ' 518,937.60 ', ' 23,352.19 ', 'Pui Yin (Loy)', 'Merdeka package. Cash buyer. SPA signed on 19-12-18.', '', 'Yes', 'Cash', '', ''),
(159, '20-18', 'Chai Wioe Heng\n*Chg unit from 20-18 on 21-12-18', 0, '', '', '0000-00-00 00:00:00.0', 394, 531, '', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Eunice', 'Docs submitted. 2111 Buyer 17-12-18 coming back to settle PTPTN. Cancellation sent to Inma on 19-2-19. Refunded on 24-4-19.', '', 'Yes', '', '', ''),
(160, '21-08', 'Ainee Aw Yang Bt Abdullah', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '13%', ' 555,255.75 ', ' 24,986.51 ', 'Aric', 'Cannot loan. Pending PBB open EF. Rejected. Cancellation sent to Inma on 25-3-19. Refunded on 10-4-19.', '', 'Yes', '', '', ''),
(161, '20-10', 'Lok Guat Har', 621220, '018-3880177', 'ghlok88@gmail.com', '0000-00-00 00:00:00.0', 437, 568, '', '13%', ' 494,247.00 ', ' 22,241.12 ', 'Joe', 'RHB: Approved 85%. LO signed on 24-11-18. SPA signed on 20-12-18.', '', 'Yes', 'RHB', '24/11/2018', '20/12/2018'),
(162, '20-08', 'Te Sin Yee & Heng Wei Loon', 891124, '017-6220815', 'tsy02255@gmail.com', '0000-00-00 00:00:00.0', 521, 638, '', '13%', ' 555,255.75 ', ' 24,986.51 ', 'Jeffrey', 'MBB: Approved 83%. LO & SPA signed on 14-12-18.', '', 'Yes', 'MBB', '14/12/2018', '14/12/2018'),
(163, '21-10', 'Ng Li Hoong & Liau Yeow Lin', 0, '', '', '0000-00-00 00:00:00.0', 437, 568, '', '13%', ' 494,247.00 ', ' 22,241.12 ', 'Joe', 'MBB: Approved 85%. LO signed on 26-12-18. SPA signed on 2-1-19.', '', 'Yes', 'MBB', '26/12/2018', '2/01/2019'),
(164, '20-09', 'Soon Lay Yong', 0, '', '', '0000-00-00 00:00:00.0', 426, 553, '', '13%', ' 481,806.00 ', ' 21,681.27 ', 'Jocelyn', 'MBB: Approved 85%. LO signed on 10-1-19. SPA signed on 12-2-19. Jocelyn 250', '', 'Yes', 'MBB', '10/01/2019', '12/02/2019'),
(165, '21-02', 'Kuan Mooi Ching & Phua Beng Huat', 0, '', '', '0000-00-00 00:00:00.0', 466, 596, '', '13%', ' 518,937.60 ', ' 23,352.19 ', 'Sharon (Ming)', 'RHB: Approved 85%. LO signed on 20-2-19. SPA signed on 27-2-19. Jocelyn 250', '', 'Yes', 'RHB', '20/02/2019', '27/02/2019'),
(166, '20-02', 'Chow Wai Tuck & Khor Wang Chin', 0, '', '', '0000-00-00 00:00:00.0', 466, 596, '', '13%', ' 518,937.60 ', ' 23,352.19 ', 'Sharon', 'Docs submitted. Rejected. Cancellation sent to Inma on 27-2-19. Refunded on 6-3-19.', '', 'Yes', '', '', ''),
(167, '21-09', 'Ong Lih Jen', 0, '', '', '0000-00-00 00:00:00.0', 426, 553, '', '13%', ' 481,806.00 ', ' 21,681.27 ', 'Sharon', 'MBB: Approved 80%. Reappeal 85%.  Cancellation sent to Inma on 1-3-19. Resubmit on 2-4-19. Cancelled on 17-9-19. Refunded on 20-9-19. Jocelyn 250', '', 'Yes', '', '', ''),
(168, '21-20', 'Ng Boon Siang & Teoh Chiew Chin', 0, '', '', '0000-00-00 00:00:00.0', 514, 634, '', '13%', ' 552,267.30 ', ' 24,852.03 ', 'Sharon', 'Docs submitted. Resubmit docs. End of Apr. Cancelled on 4-9-18. Refunded on 10-10-19.', '', 'Yes', '', '', ''),
(169, '20-01', 'Ng Yeok Poh', 0, '', '', '0000-00-00 00:00:00.0', 508, 627, '', '13%', ' 545,820.60 ', ' 24,561.93 ', 'Jeffrey', 'Rejected. Cancellation sent to Inma on 21-2-19. Refunded on 20-3-19.', '', 'Yes', '', '', ''),
(170, '21-05', 'Choong Siew Shean', 0, '', '', '0000-00-00 00:00:00.0', 494, 617, '', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Yeap', 'RHB: Approved 85%. LO & SPA signed on 18-1-19.', '', 'Yes', 'RHB', '18/01/2019', '18/01/2019'),
(171, '21-15', 'Choong Siew Shean', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Yeap', 'MBB: Approved 85%. LO & SPA signed on 18-1-19.', '', 'Yes', 'MBB', '18/01/2019', '18/01/2019');
INSERT INTO `admin_project` (`id`, `unit_no`, `purchaser_name`, `ic`, `contact`, `email`, `booking_date`, `sq_ft`, `spa_price`, `package`, `rebate`, `nettprice`, `totaldevelopercomm`, `agent`, `loanstatus`, `remark`, `form_Collected`, `bank_approved`, `lo_signed_date`, `la_signed_date`) VALUES
(172, '20-19', 'Yeoh Lay Kim', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '13%', ' 478,413.00 ', ' 21,528.59 ', 'Yeap', 'MBB: Approved 85%. LO & SPA signed on 18-1-19.', '', 'Yes', 'MBB', '18/01/2019', '18/01/2019'),
(173, '20-16', 'Choa Teong Beng', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Tiger', 'Cancelled on 16-4-19. Refunded on 11-6-19.', '', 'Yes', '', '', ''),
(174, '20-15', 'Neoh Hooi Bin & Goh Ah Choon', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Alvin', 'Rejected on 11-2-19. Cancellation sent to Inma on 20-2-19. Refunded on 24-4-19.', '', 'Yes', '', '', ''),
(175, '21-16', 'Yeoh Kok Heong & Ho Lee Fen', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Sharon', 'MBB: Approved 85%. LO signed on 15-2-19. SPA signed on 21-2-19. Jocelyn 250', '', 'Yes', 'MBB', '15/02/2019', '21/02/2019'),
(176, '21-17', 'Yeoh Kok Heong & Ho Lee Fen', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '13%', ' 478,413.00 ', ' 21,528.59 ', 'Sharon', 'MBB: Approved 85%. LO signed on 15-2-19. SPA signed on 21-2-19. Jocelyn 250', '', 'Yes', 'MBB', '15/02/2019', '21/02/2019'),
(177, '20-17', 'Tan Soo Chien', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '13%', ' 478,413.00 ', ' 21,528.59 ', 'Alex Wan', 'RHB: Approved 85%. LO signed on 30-1-19. SPA signed on 19-2-19.', '', 'Yes', 'RHB', '30/01/2019', '19/02/2019'),
(178, '21-13A', 'Vicknes Nair A/L Chandrasekaran', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Jeffrey', 'Cancellation sent to Inma on 11-3-19. Refunded on 10-4-19.', '', 'Yes', '', '', ''),
(179, '21-19', 'Woo Foong Chew *Chg unit from 20-02 on 13-3-19', 0, '', '', '0000-00-00 00:00:00.0', 423, 549, '', '13%', ' 478,413.00 ', ' 21,528.59 ', 'Sharon', 'MBB: Approved 85%. LO signed on 28-3-19. SPA signed on 17-4-19.', '', 'Yes', 'MBB', '28/03/2019', '17/04/2019'),
(180, '21-18', 'Choo Lin Chong', 0, '', '', '0000-00-00 00:00:00.0', 394, 531, '', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Jojo', 'MBB: Approved 85%. LO signed on 2-4-19. SPA signed on 22-4-19.', '', 'Yes', 'MBB', '2/04/2019', '22/04/2019'),
(181, '21-07', 'Yap Diat Siah & Chong Boon Jiuan', 0, '', '', '0000-00-00 00:00:00.0', 521, 638, '', '13%', ' 555,255.75 ', ' 24,986.51 ', 'Jojo', 'MBB: Approved 70%. LO signed on 8-4-19. SPA signed on 22-4-19.', '', 'Yes', 'MBB', '8/04/2019', '22/04/2019'),
(182, '20-12', 'Yeoh Qing En', 0, '', '', '0000-00-00 00:00:00.0', 449, 583, '', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Kenny', 'Rejected. Cancellation sent to Inma on 10-5-19. Refunded on 12-6-19.', '', 'Yes', '', '', ''),
(183, '20-15', 'Lee Kok Tat & Lim Han Neng', 0, '', '', '0000-00-00 00:00:00.0', 422, 548, '6.5% 3y + 9', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Yeap', 'MBB: Approved 80%. LO signed on 3-5-19. Want sign SPA on 17-5-19.', '', 'Yes', 'MBB', '3/05/2019', '17/05/2019'),
(184, '20-13A', 'Kenneth Leo A/L P Sylvester', 850928, '019-5756327', 'kenneth_leo85@hotmail.com', '0000-00-00 00:00:00.0', 422, 548, '6.5% 3y + 9', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Sharon', 'Credit card too high. Cancellation sent to Inma on 10-6-19. Refunded on 24-6-19.', '', 'Yes', '', '', ''),
(185, '20-18', 'Lim Choon Lan', 790301, '012-4851666', 'lcljane78@yahoo.com', '0000-00-00 00:00:00.0', 394, 531, '6.5% 3y + 9', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Carson', 'Pending 1 balance sheet docs. Should be ok.\nMBB unable proceed.Trying UOB now. Approve 60% only. Cancellation sent to Inma on 27-8-19. Refunded on 30-8-19.', '', 'Yes', '', '', ''),
(186, '20-20', 'Ang Chun Siang & Mok Yeop Sin', 810219, '012-4611122\n012-4060112', '', '0000-00-00 00:00:00.0', 514, 634, '6.5% 3y + 9', '13%', ' 552,267.30 ', ' 24,852.03 ', 'Sharon', 'Increase FD. In progress preparing. Cancellation WA to Inma on 14-8-19. Refunded on 20-8-19.', '', 'Yes', '', '', ''),
(187, '21-12', 'Saw Chong Choo & Loke Min Choo', 981211, '016-4845670\n014-9433430', '', '0000-00-00 00:00:00.0', 449, 583, '6.5% 3y + 9', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Kenneth', 'MBB: Approved 70%. LO signed on 9-5-19. SPA signed on 23-5-19.', '', 'Yes', 'MBB', '9/05/2019', '23/05/2019'),
(188, '21-08', 'Tan Theng Theng', 681003, '012-4941918', 'theng2tan@hotmail.com', '0000-00-00 00:00:00.0', 521, 638, '6.5% 3y + 9', '13%', ' 555,255.75 ', ' 24,986.51 ', 'Brian', 'MBB: Approved 85%. LO signed on 18-6-19. SPA signed on 20-6-19.', '', 'Yes', 'MBB', '18/06/2019', '20/06/2019'),
(189, '20-07', 'Lee Chung Wah', 780316, '014-3286833', 'leech316@yahoo.com', '0000-00-00 00:00:00.0', 521, 638, '6.5% 3y + 9', '13%', ' 555,255.75 ', ' 24,986.51 ', 'Eunice', 'MBB: Approved 85%. LO & SPA signed on 3-6-19.', '', 'Yes', 'MBB', '3/06/2019', '3/06/2019'),
(190, '20-05', 'Loo Seng Hooi', 830325, '018-2002055\n012-5276180', 'den_loo@hotmail.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Kimberlyn', 'MBB: Approved 85%. LO signed on 16-6-19. SPA signed on 18-6-19.', '', 'Yes', 'MBB', '16/06/2019', '18/06/2019'),
(191, '20-16', 'Low Siew Hoay & Tan Hong Seng', 911107, '016-4667317\n018-4720687', 'lowsiewhoay@hotmail.com\nhstkevin@yahoo.com', '0000-00-00 00:00:00.0', 422, 548, '6.5% 3y + 9', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Joe', 'MBB: Approved 85%. LO signed on 27-6-19. SPA signed on 9-7-19.', '', 'Yes', 'MBB', '27/06/2019', '9/07/2019'),
(192, '21-06', 'Khor Hock Sin', 801004, '012-5525451', 'selestong86@gmail.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Khor', 'Loan rejected. Cancellation sent to Inma on 14-10-19. Refunded on 18-10-19.', '', 'Yes', '', '', ''),
(193, '20-12', 'Chow Joo Siang & Goh Kuan Lin', 800725, '016-4867700\n012-5229089', 'chow_js@yahoo.com\nlyngoh85@gmail.com', '0000-00-00 00:00:00.0', 449, 583, '6.5% 3y + 9', '13%', ' 507,819.00 ', ' 22,851.86 ', 'Sharon', 'MBB: 85%. LO signed on 5-10-19. SPA signed on 16-10-19.', '', 'Yes', 'MBB', '5/10/2019', '16/10/2019'),
(194, '21-06', 'Ang Ee Lynn', 810606, '65-86156829', 'elynn_ang@yahoo.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Kimberlyn', '', '', 'Yes', '', '', ''),
(195, '21-13A', 'Beh Chong Yauw', 780714, '016-4144451', 'danielbehcy@gmail.com', '0000-00-00 00:00:00.0', 422, 548, '6.5% 3y + 9', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Alvin', 'SPA signed on 21-11-19. Loan on 2020.', '', 'Yes', '', '', ''),
(196, '21-13', 'Tan Lee Lee', 701002, '012-4842741', 'mapics_lily@yahoo.com', '0000-00-00 00:00:00.0', 450, 585, '6.5% 3y + 9', '13%', ' 508,950.00 ', ' 22,902.75 ', 'Leong', 'MBB: 85%. LO signed on 1-11-19. SPA signed on 14-11-19.', '', 'Yes', 'MBB', '1/11/2019', '14/11/2019'),
(197, '20-13A', 'Chua Boon Kui', 770618, '012-5556601', 'bkbkchua@gmail.com', '0000-00-00 00:00:00.0', 422, 548, '6.5% 3y + 9', '13%', ' 477,282.00 ', ' 21,477.69 ', 'Jeffrey', 'MBB: 85%. LO & SPA signed together.', '', 'Yes', 'MBB', '', ''),
(198, '20-18', 'Sokalingam Meena A/P Muthalagappa', 550330, '012-4760366', '', '0000-00-00 00:00:00.0', 394, 531, '6.5% 3y + 9', '13%', ' 462,753.00 ', ' 20,823.89 ', 'Joe', 'Cash Buyer. SPA signed on 6-11-19.', '', 'Yes', 'Cash', '', ''),
(199, '20-13', 'Tan Chu Yeang', 721008, '012-4842262', 'calvincytan@yahoo.com', '0000-00-00 00:00:00.0', 450, 585, '6.5% 3y + 9', '13%', ' 508,950.00 ', ' 22,902.75 ', 'Joe', 'Cancellation sent to Inma on 3-12-19. Refunded on 5-12-19.', '', 'Yes', '', '', ''),
(200, '20-01', 'Lim Cheng Seang', 720304, '012-4862988', 'limcs610304@gmail.com', '0000-00-00 00:00:00.0', 508, 627, '6.5% 3y + 9', '13%', ' 545,820.60 ', ' 24,561.93 ', 'Eunice', 'Pending approval.', '', 'Yes', '', '', ''),
(201, '21-09', 'Lim Tze Ni & Lee Seng Hong', 891027, '012-4741027\n012-4817508', 'kss1279@hotmail.com\nlimtni@hotmail.com', '0000-00-00 00:00:00.0', 426, 553, '6.5% 3y + 9', '13%', ' 481,806.00 ', ' 21,681.27 ', 'Crystal Ooi', '', '', 'Yes', '', '', ''),
(202, '21-01', 'Lim Kuang Chai', 730322, '012-5644536', 'kuangchai@yahoo.com', '0000-00-00 00:00:00.0', 508, 627, '6.5% 3y + 9', '13%', ' 545,820.60 ', ' 24,561.93 ', 'Joe', 'MBB: 85%. LO signed on 2-12-19. SPA signed on 20-12-19.', '', 'Yes', 'MBB', '2/12/2019', '20/12/2019'),
(203, '20-02', 'Yew Seng Kai', 0, '', '', '0000-00-00 00:00:00.0', 466, 596, '6.5% 3y + 9', '13%', ' 518,937.60 ', ' 23,352.19 ', 'Marcus', 'Rejected. Refunded by GIC on 4-11-19.', '', 'Yes', '', '', ''),
(204, '21-03', 'Lien Chong Guan', 751229, '019-4167818', 'cglien2010@gmail.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Sharon', 'MBB: 85%. LO signed on 8-11-19. SPA signed on 9-12-19.', '', 'Yes', 'MBB', '8/11/2019', '9/12/2019'),
(205, '20-03', 'Samjin Marketing Malaysia Snd Bhd', 650814, '019-3199922', 'skenglee@gmail.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Alvin', 'SPA signed on 7-12-19 at KL. Loan on 2020.', '', 'Yes', '', '', ''),
(206, '20-02', 'Puncak Industrial Supply Sdn Bhd *Chg unit frm 15-', 650814, '019-3199922', 'skenglee@gmail.com', '0000-00-00 00:00:00.0', 466, 596, '6.5% 3y + 9', '13%', ' 518,937.60 ', ' 23,352.19 ', 'Alvin', 'SPA signed on 7-12-19 at KL. Loan on 2020.', '', 'Yes', '', '', ''),
(207, '20-20', 'Yeoh Khi Sheng', 930412, '016-4418423', '', '0000-00-00 00:00:00.0', 514, 634, '6.5% 3y + 9', '13%', ' 552,267.30 ', ' 24,852.03 ', 'Eunice', 'Cancellation sent to Inma on 13-11-19. Refunded on 20-11-19.', '', 'Yes', '', '', ''),
(208, '21-20', 'Ang Gaik Hwa', 861127, '012-4232931', '', '0000-00-00 00:00:00.0', 514, 634, '6.5% 3y + 9', '13%', ' 552,267.30 ', ' 24,852.03 ', 'Jojo', 'Cancellation sent to Inma on 27-12-19.', '', 'Yes', '', '', ''),
(209, '20-3A', 'Ng Wah Nam', 680704, '012-4293709', 'bernard.ng@cleanmarklabels.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 524,875.00 ', ' 23,619.38 ', 'Alvin', 'Cancellation sent to Inma on 18-11-19.', '', 'Yes', '', '', ''),
(210, '20-06', 'Ng Wah Nam', 680704, '012-4293709', 'bernard.ng@cleanmarklabels.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 524,875.00 ', ' 23,619.38 ', 'Alvin', 'Cancellation sent to Inma on 18-11-19.', '', 'Yes', '', '', ''),
(211, '21-09', 'Goh Eng Joo *Chg unit frm 21-3A & 13-16', 780616, '016-4585248', 'wendy.fong0302@gmail.com', '0000-00-00 00:00:00.0', 426, 553, '6.5% 3y + 9', '13%', ' 481,806.00 ', ' 21,681.27 ', 'Alvin', 'UOB: 57%. Down payment Installment 3y. LO signed on 28-12-19.', '', 'Yes', 'UOB', '', ''),
(212, '20-3A', 'Tan ChinYung', 910809, '016-3379165', '', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Bertha', 'Did not pay b.fees', '', 'Yes', '', '', ''),
(213, '20-06', 'Tan Heng Wei & Tan Chan Yee', 890508, '012-4365108', '', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Bertha', 'Cancellation sent to Inma on 24-12-19.', '', 'Yes', '', '', ''),
(214, '16-08', 'Tan Lay Tin', 670614, '017-3782303', '', '0000-00-00 00:00:00.0', 521, 638, '6.5% 3y + 9', '13%', ' 555,255.75 ', '', 'Crystal Ooi', 'MBB: 160k. ', '', 'Yes', '', '', ''),
(215, '21-3A', 'Daniel Tan Wei Min', 911125, '016-4997265', 'danieltan27@hotmail.com', '0000-00-00 00:00:00.0', 494, 617, '6.5% 3y + 9', '13%', ' 537,225.00 ', ' 24,175.13 ', 'Yeap', 'MBB: 85%. LO & SPA signed on 22-11-19.', '', 'Yes', 'MBB', '22/11/2019', '22/11/2019'),
(216, '21-11', 'Lim Chee Teong', 821102, '65-9773 3752', '', '0000-00-00 00:00:00.0', 364, 491, '6.5% 3y + 9', '11%', ' 437,346.00 ', ' 19,680.57 ', 'Jeffrey', 'MBB: 85%. LO & SPA signed on 24-12-19.', '', 'Yes', 'MBB', '24/12/2019', '24/12/2019'),
(217, '20-13', 'Se Poh Wah & Khor Chuan Chye', 820413, '012-4622543\n016-7762251', '', '0000-00-00 00:00:00.0', 450, 585, '6.5% 3y + 9', '13%', ' 508,950.00 ', ' 22,902.75 ', 'Jordan', 'MBB: 85%.', '', 'Yes', '', '', ''),
(218, '20-20', 'Khor Hai Hin', 700130, '012-4201306', 'hhkhor22@gmail.com', '0000-00-00 00:00:00.0', 514, 634, '6.5% 3y + 9', '13%', ' 552,267.30 ', ' 24,852.03 ', 'Alvin', '', '', 'Yes', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `advance_slip`
--

CREATE TABLE `advance_slip` (
  `id` int(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `amount` decimal(65,2) NOT NULL,
  `status` varchar(255) NOT NULL,
  `check_id` varchar(255) DEFAULT NULL,
  `receive_status` varchar(255) NOT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NULL DEFAULT NULL ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank_list`
--

CREATE TABLE `bank_list` (
  `id` int(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank_list`
--

INSERT INTO `bank_list` (`id`, `bank_name`) VALUES
(1, 'Affin Bank Berhad'),
(2, 'Affin Hwang Investment Bank Berhad'),
(3, 'Affin Islamic Bank Berhad'),
(4, 'Al Rajhi Banking & Investment Corporation (Malaysia) Berhad'),
(5, 'Alliance Bank Malaysia Berhad'),
(6, 'Alliance Investment Bank Berhad'),
(7, 'Alliance Islamic Bank Berhad'),
(8, 'AmBank (M) Berhad'),
(9, 'AmInvestment Bank Berhad'),
(10, 'AmIslamic Bank Berhad'),
(11, 'Asian Finance Bank Berhad'),
(12, 'Bangkok Bank Berhad'),
(13, 'Bank Islam Malaysia Berhad'),
(14, 'Bank Muamalat Malaysia Berhad'),
(15, 'Bank of America Malaysia Berhad'),
(16, 'Bank of China (Malaysia) Berhad'),
(17, 'Bank of Tokyo-Mitsubishi UFJ (Malaysia) Berhad'),
(18, 'BNP Paribas Malaysia Berhad'),
(19, 'China Construction Bank (Malaysia) Berhad'),
(20, 'CIMB Bank Berhad'),
(21, 'CIMB Investment Bank Berhad'),
(22, 'CIMB Islamic Bank Berhad'),
(23, 'Citibank Berhad'),
(24, 'Deutsche Bank (Malaysia) Berhad'),
(25, 'Hong Leong Bank Berhad'),
(26, 'Hong Leong Investment Bank Berhad'),
(27, 'Hong Leong Islamic Bank Berhad'),
(28, 'HSBC Amanah Malaysia Berhad'),
(29, 'HSBC Bank Malaysia Berhad'),
(30, 'India International Bank Malaysia Berhad'),
(31, 'Industrial and Commercial Bank of China (Malaysia) Berhad'),
(32, 'J.P. Morgan Chase Bank Berhad'),
(33, 'KAF Investment Bank Berhad'),
(34, 'Kenanga Investment Bank Berhad'),
(35, 'Kuwait Finance House (Malaysia) Berhad'),
(36, 'Malayan Banking Berhad'),
(37, 'Maybank Investment Bank Berhad'),
(38, 'Maybank Islamic Berhad'),
(39, 'MIDF Amanah Investment Bank Berhad'),
(40, 'Mizuho Corporate Bank (Malaysia) Berhad'),
(41, 'National Bank of Abu Dhabi Malaysia Berhad'),
(42, 'OCBC Al-Amin Bank Berhad'),
(43, 'OCBC Bank (Malaysia) Berhad'),
(44, 'Public Bank Berhad'),
(45, 'Public Investment Bank Berhad'),
(46, 'Public Islamic Bank Berhad'),
(47, 'RHB Bank Berhad'),
(48, 'RHB Investment Bank Berhad'),
(49, 'RHB Islamic Bank Berhad'),
(50, 'Standard Chartered Bank Malaysia Berhad'),
(51, 'Standard Chartered Saadiq Berhad'),
(52, 'Sumitomo Mitsui Banking Corporation Malaysia Berhad'),
(53, 'The Bank of Nova Scotia Berhad'),
(54, 'The Royal Bank of Scotland Berhad'),
(55, 'United Overseas Bank (Malaysia) Berhad');

-- --------------------------------------------------------

--
-- Table structure for table `commission`
--

CREATE TABLE `commission` (
  `id` int(11) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `upline` varchar(255) NOT NULL,
  `commission` decimal(65,1) NOT NULL,
  `purchaser_name` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `check_id` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `unit_no` varchar(255) DEFAULT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp(1) NULL DEFAULT NULL ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commission`
--

INSERT INTO `commission` (`id`, `loan_uid`, `upline`, `commission`, `purchaser_name`, `details`, `receive_status`, `check_id`, `project_name`, `unit_no`, `booking_date`, `date_created`, `date_updated`) VALUES
(116, 'f373728c27e09d6d0e87b9ae1cffed60', 'Eddie Song', '75.0', 'a,b', '1st Commission', 'COMPLETED', NULL, 'Project A1', '3-9', '2020-02-07', '2020-02-10 06:45:18', NULL),
(117, 'f373728c27e09d6d0e87b9ae1cffed60', '', '75.0', 'a,b', '1st Commission', 'COMPLETED', NULL, 'Project A1', '3-9', '2020-02-07', '2020-02-10 06:45:18', NULL),
(118, 'f373728c27e09d6d0e87b9ae1cffed60', 'Ai Pheng', '500.0', 'a,b', '1st Commission', 'COMPLETED', NULL, 'Project A1', '3-9', '2020-02-07', '2020-02-10 06:45:18', NULL),
(119, 'db883d19a2da12194e04fe634f174044', 'Eddie Song', '75.0', 'a,b,c', '1st Commission', 'COMPLETED', NULL, 'Project A1', '3-8', '2020-02-07', '2020-02-10 06:45:18', NULL),
(120, 'db883d19a2da12194e04fe634f174044', '', '75.0', 'a,b,c', '1st Commission', 'COMPLETED', NULL, 'Project A1', '3-8', '2020-02-07', '2020-02-10 06:45:18', NULL),
(121, 'db883d19a2da12194e04fe634f174044', 'Ai Pheng', '500.0', 'a,b,c', '1st Commission', 'COMPLETED', NULL, 'Project A1', '3-8', '2020-02-07', '2020-02-10 06:45:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `purchaser_name` varchar(255) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `unit_no` varchar(255) DEFAULT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `project_handler` varchar(255) DEFAULT NULL,
  `claims_status` varchar(255) DEFAULT NULL,
  `claims_date` varchar(255) DEFAULT NULL,
  `invoice_name` varchar(255) DEFAULT NULL,
  `invoice_type` varchar(255) DEFAULT NULL,
  `item` varchar(255) NOT NULL,
  `item2` varchar(255) DEFAULT NULL,
  `item3` varchar(255) DEFAULT NULL,
  `item4` varchar(255) DEFAULT NULL,
  `item5` varchar(255) DEFAULT NULL,
  `remark` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `amount2` decimal(65,0) DEFAULT NULL,
  `amount3` decimal(65,0) DEFAULT NULL,
  `amount4` decimal(65,0) DEFAULT NULL,
  `amount5` decimal(65,0) DEFAULT NULL,
  `unit` varchar(255) NOT NULL,
  `unit2` varchar(255) NOT NULL,
  `unit3` varchar(255) NOT NULL,
  `unit4` varchar(255) NOT NULL,
  `unit5` varchar(255) NOT NULL,
  `project` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `check_id` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_multi`
--

CREATE TABLE `invoice_multi` (
  `id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `project_handler` varchar(255) NOT NULL,
  `invoice_name` varchar(255) NOT NULL,
  `invoice_type` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `claims_status` varchar(255) NOT NULL,
  `receive_date` varchar(255) DEFAULT 'NULL',
  `check_id` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NULL DEFAULT NULL ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_multi`
--

INSERT INTO `invoice_multi` (`id`, `project_name`, `unit_no`, `booking_date`, `project_handler`, `invoice_name`, `invoice_type`, `item`, `amount`, `charges`, `receive_status`, `claims_status`, `receive_date`, `check_id`, `final_amount`, `bank_account_holder`, `bank_name`, `bank_account_no`, `date_created`, `date_updated`) VALUES
(10, 'Project A1', '3-9,3-8', '2020-02-10', 'mike', 'Invoice', 'Others', 'raspbery,raspbery', '2500,2250', 'NO', 'PENDING', '1,1', 'NULL', NULL, '2500,2250', 'GIC Holding', 'Public Bank Sdn Bhd', '123456789', '2020-02-10 06:26:54.1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loan_status`
--

CREATE TABLE `loan_status` (
  `id` bigint(25) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `purchaser_name` varchar(255) NOT NULL,
  `ic` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `sq_ft` varchar(255) NOT NULL,
  `spa_price` varchar(255) NOT NULL,
  `package` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL,
  `rebate` varchar(255) NOT NULL,
  `extra_rebate` varchar(255) NOT NULL,
  `nettprice` varchar(255) NOT NULL,
  `totaldevelopercomm` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `loanstatus` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `bform_Collected` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `lawyer` varchar(255) NOT NULL,
  `pending_approval_status` varchar(255) NOT NULL,
  `bank_approved` varchar(255) NOT NULL,
  `lo_signed_date` varchar(255) NOT NULL,
  `la_signed_date` varchar(255) NOT NULL,
  `spa_signed_date` varchar(255) NOT NULL,
  `fullset_completed` varchar(255) NOT NULL,
  `cash_buyer` varchar(255) NOT NULL,
  `cancelled_booking` varchar(255) NOT NULL,
  `case_status` varchar(255) NOT NULL,
  `event_personal` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `agent_comm` varchar(255) NOT NULL,
  `upline1` varchar(255) NOT NULL,
  `upline2` varchar(255) NOT NULL,
  `pl_name` varchar(255) NOT NULL,
  `hos_name` varchar(255) NOT NULL,
  `lister_name` varchar(255) NOT NULL,
  `ul_override` varchar(255) NOT NULL,
  `uul_override` varchar(255) NOT NULL,
  `pl_override` varchar(255) NOT NULL,
  `hos_override` varchar(255) NOT NULL,
  `lister_override` varchar(255) NOT NULL,
  `admin1_override` varchar(255) NOT NULL,
  `admin2_override` varchar(255) NOT NULL,
  `admin3_override` varchar(255) NOT NULL,
  `gic_profit` varchar(255) NOT NULL,
  `total_claimed_dev_amt` int(255) NOT NULL,
  `total_bal_unclaim_amt` int(255) NOT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `pdf_date` varchar(255) DEFAULT NULL,
  `status_1st` varchar(255) NOT NULL,
  `1st_claim_amt` varchar(255) NOT NULL,
  `sst1` varchar(255) DEFAULT NULL,
  `request_date1` varchar(255) DEFAULT NULL,
  `check_no1` varchar(255) DEFAULT NULL,
  `receive_date1` varchar(255) DEFAULT NULL,
  `status_2nd` varchar(255) NOT NULL,
  `2nd_claim_amt` varchar(255) NOT NULL,
  `sst2` varchar(255) DEFAULT NULL,
  `request_date2` varchar(255) DEFAULT NULL,
  `check_no2` varchar(255) DEFAULT NULL,
  `receive_date2` varchar(255) DEFAULT NULL,
  `status_3rd` varchar(255) NOT NULL,
  `3rd_claim_amt` varchar(255) NOT NULL,
  `sst3` varchar(255) DEFAULT NULL,
  `request_date3` varchar(255) DEFAULT NULL,
  `check_no3` varchar(255) DEFAULT NULL,
  `receive_date3` varchar(255) DEFAULT NULL,
  `status_4th` varchar(255) NOT NULL,
  `4th_claim_amt` varchar(255) NOT NULL,
  `sst4` varchar(25) DEFAULT NULL,
  `request_date4` varchar(255) DEFAULT NULL,
  `check_no4` varchar(255) DEFAULT NULL,
  `receive_date4` varchar(255) DEFAULT NULL,
  `status_5th` varchar(255) NOT NULL,
  `5th_claim_amt` varchar(255) NOT NULL,
  `sst5` varchar(25) DEFAULT NULL,
  `request_date5` varchar(255) DEFAULT NULL,
  `check_no5` varchar(255) DEFAULT NULL,
  `receive_date5` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loan_status`
--

INSERT INTO `loan_status` (`id`, `project_name`, `loan_uid`, `unit_no`, `purchaser_name`, `ic`, `contact`, `email`, `booking_date`, `sq_ft`, `spa_price`, `package`, `discount`, `rebate`, `extra_rebate`, `nettprice`, `totaldevelopercomm`, `agent`, `loanstatus`, `remark`, `bform_Collected`, `payment_method`, `lawyer`, `pending_approval_status`, `bank_approved`, `lo_signed_date`, `la_signed_date`, `spa_signed_date`, `fullset_completed`, `cash_buyer`, `cancelled_booking`, `case_status`, `event_personal`, `rate`, `agent_comm`, `upline1`, `upline2`, `pl_name`, `hos_name`, `lister_name`, `ul_override`, `uul_override`, `pl_override`, `hos_override`, `lister_override`, `admin1_override`, `admin2_override`, `admin3_override`, `gic_profit`, `total_claimed_dev_amt`, `total_bal_unclaim_amt`, `pdf`, `pdf_date`, `status_1st`, `1st_claim_amt`, `sst1`, `request_date1`, `check_no1`, `receive_date1`, `status_2nd`, `2nd_claim_amt`, `sst2`, `request_date2`, `check_no2`, `receive_date2`, `status_3rd`, `3rd_claim_amt`, `sst3`, `request_date3`, `check_no3`, `receive_date3`, `status_4th`, `4th_claim_amt`, `sst4`, `request_date4`, `check_no4`, `receive_date4`, `status_5th`, `5th_claim_amt`, `sst5`, `request_date5`, `check_no5`, `receive_date5`, `date_created`, `date_updated`) VALUES
(20, 'Project A1', 'f373728c27e09d6d0e87b9ae1cffed60', '3-9', 'a,b', '11,22', '01155670433,01155670433', 'mustaripro96@gmail.com,dd', '2020-02-07', '1200', '10000', '131', '', '', '', '100000', '5000', 'Ai Pheng', '', '', '', '', '', '', '', '2020-02-07', '2020-02-07', '2020-02-07', '', '', '', 'COMPLETED', '', '3', '3000', 'Eddie Song', '', '', '', '', '150', '150', '450', '150', '0', '15', '15', '15', '1055', 2500, 2500, NULL, NULL, 'MULTI', '2500', 'NO', '10-02-2020', '123', '', '', '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '2020-02-07 04:06:52', '2020-02-10 06:45:18'),
(21, 'Project A1', 'db883d19a2da12194e04fe634f174044', '3-8', 'a,b,c', '11,22,33', '01155670433,01155670433,1111', 'mustaripro96@gmail.com,aada,33', '2020-02-07', '1200', '10000', '131', '', '', '', '100000', '4500', 'Ai Pheng', '', '', '', 'GIC Merchant', '', '', '', '2020-02-07', '2020-02-07', '2020-02-07', '', '', '', 'COMPLETED', '', '3', '3000', 'Eddie Song', '', '', '', '', '150', '150', '450', '150', '0', '15', '15', '15', '1055', 2250, 2250, NULL, NULL, 'MULTI', '2250', 'NO', '10-02-2020', '123', '', '', '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '2020-02-07 04:08:38', '2020-02-10 06:45:18'),
(22, 'Project A1', 'b92252ef92b9e9b9129253cc690e7d1d', 'DC-001', 'a,b,c,d', '11,22,33,44', '01155670433,01155670433,33,44', 'mustaripro96@gmail.com,22,33,44', '2020-02-07', '1200', '10000', '131', '', '', '', '100000', '4000', 'Ai Pheng', '', '', '', '', '', '', '', '2020-02-07', '2020-02-07', '2020-02-07', '', '', '', 'COMPLETED', '', '3', '3000', 'Eddie Song', '', '', '', '', '150', '150', '450', '150', '0', '15', '15', '15', '1055', 0, 4000, NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '2020-02-07 04:09:32', '2020-02-10 06:25:35'),
(23, 'Project A1', '76cb64f86c50f6ee8de38d1e97ff6f7c', '19-01', 'Clark Kent', '965207-02-8569', '+6019-242342', 'mustaripro96@gmail.com', '2020-02-07', '1200', '10000', '131', '', '', '', '100000', '3500', 'Alex Lim', '', '', '', 'GIC Merchant', '', '', '', '2020-02-07', '2020-02-07', '2020-02-07', '', '', '', 'COMPLETED', '', '3', '3000', 'Eddie Song', ' Jeffrey ', '', '', '', '150', '150', '450', '150', '0', '15', '15', '15', '1055', 0, 3500, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '2020-02-07 04:20:08', '2020-02-10 03:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `payment_list`
--

CREATE TABLE `payment_list` (
  `id` int(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_list`
--

INSERT INTO `payment_list` (`id`, `payment_method`) VALUES
(1, 'Developer Merchant'),
(2, 'GIC Merchant'),
(3, 'TT to Developer'),
(4, 'TT to GIC');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` bigint(20) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `add_projectppl` varchar(255) NOT NULL,
  `claims_no` varchar(255) NOT NULL,
  `data_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `data_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `project_name`, `add_projectppl`, `claims_no`, `data_created`, `data_updated`) VALUES
(6, 'Project A1', 'mike', '2', '2020-02-06 08:34:33', '2020-02-06 08:34:33');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `upline1` varchar(255) DEFAULT 'NULL',
  `upline2` varchar(255) DEFAULT 'NULL',
  `username` varchar(255) DEFAULT 'NULL',
  `full_name` varchar(255) DEFAULT NULL,
  `user_type` int(11) DEFAULT 3,
  `password` varchar(255) NOT NULL DEFAULT '''48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057''',
  `salt` varchar(255) NOT NULL DEFAULT '''5e29f9c1c505e3e6c954240573a4c4d15c5acf93''',
  `date_created` timestamp(1) NULL DEFAULT NULL,
  `ic` varchar(255) DEFAULT NULL,
  `birth_month` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_no` int(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `upline1`, `upline2`, `username`, `full_name`, `user_type`, `password`, `salt`, `date_created`, `ic`, `birth_month`, `contact`, `email`, `bank`, `bank_no`, `address`) VALUES
(1, 'Eddie Song', NULL, 'Ai Pheng', 'Tan Ai Pheng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '670130-07-5098', 'Jan', '012-5761433', 'aiphengtan123@gmail.com', 'PBB', 6465, '42-3-23 Jalan Van Praagh, 11600 Pulau Pinang'),
(2, 'Eddie Song', ' Jeffrey ', 'Alex Lim', 'Lim Khim Hong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '850524-07-5339', 'May', '014-5992777', 'alexlim747@gmail.com', 'PBB', 3191, ''),
(3, NULL, 'Eddie Song', 'Alex Tang', 'Tang Kiam How', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '760916-07-5881', 'Sep', '012-4306611', 'alextang6611@gmail.com', 'PBB', 3203, '37, Jalan Krian, 10400 Georgetown, Penang.'),
(4, 'Jonathan', 'Joe ', 'Alex Wan', 'Wan Khye Chiat', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '870817-07-5293', 'Aug', '016-5928863', 'alexdental87@gmail.com', 'PBB', 6474, '32A, Changkat Sg. Ara 20, 11900 Bayan Lepas, Penang.'),
(5, 'Eddie Song', 'Patrick', 'Alison', 'Ch\'ng Lay Pheng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '720623-07-5486', 'Jun', '012-5526793', 'chngalison@gmail.com', 'PBB', 6469, '561 Jalan Masjid Negeri, 11600 Greenland, Penang'),
(6, 'Eddie Song', 'Patrick', 'Alvin', 'Tan Beng Hwa', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '701103-07-5833', 'Nov', '012-5325777', 'gicalvin777@gmail.com', 'PBB', 4466, '43 Jalan Sungai Ara Satu, Taman Sungai Ara, 11900 Bayan Lepas, Pulau Pinang.'),
(7, 'Eddie Song', 'Patrick', 'Aric', 'Teh Kok Teng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '920704-07-5329', 'Jul', '012-9488378', 'aricteng@gmail.com', 'PBB', 6423, ''),
(8, 'Eddie Song', NULL, 'Bertha Heng', 'Heng Bei Kuang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2017-09-19 16:00:00.0', '810925-02-5352', 'Sep', '012-4251783', 'berthaheng@gmail.com ', 'PBB', 4936, '12A, Lorong Seri Senangan 2, Taman Seri Senangan, 13050, Butterworth, Penang.'),
(9, 'Eddie Song', NULL, 'Joanne Boey', 'Boey Ying', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '740901-07-5290', 'Sep', '016-4198345', 'joanneboeygic@gmail.com', 'PBB', 6907, '26B-16-11, Lengkok Erskine, Taman Kristal, 10470 Tanjung Tokong, Penang'),
(10, 'Jonathan', 'Joe ', 'James Boon', 'Lim Chin Boon', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-12-03 16:00:00.0', '921217-02-5687', 'Dec', '012-4418613', 'gicjamesboon@gmail.com', 'PBB', 4616, 'C-04-01, Rifle Range, Jalan Padang Tembak, Ayer Itam, 11400, Penang'),
(11, 'Eddie Song', 'Jeffrey', 'Brian Ho', 'Ho Pin Chuan', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '820115-07-5277', 'Nov', '012-4280115', 'brianhpc82@gmail.com', 'PBB', 4803, '62-4-8 Lebuh Sg Pinang 1, Pinang Court 2 ,11600 Penang.'),
(12, 'Patrick', 'Aric', 'Caron Wong', 'Caron Wong ', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '920515-07-5725', 'May', '016-5515470', 'caronwong1505@gmail.com', 'PBB', 4658, 'Q1-5, Desa Permai Indah , Jalan Helang, Sungai Dua 11700 Pulau Pinang.'),
(13, 'Jeffrey', 'Jordan', 'Carson Chuah', 'Chuah Eng Hong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-08-14 16:00:00.0', '960106-08-5611', 'Jan', '018-4096321', 'Enghong96c@gmail.com', 'PBB', 6467, '63B-6-11, Jalan Lenggong Jelutong, 11600 Penang.'),
(14, 'Eddie Song', 'Alex Tang', 'Cason Ong', 'Ong Chen Aun', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-02 16:00:00.0', '891220-07-5613', 'Dec', '017-5577991', 'Cason.ong@gmail.com', 'PBB', 4626, '12, Changkat Minden Lorong 11, Gelugor, Penang'),
(15, 'Aric', 'Caron', 'Catherine Wong', 'Catherine Wong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-12-31 16:00:00.0', '981121-07-6468', 'Nov', '016-4673181', 'wongcatherine3@gmail.com', 'PBB', 6483, 'Q1-5, Desa Permai Indah , Jalan Helang, Sungai Dua 11700 Pulau Pinang.'),
(16, 'Eddie Song', 'Jeffrey', 'Chanel Hoe', 'Hoe Mei Ling', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '840612-01-6292', 'Jun', '018-2248586', 'chanelhoe612@gmail.com', 'PBB', 6405, ''),
(17, NULL, 'Eddie Song', 'Charles Chew', 'Charles Chew Chu Kwang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '790822-07-5829', 'Aug', '012-7230447', '', 'PBB', 6908, '11A, Jalan Pantai Jerjak 8, Sungai Nibong, 11900 Bayan Lepas, Pulau Pinang.'),
(18, 'Eddie Song', 'Patrick', 'Chew', 'Chew Choon Seng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '510901-07-5027', 'Sep', '016-4882793', 'manntitanuim427@gmail.com', '', 0, ''),
(19, 'Aric', 'Caron Wong', 'Choo', 'Choo Hon Kiat', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-08-31 16:00:00.0', '920528-08-5885', 'May', '016-5686126', 'gic.choo5885@gmail.com ', 'PBB', 4629, 'Blok 2-22B-08, One World Tingkat Mahsuri 2, Bayan Baru, 11950 Bayan Lepas, Penang.'),
(20, 'Eddie Song', NULL, 'Clover Neoh', 'Neoh Jia Huey', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2017-09-07 16:00:00.0', '970220-07-5504', 'Feb', '012-4280220', 'Theofficialcloverjay@gmail.com', '', 0, 'Block 9A-07-02 Taman Kheng Tean, Jalan Van Pragh 11600 Penang.'),
(21, NULL, 'Boey', 'David Har', 'Har Tak Seng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-08 16:00:00.0', '650911-07-5121', 'Sep', '016-4855155', 'hartseng@gmail.com', '', 0, 'Block G-3-4 Taman Sri Setia, Lintang Lembah Permai 2, 11200 Tanjung Bungah, Pulau Pinang'),
(22, NULL, NULL, 'Eddie Song', 'Song Meng Wai', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '751023-07-5775', 'Oct', '013-4888833', 'lycyge@gmail.com', 'PBB', 3984, '501-G, 15-03 Diamond Villa, Jalan Tanjong Bungah, 11200 Tanjong Bungah, Pulau Pinang'),
(23, 'Eddie Song', 'Patrick', 'Erik', 'Tan Yong Wei', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '850501-08-6211', 'May', '016-5075075', 'erik.tywei@gmail.com', 'PBB', 6469, 'Blk 19-1-01, Jalan Mesra 2, Taman Mesra, 13400 Butterworth.'),
(24, 'Jonathan', 'Joe ', 'Eunice Loe', 'Loe Yew Lee', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-02-20 16:00:00.0', '850807-07-5654', 'Aug', '016-4642480', 'euniceloe85@gmail.com', 'PBB', 4676, '6C-29-03, All Season Park, Autumn Tower, 11500 Bandar Baru, Ayer Itam, Penang'),
(25, 'Eddie Song', 'Patrick ', 'Ewe Kok Tai', 'Ewe Kok Tai', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-10-28 16:00:00.0', '770730-07-5823', 'Jul', '012-4237602', 'koktai222@gmail.com', '', 0, '8E, Jalan Delima, Island Glades, 11700 Penang'),
(26, 'Eddie Song', 'Patrick', 'Jane Tan', 'Tan Chia Inn', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '800114-07-5234', 'Jan', '016-4395085', 'gicjanetan@gmail.com', 'PBB', 4507, '50A-27-5 U Garden, Persiaran Minden 1, 11700 Gelugor Penang.'),
(27, 'Eddie Song', 'Jeffrey', 'Jessica Goh', 'Goh Yoke Mei', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '681117-02-5874', 'Nov', '011-12191587', 'jcgoh33@gmail.com', 'PBB', 6467, '27-17-08 Lengkok Nipah 2,Taman Jubilee, 11900 Penang.'),
(28, NULL, 'Eddie Song', 'Jeffrey Chan', 'Chan Jiang Liang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '820824-07-5181', 'Aug', '012-5812903', 'chanjiangliang@gmail.com', 'PBB', 3187, '694-A Mukim 16, Jalan Lintang, 11500 Ayer Itam, Pulau Pinang'),
(29, 'Jonathan', 'Joe ', 'Jimmy Wang', 'Wang Eng Hai', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-09-02 16:00:00.0', '890213-07-5223', 'Feb', '012-4056499', 'jimmyhai2288@gmail.com', 'PBB', 6424, '352-1-3 Jalan Perak, 11600 Georgetown, Penang'),
(30, 'Eddie Song', NULL, 'Joanne Chew', 'Chew Lai Kuen', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '580512-07-5596', 'May', '012-4991505', 'joannechew1991@gmail.com', 'PBB', 6464, '19-12-12 Taman Seri Sari Hilir Paya Terubong 1, Bayan Lepas, 11900 Penang'),
(31, 'Eddie Song', NULL, 'Jocelyn Tok', 'Tok Pei Chi', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '850319-07-5620', 'Mar', '016-5573696', 'Jocelynchi7296@gmail.com ', 'PBB', 6390, '1H, Jalan Beriksa Satu, 11500 Ayer Itam, Penang'),
(32, 'Eddie Song', NULL, 'Jojo Cheah', 'Cheah Mei Qing', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '851106-07-5218', 'Nov', '016-4105611', 'jocelyn85991@gmail.com', 'PBB', 4910, '5-05-5 Jalan Arratoon, 10050 Pulau Pinang'),
(33, 'Eddie Song', 'Jonathan', 'Joe Yeap', 'Yeap Chia Heng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '831011-07-5145', 'Oct', '012-5556956', 'gicjoeyeap@gmail.com', 'PBB', 4608, '1-T Jalan Zoo, 11500 Ayer Itam, Pulau Pinang.'),
(34, NULL, 'Eddie Song', 'Jonathan Ch\'ng', 'Ch\'ng Zhi Fong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '831107-07-5297', 'Nov', '016-4441232', 'gicjonathan6@gmail.com', 'PBB', 3169, 'Blok 14-16-4, Tingkat Paya Terubong 3, 11060 Ayer Itam, Pulau Pinang.'),
(35, 'Eddie Song', 'Jeffrey', 'Jolin Khaw', 'Khaw Hooi Ling', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2017-07-16 16:00:00.0', '770520-07-5630', 'May', '016-4400066', 'jolinkhaw77@gmail.com', '', 0, '31, Lorong Bukit Gambir 2, Ashley Green, 11700 Penang'),
(36, 'Eddie Song', 'Jeffrey', 'Jordan Foo', 'Foo Yi Heng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '940426-07-5633', 'Apr', '017-4048577', 'jordanfoo729@gmail.com', 'PBB', 4766, 'Block 99-7-3A, Lintang Sg Pinang, Taman Pelangi Indah, 11600 Penang.'),
(37, 'Alex Tang', 'Vincent Ooi', 'Kenneth Kung', 'Kung Choon Heng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-12-18 16:00:00.0', '891029-07-5471', 'Oct', '016-5216618', 'Kennethkung89@gmail.com', 'PBB', 4637, 'Blok 23-3-3, Tingkat Paya Terubong 3, 11060 Ayer Itam, Penang.'),
(38, 'Eddie Song', 'Patrick ', 'Kenny Teh', 'Teh Han Hoe', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '750308-07-5635', 'Mar', '016-4116363', 'kennyteh138@gmail.com', 'PBB', 3141, '75A-15-1 Central Park, Lebuhraya Jelutong, 11600 Penang.'),
(39, 'Eddie Song', NULL, 'Kimberlyn Lim', 'Lim Bee Ley', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '680313-07-5364', 'Mar', '012-4545226', 'kimberlyn6868@gmail.com', 'PBB', 3176, '6E-15-12, Melody Homes, Lebuhraya Thean Teik, 11500 Pulau Pinang'),
(40, 'Jonathan', 'Joe ', 'Lester Teoh', 'Teoh En Rui', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-03-26 16:00:00.0', '931115-07-5113', 'Nov', '011-11511554', 'teoh_7372@hotmail.com', 'PBB', 6464, '21-G-04, Pangsapuri Sri Abadi, Lintang Sungai Ara 2, 11900 Penang.'),
(41, 'Eddie Song', NULL, 'Lim Ewe Jin', 'Lim Ewe Jin', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '660706-07-5519', 'Jul', '016-6027799', 'limewejin@gmail.com', 'PBB', 6800, '2D-01-06 Medan Angsana 1, 11500 Ayer Itam, Pulau Pinang.'),
(42, 'Eddie Song', NULL, 'Louise Tan', 'Tan Chang Looi', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '750809-07-5262', 'Aug', '019-5088986', 'louisetan75@gmail.com', 'PBB', 6465, '56 Jalan Tembusu, Bayan Lalang, 13400 Butterworth'),
(43, 'Eddie Song', 'Patrick', 'Loy', 'Loy Ter Ren', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '851002-07-5547', 'Oct', '012-4918110', 'darienloy@gmail.com ', 'PBB', 6909, 'Blok 9-15-7 Tkt Paya Terubong 2, Ayer Itam, 11060 Pulau Pinang.'),
(44, 'Eddie Song', NULL, 'Marcus', 'Koay Teng Koo', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '821005-07-5123', 'Oct', '011-28487887', 'marcuskoay7887@gmail.com', 'PBB', 4927, '36-9-9, Sri Bukit Jambul, 11900 Penang'),
(45, 'Aric', 'Caron', 'Meng Jie', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '', '', '', '', '', 0, ''),
(46, 'Aric', 'Caron', 'Ocean Leong', 'Leong Chin Wei', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-09-30 16:00:00.0', '930725-07-5255', 'Jul', '014-2456737', 'gicocean@gmail.com', 'PBB', 4688, '16, Lorong Gamelan Indah 8, Taman Gamelan Indah, 14200 Sungai Bakap, Penang.'),
(47, '', NULL, 'Olivvia Tan', 'Tan Siew Poh', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '671213-07-5266', 'Dec', '016-2198324', 'sp1312tan@gmail.com', 'PBB', 6406, ''),
(48, '', 'Eddie Song', 'Patrick Oon', 'Oon Liang Siang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '831030-07-5415', 'Oct', '016-4415503', 'gicpatrick328@gmail.com', 'PBB', 3146, '33-1-12 Taman Pekaka, Sungai Dua, 11700 Gelugor, Pulau Pinang'),
(49, 'Jonathan', 'Joe ', 'Philip Yeoh', 'Yeoh Chin Yong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-07-18 16:00:00.0', '930102-07-5523', 'Jan', '012-2579985', 'Philipyong93@gmail.com ', 'PBB', 4613, '7-G-1, Jalan Padang Tembak, 11400 Ayer Itam, Penang'),
(50, 'Eddie Song', 'Patrick', 'Robert Ooi', 'Ooi Chin Aun', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '830405-07-5645', 'Apr', '012-4928363', 'gicrobertooi@gmail.com', 'PBB', 4540, ''),
(51, 'Eddie Song', NULL, 'Sharon Ong', 'Ong Shian Rong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '681111-10-5662', 'Nov', '013-4366670', 'srong04@gmail.com', 'PBB', 6465, '7-10-5 Taman Seri Damai, 11600 Penang'),
(52, '', 'Alvin', 'Tiger Goh', 'Goh Chee Pin', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-11-26 16:00:00.0', '730727-07-5113', 'Jul', '016-4140012', 'gohcheepin1983@gmail.com', 'PBB', 4910, '101-16-01 Gambir Heights, 11700 Gelugor, Penang'),
(53, '', 'Alex Tang', 'Vincent ', 'Ooi Ee Wen', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-12-18 16:00:00.0', '870502-07-5395', 'May', '017-4430881 / 019-5406789', 'vincentooihomes@gmail.com', 'PBB', 4816, '71-G-4 Lebuhraya Batu Maung, 11960 Bayan Lepas, Penang.'),
(54, 'Jonathan', 'Joe', 'Yeap', 'Yeap Tan Kheng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-02-12 16:00:00.0', '700904-07-5337', 'Sep', '016-4489170', 'gic.yeap@gmail.com', 'PBB', 3118, '118-B5-07-03, Putra Place Condo, Persiaran Bayan Indah, Penang'),
(55, '', 'Tiger', 'Law', 'Law Boon Keat', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-14 16:00:00.0', '830130-09-5021', 'Jan', ' 012-4299288', 'lawboonkeat@gmail.com', 'PBB', 4310, '11-05 Lintang Slim, Harmony View, 11700 Georgetown, Penang.'),
(56, '', 'Alex Wan', 'Monica Choong', 'Choong Wei Wei', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-07 16:00:00.0', '821007-07-5274', 'Oct', '016-4571628', '', 'PBB', 4673, 'No. 2, Jalan Pantai Jerjak 16, 11900 Bayan Lepas, Penang.'),
(57, '', 'Berth Heng', 'Chan Yi Hong', 'Chan Yi Hong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-11 16:00:00.0', '961024-07-5799', 'Oct', '012-7028368', '', '', 0, 'Solok Angsana 1M-8-6, Desa Ixora, 11500 Penang.'),
(58, '', 'Alex Tang', 'Natasha Ng', 'Ng Ai Pen', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-13 16:00:00.0', '761025-07-5444', 'Oct', '012-4059174', 'gicnatasha@gmail.com', 'PBB', 6486, '226-19-1 Scott Residence, Jalan Macalister, Georgetown, Penang.'),
(59, 'Eddie Song', 'Jeffrey', 'Cholye Lee', 'Lee Bao En', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-25 16:00:00.0', '931027-08-5928', 'Oct', '012-5493191', 'cholye27@gmail.com', 'PBB', 4455, '75F, Jalan Kelang Sago, 09000 Kulim, Kedah.'),
(60, 'Patrick ', 'Ewe Kok Tai', 'Josh Ang', 'Ang Kok Lim', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-03-03 16:00:00.0', '760510-07-5603', 'May', '012-6523899', 'joshang78@gmail.com', '', 0, '17-09-09 Serina Bay, Hilir Sungai Pinang, 11600 Jelutong, Penang.'),
(61, 'Patrick ', 'Ewe Kok Tai', 'Angie Khoo', 'Khoo Ai Hong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-03-03 16:00:00.0', '780606-07-5390', 'Jun', '012-4290229', 'angiekhoo06@gmail.com', '', 0, '17-09-09 Serina Bay, Hilir Sungai Pinang, 11600 Jelutong, Penang.'),
(62, '', 'Berth Heng', 'Harry Lim', 'Lim Swee Hua', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-18 16:00:00.0', '740817-07-5319', 'Aug', '012-4521783', 'lim.sweehua@gmail.com', '', 0, '12A, Lorong Seri Senangan 2, Taman Seri Senangan, 13050, Butterworth, Penang.'),
(63, '', 'Kimberlyn', 'Jane Chong', 'Chong Choy Lin', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-16 16:00:00.0', '560620-07-5106', 'Jun', '017-5848399', 'janelin.Chong@gmail.com', 'PBB', 6919, '422-6-5 Taman Bukit Bendera, Ayer Itam, Penang.'),
(64, '', 'Vincent Ooi', 'Mark Neoh', 'Neoh Wei Keong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-03-05 16:00:00.0', '860404-35-5503', 'Apr', '011-51128992', '', '', 0, '2B-7-6, Imperial Residences, Lintang Sungai Ara 14, 11900 Penang'),
(65, '', 'Eddie', 'Dato Kirby Lee', 'Lee Kong Beng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-31 16:00:00.0', '760319-07-5269', 'Mar', '019-4222112', 'leekirby36@gmail.com', 'PBB', 4937, '9-5-3 Melody Vila, Lintang P.Ramlee, 10460 Penang.'),
(66, '', 'Caron', 'Ooi Wah Seong', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-09 16:00:00.0', '', '', '', '', '', 0, ''),
(67, '', 'Ocean', 'Yuki Lee', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-10 16:00:00.0', '', '', '', '', '', 0, ''),
(68, '', 'Tiger', 'Admond Lee', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-12-31 16:00:00.0', '', '', '', '', '', 0, ''),
(69, '', 'Lester', 'Lim Chee Yang', 'Lim Chee Yang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-13 16:00:00.0', '940421-08-5031', 'Apr', '017-4155790', '', 'PBB', 4645, '25, Jalan Pekaka 4, Taman Pekaka, 14300 Nibong Tebal, Penang'),
(70, '', '', 'Benny Low', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '', '', '', 'loweek2828@gmail.com', '', 0, ''),
(71, '', '', 'Robert Ooi', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '', '', '', 'robohs1982@gmail.com', '', 0, ''),
(72, '', 'Caron', 'Ah Seong ', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '', '', '', '', '', 0, ''),
(73, 'Patrick Oon', 'Loy', 'Pui Yin', '', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '', '', '016-4530882', '', '', 0, ''),
(74, 'Patrick Oon', 'Alison', 'Allisse Zen', 'Loh Ai Ling', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2025-03-18 16:00:00.0', '820705-07-5322', 'Jul', '019-4551414', 'sensen1431@gmail.com', 'PBB', 3988, '76-6-18 Sri Wangsa 2, Jalan Penaawar Satu, 11600 Penang.'),
(75, 'Eddie', 'Kirby', 'Kean', 'Khaw Choo Kean', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-03 16:00:00.0', '990104-43-5339', 'Jan', '010-3956180', 'unstoppablekean@gmail.com', 'PBB', 6482, '16, Lintang Permai 2, 11700 Gelugor, Penang.'),
(76, '', 'Bertha', 'Angelyn', 'Angelyn Seng Hsiao Ching', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-03 16:00:00.0', '870722-07-5274', 'Jul', '012-6440722', 'angelynching1234@gmail.com', 'PBB', 0, 'A2-9 Lintang Batu Lanchang, 11600 Penang'),
(77, 'Eddie', 'Patrick ', 'Donald Leong', 'Law Kim Leong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '821019-07-5693', 'Oct', '016-4750621', 'lawleong1982@gmail.com', 'PBB', 2147483647, 'Blk 0-16-7, Taman Desa Relau 2, 11900 Pulau Pinang.'),
(78, 'Jordan', 'Carson', 'Nolvin Ng', 'Ng Dian Sheng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2023-04-18 16:00:00.0', '960213-07-5021', 'Feb', '011-36298210', 'nolvin_ng@live.com.my', 'PBB', 0, 'No.3 Medan Mudan 1, Sungai Dua, 11700 Gelugor, Pulau Pinang.'),
(79, 'Aric', 'Caron', 'Meng Jie', 'Tan Meng Jie', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '930607-07-5190', 'Jun', '016-4252511', '', 'PBB', 0, '7, Persiaran Mayang Pasir 4, 11950 Bukit Gedung, Penang.'),
(80, 'Aric', 'Caron', 'Suki', 'Ooi Suki', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '850216-07-5644', 'Feb', '016-4415383', 'miyuki.ooi123@gmail.com', 'PBB', 0, 'M10-4, Jalan Helang, 11700, Sg Dua, Penang'),
(81, 'Jordan', 'Carson', 'Ben Liau', 'Ben Liau Tze Chen', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2025-04-18 16:00:00.0', '991209-02-6173', 'Dec', '013-533-9546', '', 'PBB', 0, '160, Darulaman Heights, 06000 Jitra, Kedah.'),
(82, 'Aric', 'Caron', 'Soo Hui', 'Ooi Soo Hui', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '800731-07-5686', 'Jul', '012-4863918', '', 'PBB', 0, 'M10-4, Jalan Helang, 11700, Penang.'),
(83, 'Patrick', 'Kenny', 'Agnes Lim', 'Lim Siew Hua', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2029-04-18 16:00:00.0', '930222-02-5248', 'Feb', '017-4366940', 'siewhua_22@hotmail.my', 'PBB', 4559, '23, Tmn Tunku Hosna, Fasa 2, 05300 Alor Star, Kedah.'),
(84, 'Patrick', 'Kenny', 'Wynee Ong', 'Ong Wan Mei', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2029-04-18 16:00:00.0', '920701-07-5024', 'Jul', '016-4450032', 'hello.wynee@gmail.com', '', 0, '9, Lorong Rozhan 1, Taman Rozhan, 14000 BM.'),
(85, 'Alex Tang', 'Vincent Ooi', 'Su Li', 'See Su Li', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-04 16:00:00.0', '910911-02-5114', 'Sep', '016-4426801', 'seesuli911@gmail.com', 'PBB', 6467, '40, Taman Kekwa, 06600 Kuala Kedah.'),
(86, 'Aric', 'Caron', 'Zhi Xiong', 'Chan Zhi Xiong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-04 16:00:00.0', '990607-07-6147', 'Jun', '018-7694109', 'chanzhixiong321@gmail.com', '', 0, 'D-13-21 Rifie Range Flats, 11500 Ayer Itam, Penang.'),
(87, 'Aric', 'Caron', 'Zhi Hao', 'Chan Zhi Hao', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-04 16:00:00.0', '990607-07-6139', 'Jun', '018-7624109', 'chanzhihao88@gmail.com', '', 0, 'D-13-21 Rifie Range Flats, 11500 Ayer Itam, Penang.'),
(88, 'Aric', 'Caron', 'Kim Chuan', 'Ooi Kim Chuan', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-04 16:00:00.0', '000530-07-0357', 'May', '017-5566750', 'kimchuanooi00@gmail.com', 'PBB', 6472, '106-B Jalan Trusan, 11600 Georgetown Penang.'),
(89, 'Jonathan', 'Joe', 'Janet Tan', 'Tan Soo Shean', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-03-04 16:00:00.0', '691126-07-5100', 'Nov', '019-5259111', 'janettss2611@gmail.com', '', 0, 'B-5-12 Lintang Hajjah Rehmah, Mutiara Heights, 11600 Jelutong, Penang.'),
(90, '-', 'Marcus', 'Cynthia', 'Cynthia Ann Martin', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-03-04 16:00:00.0', '860415-35-5330', 'Apr', '013-6775553', 'cynthia415ann@gmail.com', '', 0, '7E-03-09 Jalan Thean Teik, 11500 Penang.'),
(91, 'Jordan', 'Carson', 'Keen', 'Wong Yik Keen', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-03-04 16:00:00.0', '960110-06-5293', 'Jan', '012-9069536', '', '', 0, ''),
(92, '-', 'Ai Pheng', 'Beng Chuan', 'Leow Beng Chuan', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-04 16:00:00.0', '920326-08-5385', 'Mar', '018-3998891', 'andyleow1992@gmail.com', 'PBB', 6478, '2-10-5 Jalan Zainal Abidin, Taman Manggis, 10400 Penang.'),
(93, 'Jordan', 'Carson', 'Wei Zhen', 'Cheng Wei Zhen', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-10-04 16:00:00.0', '960429-07-5469', 'Apr', '018-4658394', 'wzcheng1996@gmail.com', 'PBB', 6415, '781 MK8 Gertak Sanggul, 11910 Bayan Lepas, Penang.'),
(94, 'Jordan', 'Carson', 'Kelvin Ooi', 'Ooi Hong Seng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2013-05-18 16:00:00.0', '960713-07-5431', 'Jul', '016-4120713', 'sengooi0713@gmail.com', 'PBB', 6464, '60-05-06, Jalan Slim, 11600 Penang.'),
(95, 'Carson', 'Kelvin Ooi', 'Jack H\'ng', 'H\'ng Kah Liang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2013-05-18 16:00:00.0', '961108-07-5585', 'Nov', '016-4365838', 'kahliang.hng.htp@gmail.com', 'PBB', 6489, '6-2-8, Desa Bukit Jambul, Persiaran Bukit Jambul 6, 11900 Penang.'),
(96, 'Caron Wong', 'Choo', 'Doreen Gan', 'Doreen Gan Chin Chin', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2021-05-18 16:00:00.0', '010227-07-0054', 'Feb', '016-6852183', 'doreengan8@gmail.com', '', 0, ''),
(97, '-', 'Jojo', 'May Tan', 'Tan Hui Min', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2021-05-18 16:00:00.0', '850506-07-5466', 'May', '012-4222105', 'maytan0506@gmail.com', '', 0, ''),
(98, 'Patrick', 'Kenny', 'Jun Ooi', 'Ooi Yeong Jiunn', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2024-05-18 16:00:00.0', '870926-35-5577', 'Sep', '016-4219268', 'yjooi87@hotmaill.com', '', 0, ''),
(99, '-', 'Sharon', 'Kelly Loh', 'Loh Sock Keow', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2024-05-18 16:00:00.0', '670623-07-5264', 'Jun', '019-3287118', 'kellylsk2016@gmail.com', '', 0, '6B-8-3A The Address Condo, Jln Bukit Jambul, 11900 Penang.'),
(100, 'Kelvin Ooi', 'Jack', 'Christopher Khoo', 'Khoo Shu Qi', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2027-05-18 16:00:00.0', '971106-07-5855', 'Nov', '011-11042185', 's6uqizzz@gmail.com', '', 0, '2A-17-7, Imperial Residences, Lintang Sg Ara 14, 11900 Penang.'),
(101, 'Aric', 'Caron', 'Jacelyn Mok', 'Jacelyn Mok Mei Suet', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2028-05-18 16:00:00.0', '991005-07-5460', 'Oct', '011-21989468', '', '', 0, '266 MK1, Pantai Acheh, 11010 Balik Pulau, Pulau Pinang.'),
(102, 'Kenny', 'Jun Ooi', 'Vivian Pua', 'Pua Kooi Khim', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-03-05 16:00:00.0', '911220-07-5600', 'Dec', '017-5283864', 'vivianqing1220@gmail.com', '', 0, '63B-6-12, Symphony Park, Jalan Lenggong, 11600 Penang.'),
(103, 'Caron Wong', 'Catherine', 'Jermaine Tan', 'Tan Hui Qian', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-12-05 16:00:00.0', '000803-07-0388', 'Aug', '016-4002808', 'huiqiantan0803@gmail.com', '', 0, '11, Persiaran Mahsuri, 2/2 11900 Bayan Lepas, Sunway Tunas Penang'),
(104, 'Caron Wong', 'Catherine', 'Carrick You', 'You Kah Chyn', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-12-05 16:00:00.0', '910318-07-5137', 'Mar', '016-4350373', 'carrickykc@gmail.com', 'PBB', 6480, 'No 56, Taman Nyaman Indah, Lrg Pondok Upih 3, 11000 Balik Pulau, Penang'),
(105, 'Carson', 'Ben Liau', 'Rain', 'Tan Ee Ze', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-12-05 16:00:00.0', '980326-35-5159', 'Mar', '012-5893862', 'gictaneeze@gmail.com', 'PBB', 6426, '33, Jalan Secayang Indah, Taman Selayang Indah, Sungai Puyu, 13020 Penang.'),
(106, 'Carson', 'Ben Liau', 'Daniel', 'Poh Shan Yi', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-12-05 16:00:00.0', '981124-07-5989', 'Nov', '014-3070518', 'gicpohshanyi@gmail.com', '', 0, '18, Lengkok Kenari 2, Taman Desari, 11900 Bayan Lepas, Sungai Ara, Penang.'),
(107, 'Jeffrey', 'Jessica', 'Adeline Teoh', 'Adeline Teoh Sin Yea', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-11-05 16:00:00.0', '710804-07-5080', 'Aug', '019-4789681', 'adelteoh@gmail.com', '', 0, '34, Solok Tembaga Tiga, Island Park, 11600 Penang'),
(108, 'Aric', 'Caron Wong', 'Chris Teoh', 'Teoh Tze Terng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-18 16:00:00.0', '930527-07-5537', 'May', '016-6665823', 'teohtterng@gmail.com', 'PBB', 4743, '15, Lebuh Rambal 9, Paya Terubong 11060 Penang.'),
(109, 'Aric', 'Caron Wong', 'Lynn Seow', 'Lynn Seow Lii Ting', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-18 16:00:00.0', '000517-07-0686', 'May', '017-4501686', 'lynnslt517@gmail.com', '', 0, '30, Lorong Permai 5, 11700 Penang.'),
(110, 'Dyson', 'Jovin', 'Tan Khai Shian', 'Tan Khai Shian', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2022-05-18 16:00:00.0', '990910-02-5123', 'Sep', '018-9486401', 'khashiantan@gmail.com', '', 0, '72A-23A-3A, Jalan Mahsuri, Arena Residence, 11950, Bayan Lepas, Penang.'),
(111, 'Jovin', 'Khai Shian', 'Yow Huah', 'Ong Yow Huah', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2025-06-18 16:00:00.0', '990309-07-6325', 'Mar', '012-4700083', '', '', 0, '112A, Taman Sri Mewah Iwdah, 11960, Bayan Lepas, Penang.'),
(112, 'Jeffrey', 'Jordan', 'Dean Lim', 'Lim Chun Ti', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2028-06-18 16:00:00.0', '950620-07-5441', 'Jun', '016-4846517', 'chuntilim95@gmail.com', 'PBB', 4530, '90-8-15 Lorong Seremban, Kota Emas, 10150 Penang.'),
(113, 'Aric', 'Caron Wong', 'Kah Seng', 'Wong Kah Seng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2028-06-18 16:00:00.0', '900126-07-5387', 'Jan', '012-4198088', 'wongkasen@gmail.com', 'PBB', 4657, '149 Gerbang Bukit Kecil 2, Sg Nibong Penang.'),
(114, 'Aric', 'Caron Wong', 'David Lim', 'Lim Chien Her', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2028-06-18 16:00:00.0', '990202-07-5277', 'Feb', '012-4788187', 'davidlim6271@gmail.com', 'PBB', 6445, '18-10-12B Menara Greenview, Halaman Tembaga, 11600 Penang.'),
(115, 'Carson', 'Ben Liau', 'Yao Ming', 'Loh Yao Ming', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2026-06-18 16:00:00.0', '990724-07-5709', 'Jul', '012-4578800', 'lohyaoming1999@gmail.com', '', 0, '5-8-5 Taman Lembah Hijau, Lorong Gangsa, 11600 Penang.'),
(116, 'Carson', 'Ben Liau', 'Yao Guang', 'Loh Yao Guang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2026-06-18 16:00:00.0', '990724-07-5661', 'Jul', '012-4178800', 'yaoguangloh@yahoo.com', '', 0, '5-8-5 Taman Lembah Hijau, Lorong Gangsa, 11600 Penang.'),
(117, 'Carson', 'Ben Liau', 'Sharon Tan', 'Tan Yi Yi', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2026-06-18 16:00:00.0', '980414-07-5288', 'Apr', '018-2253001', 'yiyitan0414@gmail.com', '', 0, '19-06B,  Lintang Sg Ara 2, 11900 Bayan Lepas Penang.'),
(118, 'Alex Tang', 'Kenneth', 'Liu Kok Xiang', 'Liu Kok Xiang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2027-06-18 16:00:00.0', '870217-07-5361', 'Feb', '012-5278217', 'kokxiang217@gmail.com', '', 0, '2-13A-9 Vertiq Condominium, Lebuh Tunku Kudin, 11700 Gelugor Penang.'),
(119, 'Eddie', 'Kirby', 'Louis Lim', 'Lim Sions Hock', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2022-07-18 16:00:00.0', '800130-07-5285', 'Jan', '017-9008952', 'pgllouis@gmail.com', 'PBB', 4048, 'Block 9B-17-02, Taman Kheng Tian, Jalan Van Praagh, 11600 Penang.'),
(120, 'Eddie', 'Kirby', 'YC Tan', 'Tan Yeong Chuan', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2022-07-18 16:00:00.0', '740826-07-5177', 'Aug', '010-3888111', 'tanyeongchuan1@gmail.com', 'PBB', 4330, '18 Jalan Bukit Minyak, Taman Kota Permai, 14000 BM Penang.'),
(121, 'Eddie', 'Kirby', 'Khor', 'Khor Yu Nong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2022-07-18 16:00:00.0', '860704-35-5011', 'Jul', '012-4018315', 'ynkhor86@gmail.com', '', 0, '18, Lorong Tekukur Indah 6, Taman Tekukur Indah'),
(122, 'Eddie', 'Kirby', 'Sam Tan', 'Tan Horng Lin', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2022-07-18 16:00:00.0', '740808-07-5069', 'Aug', '019-4815955', 'sjesuccess168@gmail.com', 'PBB', 6338, '23-16-01 Alpine Tower, Tingkat Bukit Jambul 1, Bayan Lepas Penang.'),
(123, 'Khor Yu Nong', 'Sam Tan', 'KS Chan', 'Chan Kin Seng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2015-07-18 16:00:00.0', '561002-07-5445', 'Oct', '016-5216935', 'kschan939@gmail.com', '', 0, '27F, Lorong Kampung Melayu, Ayer Itam 11500 Penang.'),
(124, 'Loy', 'Pui Yin', 'Phang Yik Chia', 'Phang Yik Chia', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2015-07-18 16:00:00.0', '920227-07-5181', 'Feb', '016-4353341', 'yikchia@gmail.com', 'PBB', 6316, '105B-2-7 Baystar Condo, Persiaran Bayan Indah 1, 11900 Penang.'),
(125, '', 'Dyson', 'Jovin', 'Pang Chong Min', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '991116-07-5312', 'Nov', '016-4196366', 'jovinpg@gmail.com', 'CIMB', 7065, '47, Medan Sungkai, 10100 Pulau Pinang.'),
(126, 'Patrick', 'Kenny', 'Crystal Tee', 'Tee Hooi Peng', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2024-07-18 16:00:00.0', '940812-07-5682', 'Aug', '016-5596624', 'crystaltee6624@gmail.com', 'PBB', 4678, '38,  Tingkat Kurau 2, TamanChai Leng Park'),
(127, 'Jeffrey', 'Cholye', 'Carlos Lim', 'Lim Kai Xiang', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-07 16:00:00.0', '880830-35-5133', 'Aug', '012-7980506', 'CarlosLKX@hotmail.com', 'PBB', 2147483647, '6591-F, Jalan Melur, Bagan Ajam 1300 B\'worth, Penang'),
(128, 'Eddie', 'Kirby', 'Kuhen ', 'Kuhen a/l Kacinathan', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-04-07 16:00:00.0', '910603-07-5857', 'Jun', '018-2040945', 'Kuhen5025@gmail.com', '', 0, '47-6-30 Paya Terubong, Jalan Air Itam 11060 Pulau Pinang'),
(129, 'Carson', 'Kelvin Ooi', 'Jia Kit', 'Chng Jia Kit', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-07 16:00:00.0', '961109-07-5229', 'Nov', '011-11109587', 'jkchng96@gmail.com', 'PBB', 2147483647, '97, Persiaran mahsuri 2/4, 11900 Bayan Lepas, Penang'),
(130, 'Eddie Song', 'Patrick', 'Ivan Tan ', 'Tan Moh Loon', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-07 16:00:00.0', '790726-07-5503', 'Jul', '012-4398359', 'tanmohloon@gmail.com', '', 0, '1-10-4, Lengkok Free School 11600 Penang'),
(131, 'Aric', 'Caron', 'Emma', 'Seow Siew Lee', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-07 16:00:00.0', '820511-02-5220', 'May', '011-61429128', 'seowemma@gmail.com', 'PBB', 2147483647, '138, Taman Sri Ampang 0585'),
(132, 'Eddie', 'Patrick', 'Michelle Tan', 'Tan Sok Kean', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-06 16:00:00.0', '830226-07-5228', 'Feb', '012-6781927', 'michelletan1927@gmail.com', '', 0, '10, Jalan Satu Air Itam 11400 Penang'),
(133, 'Patrick', 'Loy', 'Fong Pui Yin', 'Fong Pui Yin', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2015-07-18 16:00:00.0', '920319-07-5496', 'Mar', '016-4530882', 'puiyinfong319@gmail.com', 'PBB', 2147483647, '45, Lorong Sungai Kelian 2, 11200 Tanjung Bungah, Penang'),
(134, 'Aric', 'Caron', 'Bryan Khoo', 'Bryan Khoo Tze Yong', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '991208-07-5475', 'Dec', '016-4939366', 'bryankhoo44@gmail.com', '', 0, 'B3-7-4 Putra Place Persiaran Bayan Indah 3 11900 Sg Nibong'),
(135, 'Bertha', 'Harry', 'Choon Kuen', 'Soo Choon Kuen', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2016-08-18 16:00:00.0', '961201-08-5163', 'Dec', '018-4007975', 'chookuen23@gmail.com', '', 0, 'No. 20, Persiaran Pakatan Jaya 11, Taman Pakatan Jayam 31150 Ulu Kinta, Perak'),
(136, ' Sam Tan', 'KS Chan', 'Kang Jie', 'Khor Khang Jian', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-08-18 16:00:00.0', '930503-07-5583', 'May', '016-4277056', 'Jie5583@gmail.com', 'PBB', 2147483647, '2-3A-02, Taman Sinar Pelangi, Lorong Penawar 2, 11600 P.P'),
(137, 'Sam Tan', 'Khor Khang Jian', 'Seng Chung', 'Chong Seng Chung', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-08-18 16:00:00.0', '720513-07-5397', 'May', '017-5682281', 'chongsengchung@gmail.com', 'PBB', 2147483647, '483, Jalan Pokok Cheri, 11500 Air Itam, PG'),
(138, 'Sam Tan', 'KS Chan', 'Winford', 'Lim Chek Hean', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-08-18 16:00:00.0', '611225-02-5261', 'Dec', '019-2242456', 'winfordlch2928@gmail.com', 'PBB', 2147483647, '288E-2-10 Fortune Court, Leb Thean Teik, Air Itam.Pg'),
(139, 'Eddie', 'Kirby', 'Louisa', 'Wong Wern Pei', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2021-08-18 16:00:00.0', '841023-07-5758', 'Oct', '016-4102883/014-3472628', 'wwp1023@gmail.com', 'PBB', 2147483647, '22-A, Villa Bunga Telang, Jalan Bunga Telang, 11200, Pg'),
(140, 'Jordan', 'Carson', 'Ivory Tee', 'Ivory Tee Yong Qian', 3, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2023-08-18 16:00:00.0', '980522-07-5218', 'May', '016-4152799', 'ivorytee40@gmail.com', '', 0, '728-J Permatang Damar Laut, 11960 Bayan Lepas'),
(141, NULL, NULL, 'admin1', 'admin1', 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-01-13 16:00:00.0', '670130-07-5098', NULL, NULL, NULL, NULL, NULL, NULL),
(142, NULL, NULL, 'admin2', 'admin2', 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-01-13 16:00:00.0', '670130-07-5098', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_project`
--
ALTER TABLE `admin_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advance_slip`
--
ALTER TABLE `advance_slip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_list`
--
ALTER TABLE `bank_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commission`
--
ALTER TABLE `commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_multi`
--
ALTER TABLE `invoice_multi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_status`
--
ALTER TABLE `loan_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_list`
--
ALTER TABLE `payment_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_project`
--
ALTER TABLE `admin_project`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `advance_slip`
--
ALTER TABLE `advance_slip`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `bank_list`
--
ALTER TABLE `bank_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `commission`
--
ALTER TABLE `commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `invoice_multi`
--
ALTER TABLE `invoice_multi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `loan_status`
--
ALTER TABLE `loan_status`
  MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `payment_list`
--
ALTER TABLE `payment_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
