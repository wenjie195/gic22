<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $loanDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getInvoice($conn);
$projectDetails = getProject($conn);
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "WHERE case_status = 'COMPLETED'";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice Record | GIC" />
    <title>Invoice Record | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Invoice Record</h1>
    <div class="short-red-border"></div>
    <h3 class="h1-title"> Single Invoice| <a style="color: maroon" href="invoiceRecordMulti.php" class="h1-title">Multi Invoice</a></h3>
    <!-- This is a filter for the table result -->
	<!-- <div class="clean"></div> -->
  <div class="float-right section-divider mobile-100">

    <form class="" action="selected.php" method="post">
      <select id="sel_id" name="invoiceRecord2"  onchange="this.form.submit();" class="clean-select">
        <?php if (isset($_GET['name'])) {
          if ($_GET['name'] == 'SHOW ALL') {
            $projectName = "WHERE case_status = 'COMPLETED'";
          }else {
            $type = $_GET['name'];
            $types = urldecode("$type");
            $projectName = "WHERE project_name = '$types'";
          }
          ?><option value=""><?php echo $_GET['name'] ?></option>
          <option value="">Choose Project</option><?php
        }else {
          ?><option value="">Choose Project</option><?php
        } ?>

        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="SHOW ALL">SHOW ALL</option><?php
            } ?>
      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
    </form>

      <?php
          if (isset($_POST['sel_name']))
          {
              $projectName =  $_POST['sel_name'];
          }
          else
          {
              // echo "string";
          }
      ?>


  </div>




    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                        <th class="th">DATE</th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?></th>
                        <th class="th">AMOUNT (RM)</th>
                        <th class="th">SST</th>
                        <th class="th">1ST STATUS</th>
                        <th class="th">RECEIVED DATE</th>
                        <th class="th">CHECK NO.</th>
                        <th class="th">ACTION</th>
                        <th class="th">DATE</th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?></th>
                        <th class="th">AMOUNT (RM)</th>
                        <th class="th">SST</th>
                        <th class="th">2ND STATUS</th>
                        <th class="th">RECEIVED DATE</th>
                        <th class="th">CHECK NO.</th>
                        <th class="th">ACTION</th>
                        <th class="th">DATE</th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?></th>
                        <th class="th">AMOUNT (RM)</th>
                        <th class="th">SST</th>
                        <th class="th">3RD STATUS</th>
                        <th class="th">RECEIVED DATE</th>
                        <th class="th">CHECK NO.</th>
                        <th class="th">ACTION</th>
                        <th class="th">DATE</th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?></th>
                        <th class="th">AMOUNT (RM)</th>
                        <th class="th">SST</th>
                        <th class="th">4TH STATUS</th>
                        <th class="th">RECEIVED DATE</th>
                        <th class="th">CHECK NO.</th>
                        <th class="th">ACTION</th>
                        <th class="th">DATE</th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?></th>
                        <th class="th">AMOUNT (RM)</th>
                        <th class="th">SST</th>
                        <th class="th">5TH  STATUS</th>
                        <th class="th">RECEIVED DATE</th>
                        <th class="th">CHECK NO.</th>
                        <th class="th">ACTION</th>
                        <!-- <th>STOCK</th> -->


                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // if ($invoiceDetails) {

                        $orderDetails = getLoanStatus($conn, $projectName);
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {
                              $proDetails = getProject($conn, "WHERE project_name=?",array("project_name"),array($orderDetails[$cntAA]->getProjectName()), "s");
                              if ($proDetails) {
                                for ($cntBB=0; $cntBB <count($proDetails) ; $cntBB++) {

?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td"><?php echo $cntAA + 1?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate1();?></td>
                                <td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate1()))."1".$orderDetails[$cntAA]->getId();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt1st();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSst1();?></td>
                                <?php if (!$orderDetails[$cntAA]->getCheckNo1() && $orderDetails[$cntAA]->getRequestDate1()) {
                                  ?><td class="td">PENDING</td><?php
                                }elseif (!$orderDetails[$cntAA]->getRequestDate1()) {
                                  ?><td class="td"></td><?php
                                }else {
                                  ?><td class="td">COMPLETED</td><?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate1();?></td>
                                <td class="td">
                                  <?php if ($orderDetails[$cntAA]->getClaimAmt1st()) {
                                ?><form class="" action="utilities/checkNo.php" method="post">
                                  <?php if ($orderDetails[$cntAA]->getCheckNo1() && $orderDetails[$cntAA]->getReceiveDate1()) {
                                    echo $orderDetails[$cntAA]->getCheckNo1();
                                    ?><button class="button-remove" type="submit" name="removeCheckID1">Edit</button><?php
                                  }elseif($orderDetails[$cntAA]->getStatus1st() == 'SINGLE') {
                                    ?>
                                    <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo1();?>">
                                    <button class="button-add" type="submit" name="addCheckID1">Submit</button>
                                  <?php }else {
                                    echo "Insert Your Check No on Multi Invoice";
                                  } ?>
                                  <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                  <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">


                                </form>
                              <?php } ?>
                              </td>
                              <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt1st() && $orderDetails[$cntAA]->getStatus1st() == 'SINGLE') {
                                  ?><form action="invoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                      <input type="hidden" name="claim_status" value="1">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate1()))."1".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate1()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }else {
                                  if ($proDetails[$cntBB]->getProjectClaims() >= 1 && $orderDetails[$cntAA]->getStatus1st() != 'MULTI') {
                                  ?><form action="editInvoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                      <input type="hidden" name="claim_status" value="1">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate1()))."1".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate1()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }} ?>

                              </td>
                              <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate2();?></td>
                              <?php if ($orderDetails[$cntAA]->getRequestDate2()) {
                                ?><td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate2()))."2".$orderDetails[$cntAA]->getId();?></td><?php
                              }else {
                                ?><td class="td"></td> <?php
                              } ?>
                              <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt2nd();?></td>
                              <td class="td"><?php echo $orderDetails[$cntAA]->getSst2();?></td>
                              <?php if (!$orderDetails[$cntAA]->getCheckNo2() && $orderDetails[$cntAA]->getRequestDate2()) {
                                ?><td class="td">PENDING</td><?php
                              }elseif (!$orderDetails[$cntAA]->getRequestDate2()) {
                                ?><td class="td"></td><?php
                              }else {
                                ?><td class="td">COMPLETED</td><?php
                              } ?>
                              <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate2();?></td>
                              <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt2nd()) {
                                  ?><form class="" action="utilities/checkNo.php" method="post">
                                    <?php if ($orderDetails[$cntAA]->getCheckNo2() && $orderDetails[$cntAA]->getReceiveDate2()) {
                                      echo $orderDetails[$cntAA]->getCheckNo2();
                                      ?><button class="button-remove" type="submit" name="removeCheckID2">Edit</button><?php
                                    }elseif($orderDetails[$cntAA]->getStatus2nd() == 'SINGLE') {
                                      ?>
                                      <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo3();?>">
                                      <button class="button-add" type="submit" name="addCheckID2">Submit</button>
                                    <?php }else {
                                      echo "Insert Your Check No on Multi Invoice";
                                    } ?>
                                    <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                    <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">


                                  </form><?php
                                } ?>


                            </td>
                            <td class="td">
                              <?php if ($orderDetails[$cntAA]->getClaimAmt2nd() && $orderDetails[$cntAA]->getStatus2nd() == 'SINGLE') {
                                ?><form action="invoice.php" method="POST">
                                  <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                    <input type="hidden" name="claim_status" value="2">
                                    <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate2()))."2".$orderDetails[$cntAA]->getId();?>">
                                    <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate2()?>">
                                        <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                        <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                    </button></a>
                                </form><?php
                              }else {
                                if ($proDetails[$cntBB]->getProjectClaims() >= 2 && $orderDetails[$cntAA]->getStatus2nd() != 'MULTI') {
                                ?><form action="editInvoice.php" method="POST">
                                  <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                    <input type="hidden" name="claim_status" value="2">
                                    <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate2()))."2".$orderDetails[$cntAA]->getId();?>">
                                    <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate2()?>">
                                        <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                        <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                    </button></a>
                                </form><?php
                              }} ?>

                            </td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate3();?></td>
                            <?php if ($orderDetails[$cntAA]->getRequestDate3()) {
                              ?><td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate3()))."3".$orderDetails[$cntAA]->getId();?></td><?php
                            }else {
                              ?><td class="td"></td> <?php
                            } ?>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt3rd();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getSst3();?></td>
                            <?php if (!$orderDetails[$cntAA]->getCheckNo3() && $orderDetails[$cntAA]->getRequestDate3()) {
                              ?><td class="td">PENDING</td><?php
                            }elseif (!$orderDetails[$cntAA]->getRequestDate3()) {
                              ?><td class="td"></td><?php
                            }else {
                              ?><td class="td">COMPLETED</td><?php
                            } ?>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate3();?></td>
                            <td class="td">
                            <?php if ($orderDetails[$cntAA]->getClaimAmt3rd()) {
                              ?><form class="" action="utilities/checkNo.php" method="post">
                                <?php if ($orderDetails[$cntAA]->getCheckNo3() && $orderDetails[$cntAA]->getReceiveDate3()) {
                                  echo $orderDetails[$cntAA]->getCheckNo3();
                                  ?><button class="button-remove" type="submit" name="removeCheckID3">Edit</button><?php
                                }elseif($orderDetails[$cntAA]->getStatus3rd() == 'SINGLE') {
                                  ?>
                                  <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo3();?>">
                                  <button class="button-add" type="submit" name="addCheckID3">Submit</button>
                                <?php }else {
                                  echo "Insert Yout Check No on Multi Invoice";
                                } ?>
                                <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">


                              </form><?php
                            } ?>

                          </td>
                                <td class="td">
                                  <?php if ($orderDetails[$cntAA]->getClaimAmt3rd() && $orderDetails[$cntAA]->getStatus3rd() == 'SINGLE') {
                                    ?><form action="invoice.php" method="POST">
                                      <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                        <input type="hidden" name="claim_status" value="3">
                                        <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate3()))."3".$orderDetails[$cntAA]->getId();?>">
                                        <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate3()?>">
                                            <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                            <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                        </button></a>
                                    </form><?php
                                  }else {
                                    if ($proDetails[$cntBB]->getProjectClaims() >= 3 && $orderDetails[$cntAA]->getStatus3rd() != 'MULTI') {
                                    ?><form action="editInvoice.php" method="POST">
                                      <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                        <input type="hidden" name="claim_status" value="3">
                                        <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate3()))."3".$orderDetails[$cntAA]->getId();?>">
                                        <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate3()?>">
                                            <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                            <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                        </button></a>
                                    </form><?php
                                  }} ?>

                                </td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate4();?></td>
                                <?php if ($orderDetails[$cntAA]->getRequestDate4()) {
                                  ?><td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate4()))."4".$orderDetails[$cntAA]->getId();?></td><?php
                                }else {
                                  ?><td class="td"></td> <?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt4th();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSst4();?></td>
                                <?php if (!$orderDetails[$cntAA]->getCheckNo4() && $orderDetails[$cntAA]->getRequestDate4()) {
                                  ?><td class="td">PENDING</td><?php
                                }elseif (!$orderDetails[$cntAA]->getRequestDate4()) {
                                  ?><td class="td"></td><?php
                                }else {
                                  ?><td class="td">COMPLETED</td><?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate4();?></td>
                                <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt4th()) {
                                  ?><form class="" action="utilities/checkNo.php" method="post">
                                    <?php if ($orderDetails[$cntAA]->getCheckNo4() && $orderDetails[$cntAA]->getReceiveDate4()) {
                                      echo $orderDetails[$cntAA]->getCheckNo4();
                                      ?><button class="button-remove" type="submit" name="removeCheckID4">Edit</button><?php
                                    }elseif($orderDetails[$cntAA]->getStatus4th() == 'SINGLE') {
                                      ?>
                                      <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo4();?>">
                                      <button class="button-add" type="submit" name="addCheckID4">Submit</button>
                                    <?php }else {
                                      echo "Insert Your Invoice On Multi Invoice";
                                    } ?>
                                    <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                    <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">


                                  </form><?php
                                } ?>

                              </td>
                                    <td class="td">
                                      <?php if ($orderDetails[$cntAA]->getClaimAmt4th() && $orderDetails[$cntAA]->getStatus4th() == 'SINGLE') {
                                        ?><form action="invoice.php" method="POST">
                                          <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                            <input type="hidden" name="claim_status" value="4">
                                            <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate4()))."4".$orderDetails[$cntAA]->getId();?>">
                                            <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate4()?>">
                                                <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                                <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                            </button></a>
                                        </form><?php
                                      }else {
                                        if ($proDetails[$cntBB]->getProjectClaims() >= 4 && $orderDetails[$cntAA]->getStatus4th() != 'MULTI') {
                                        ?><form action="editInvoice.php" method="POST">
                                          <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                            <input type="hidden" name="claim_status" value="4">
                                            <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate4()))."4".$orderDetails[$cntAA]->getId();?>">
                                            <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate4()?>">
                                                <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                                <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                            </button></a>
                                        </form><?php
                                      }} ?>

                                    </td>
                                    <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate5();?></td>
                                    <?php if ($orderDetails[$cntAA]->getRequestDate5()) {
                                      ?><td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate5()))."5".$orderDetails[$cntAA]->getId();?></td><?php
                                    }else {
                                      ?><td class="td"></td> <?php
                                    } ?>
                                    <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt5th();?></td>
                                    <td class="td"><?php echo $orderDetails[$cntAA]->getSst5();?></td>
                                    <?php if (!$orderDetails[$cntAA]->getCheckNo5() && $orderDetails[$cntAA]->getRequestDate5()) {
                                      ?><td class="td">PENDING</td><?php
                                    }elseif (!$orderDetails[$cntAA]->getRequestDate5()) {
                                      ?><td class="td"></td><?php
                                    }else {
                                      ?><td class="td">COMPLETED</td><?php
                                    } ?>
                                    <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate5();?></td>
                                    <td class="td">
                                    <?php if ($orderDetails[$cntAA]->getClaimAmt5th()) {
                                      ?><form class="" action="utilities/checkNo.php" method="post">
                                        <?php if ($orderDetails[$cntAA]->getCheckNo5() && $orderDetails[$cntAA]->getReceiveDate5()) {
                                          echo $orderDetails[$cntAA]->getCheckNo5();
                                          ?><button class="button-remove" type="submit" name="removeCheckID5">Edit</button><?php
                                        }elseif($orderDetails[$cntAA]->getStatus5th() == 'SINGLE') {
                                          ?>
                                          <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo5();?>">
                                          <button class="button-add" type="submit" name="addCheckID5">Submit</button>
                                        <?php }else {
                                          echo "Insert Check No on Multi Invoice";
                                        } ?>
                                        <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                        <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">


                                      </form><?php
                                    } ?>

                                  </td>
                                        <td class="td">
                                          <?php if ($orderDetails[$cntAA]->getClaimAmt5th() && $orderDetails[$cntAA]->getStatus5th() == 'SINGLE') {
                                            ?><form action="invoice.php" method="POST">
                                              <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                                <input type="hidden" name="claim_status" value="5">
                                                <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate5()))."5".$orderDetails[$cntAA]->getId();?>">
                                                <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate5()?>">
                                                    <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                                    <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                                </button></a>
                                            </form><?php
                                          }else {
                                            if ($proDetails[$cntBB]->getProjectClaims() >= 5 && $orderDetails[$cntAA]->getStatus5th() != 'MULTI') {

                                            ?><form action="editInvoice.php" method="POST">
                                              <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                                <input type="hidden" name="claim_status" value="5">
                                                <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate5()))."5".$orderDetails[$cntAA]->getId();?>">
                                                <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate5()?>">
                                                    <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                                    <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                                </button></a>
                                            </form><?php
                                          } }?>

                                        </td>

                            </tr>
                            <?php
                            }
                          }
                        }
                        }
                    // }
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        // $orderDetails = getLoanStatus($conn, $projectName);

                    //}
                    ?>
                </tbody>
            </table><br>


    </div>

    <!-- <div class="three-btn-container">
    <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
      <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
    <!-- </div> -->
    <?php $conn->close();?>

</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Edit Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<style>
      body {

      }
      input {

      font-weight: bold;
      /* color: white; */
      text-align: center;
      border-radius: 15px;
      }
      .no-outline:focus {
      outline: none;

      }
      button{
        border-radius: 15px;
        color: white;

      }
      .button-add{
        background-color: green;
      }
      .button-remove{
        background-color: maroon;
      }
    </style>
</body>
</html>
