<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
// require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$cntNO = 0;
$cntYES = 0;

// $loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$loanUidRows = getLoanStatus($conn);
$invoiceDetails = getInvoice($conn);
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "";

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Loan Status</h1>
    <div class="short-red-border overflow"></div>
    <!-- This is a filter for the table result -->
    <div class="width100 overflow section-divider">
          <a href="adminAddNewProject.php">
              <div class="five-red-btn-div">
                  <p class="short-p five-red-p g-first-3-p n-p">NEW PROJECT</p>
              </div>
          </a>
          <a href="adminAddNewProduct.php">
              <div class="five-red-btn-div left-mid-red">
                  <p class="short-p five-red-p f-first-3-p a-p">ADD NEW LOAN</p>
              </div>
          </a>
          <a href="">
          <form method="post" action="export_data.php">
              <button class="five-red-btn-div clean clean-button" type="submit" name="export">
                  <p class="short-p five-red-p e-first-3-p e-p">EXPORT TO EXCEL</p>
              </button>
          </form>
          </a>
      </div>
      <div class="clear"></div>
    <div class="section-divider width100 overflow">
		<?php $projectDetails = getProject($conn); ?>

    <form class="" action="selected.php" method="post">
      <select id="sel_id" name="admin2Product"  onchange="this.form.submit();" class="clean-select">
        <?php if (isset($_GET['name'])) {
          if ($_GET['name'] == 'SHOW ALL') {
            $projectName = "";
          }else {
            $type = $_GET['name'];
            $types = urldecode("$type");
            $projectName = "WHERE project_name = '$types'";
          }
          ?><option value=""><?php echo $_GET['name'] ?></option>
          <option value="">Choose Project</option><?php
        }else {
          ?><option value="">Choose Project</option><?php
        } ?>

        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="SHOW ALL">SHOW ALL</option><?php
            } ?>
      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
    </form>


    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?></th>
                        <th class="th">IC</th>
                        <th class="th">CONTACT</th>
                        <th class="th">E-MAIL</th>
                        <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                        <th class="th">SQ FT</th>
                        <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?></th>
                        <th class="th">PACKAGE</th>
                        <th class="th">DISCOUNT</th>
                        <th class="th">REBATE</th>
                        <!-- <th class="th"><?php //echo wordwrap("EXTRA REBATE",10,"</br>\n");?></th> -->
                        <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?></th>
                        <th class="th">AGENT</th>
                        <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?></th>

                        <th class="th">REMARK</th>
                        <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?></th>
                        <th class="th">LAWYER</th>
                        <!-- <th class="th"><?php //echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?></th> -->
                        <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LOAN AMOUNT",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?></th>
                        <th class="th">CASH BUYER</th>
                        <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("EVENT / PERSONAL",10,"</br>\n");?></th>
                        <th class="th">RATE</th>
                        <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?></th>
                        <th class="th">UPLINE</th>
                        <th class="th">UP-UPLINE</th>
                        <th class="th"><?php echo wordwrap("PL NAME",5,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("HOS NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LISTER NAME",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UL OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UUL OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PL OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("HOS OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LISTER OVERRIDE",10,"</br>\n");?></th>

                        <th class="th"><?php echo wordwrap("ADMIN1 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("ADMIN2 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("ADMIN3 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("GIC PROFIT",8,"</br>\n");?></th>
                          <th class="th"><?php echo wordwrap("TOTAL CLAIMED DEV AMT",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("TOTAL BALANCED DEV AMT",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("DATE MODIFIED",10,"</br>\n");?></th>
                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->

                        <th>ACTION</th>
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $orderDetails = getLoanStatus($conn, $projectName);
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {
                              if ($orderDetails[$cntAA]->getCancelledBooking() != 'YES') {
                                $cntNO++;
                              ?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td"><?php echo $cntNO + $cntYES;?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getPurchaserName());?></td>
                                <td class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getIc());?></td>
                                <td class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getContact());?></td>

                                <td class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getEmail());?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo number_format($orderDetails[$cntAA]->getSqFt());?></td>

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getSpaPrice();?></td> -->

                                <!-- show , inside value -->
                                <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                                <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getExtraRebate();?></td> -->

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getNettPrice();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                                <!-- show , inside value -->
                                <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                                <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                                <?php $totalDevComm = $orderDetails[$cntAA]->getTotalDeveloperComm();?>
                                <td class="td"><?php echo $totalDevCommValue = number_format($totalDevComm, 2); ?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                                <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getRemark();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLoanAmount();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRate();
                                if ($orderDetails[$cntAA]->getRate()) {
                                  echo "%";
                                }?></td>

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getAgentComm();?></td> -->

                                <?php $agentComm = $orderDetails[$cntAA]->getAgentComm();?>
                                <td class="td"><?php echo $agentCommValue = number_format($agentComm, 2); ?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline1();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline2();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPlName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getHosName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getListerName();?></td>

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getUlOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getUulOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getPlOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getHosOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getListerOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getAdmin1Override();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getAdmin2Override();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getGicProfit();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalClaimDevAmt();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalBalUnclaimAmt();?></td> -->

                                <!-- show , inside value -->
                                <?php $ulOverride = $orderDetails[$cntAA]->getUlOverride();?>
                                <td class="td"><?php echo $ulOverrideValue = number_format($ulOverride, 2); ?></td>
                                <?php $uUlOverride = $orderDetails[$cntAA]->getUulOverride();?>
                                <td class="td"><?php echo $uUlOverrideValue = number_format($uUlOverride, 2); ?></td>
                                <?php $plOverride = $orderDetails[$cntAA]->getPlOverride();?>
                                <td class="td"><?php echo $plOverrideValue = number_format($plOverride, 2); ?></td>
                                <?php $hosOverride = $orderDetails[$cntAA]->getHosOverride();?>
                                <td class="td"><?php echo $hosOverrideValue = number_format($hosOverride, 2); ?></td>
                                <?php $listerOverride = $orderDetails[$cntAA]->getListerOverride();?>
                                <td class="td"><?php echo $listerOverrideValue = number_format($listerOverride, 2); ?></td>
                                <?php $a1Over = $orderDetails[$cntAA]->getAdmin1Override();?>
                                <td class="td"><?php echo $a3OverValue = number_format($a1Over, 2); ?></td>
                                <?php $a2Over = $orderDetails[$cntAA]->getAdmin2Override();?>
                                <td class="td"><?php echo $a2OverValue = number_format($a2Over, 2); ?></td>
                                <?php $a3Over = $orderDetails[$cntAA]->getAdmin3Override();?>
                                <td class="td"><?php echo $a3OverValue = number_format($a3Over, 2); ?></td>
                                <?php $gicProfit = $orderDetails[$cntAA]->getGicProfit();?>
                                <td class="td"><?php echo $gicProfitValue = number_format($gicProfit, 2); ?></td>
                                <?php $totalClaim = $orderDetails[$cntAA]->getTotalClaimDevAmt();?>
                                <td class="td"><?php echo $totalClaimValue = number_format($totalClaim, 2); ?></td>
                                <?php $totalUnclaim = $orderDetails[$cntAA]->getTotalBalUnclaimAmt();?>
                                <td class="td"><?php echo $totalUnclaimValue = number_format($totalUnclaim, 2); ?></td>
                                <td id="dateModified" class="td">
                                  <form class="" action="historyIndividual.php" method="post">
                                    <input type="hidden"  id="loanUid" name="loan_uid" value="<?php $orderDetails[$cntAA]->getLoanUid() ?>">
                                    <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($orderDetails[$cntAA]->getDateUpdated()));?></a> </button>
                                    </td>
                                  </form>

                                <td class="td">
                                    <form action="editProduct.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                            <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                                            <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                                        </button>
                                    </form>
                                </td>

                            </tr>
                            <?php
                          }}
                          for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                          {
                            if ($orderDetails[$cntAA]->getCancelledBooking() == 'YES') {
                              $cntYES++;
                            ?>
                          <tr>
                              <!-- <td><?php //echo ($cntAA+1)?></td> -->
                              <td style="background-color: pink " class="td"><?php echo $cntYES + $cntNO;?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getPurchaserName());?></td>
                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getIc());?></td>
                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getContact());?></td>

                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getEmail());?></td>
                              <td style="background-color: pink " class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                              <td style="background-color: pink " class="td"><?php echo number_format($orderDetails[$cntAA]->getSqFt());?></td>

                              <!-- <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getSpaPrice();?></td> -->

                              <!-- show , inside value -->
                              <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                              <td style="background-color: pink " class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>

                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                              <!-- <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getExtraRebate();?></td> -->

                              <!-- <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getNettPrice();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                              <!-- show , inside value -->
                              <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                              <td style="background-color: pink " class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                              <?php $totalDevComm = $orderDetails[$cntAA]->getTotalDeveloperComm();?>
                              <td style="background-color: pink " class="td"><?php echo $totalDevCommValue = number_format($totalDevComm, 2); ?></td>

                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                              <td style="background-color: pink " class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>

                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getRemark();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                              <!-- <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td> -->
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getLoanAmount();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>

                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>

                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getRate();
                              if ($orderDetails[$cntAA]->getRate()) {
                                echo "%";
                              }?></td>

                              <!-- <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getAgentComm();?></td> -->

                              <?php $agentComm = $orderDetails[$cntAA]->getAgentComm();?>
                              <td style="background-color: pink " class="td"><?php echo $agentCommValue = number_format($agentComm, 2); ?></td>

                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getUpline1();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getUpline2();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getPlName();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getHosName();?></td>
                              <td style="background-color: pink " class="td"><?php echo $orderDetails[$cntAA]->getListerName();?></td>

                              <!-- <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getUlOverride();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getUulOverride();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getPlOverride();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getHosOverride();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getListerOverride();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getAdmin1Override();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getAdmin2Override();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getGicProfit();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getTotalClaimDevAmt();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $orderDetails[$cntAA]->getTotalBalUnclaimAmt();?></td> -->

                              <!-- show , inside value -->
                              <?php $ulOverride = $orderDetails[$cntAA]->getUlOverride();?>
                              <td style="background-color: pink " class="td"><?php echo $ulOverrideValue = number_format($ulOverride, 2); ?></td>
                              <?php $uUlOverride = $orderDetails[$cntAA]->getUulOverride();?>
                              <td style="background-color: pink " class="td"><?php echo $uUlOverrideValue = number_format($uUlOverride, 2); ?></td>
                              <?php $plOverride = $orderDetails[$cntAA]->getPlOverride();?>
                              <td style="background-color: pink " class="td"><?php echo $plOverrideValue = number_format($plOverride, 2); ?></td>
                              <?php $hosOverride = $orderDetails[$cntAA]->getHosOverride();?>
                              <td style="background-color: pink " class="td"><?php echo $hosOverrideValue = number_format($hosOverride, 2); ?></td>
                              <?php $listerOverride = $orderDetails[$cntAA]->getListerOverride();?>
                              <td style="background-color: pink " class="td"><?php echo $listerOverrideValue = number_format($listerOverride, 2); ?></td>
                              <?php $a1Over = $orderDetails[$cntAA]->getAdmin1Override();?>
                              <td style="background-color: pink " class="td"><?php echo $a3OverValue = number_format($a1Over, 2); ?></td>
                              <?php $a2Over = $orderDetails[$cntAA]->getAdmin2Override();?>
                              <td style="background-color: pink " class="td"><?php echo $a2OverValue = number_format($a2Over, 2); ?></td>
                              <?php $a3Over = $orderDetails[$cntAA]->getAdmin3Override();?>
                              <td style="background-color: pink " class="td"><?php echo $a3OverValue = number_format($a3Over, 2); ?></td>
                              <?php $gicProfit = $orderDetails[$cntAA]->getGicProfit();?>
                              <td style="background-color: pink " class="td"><?php echo $gicProfitValue = number_format($gicProfit, 2); ?></td>
                              <?php $totalClaim = $orderDetails[$cntAA]->getTotalClaimDevAmt();?>
                              <td style="background-color: pink " class="td"><?php echo $totalClaimValue = number_format($totalClaim, 2); ?></td>
                              <?php $totalUnclaim = $orderDetails[$cntAA]->getTotalBalUnclaimAmt();?>
                              <td style="background-color: pink " class="td"><?php echo $totalUnclaimValue = number_format($totalUnclaim, 2); ?></td>
                              <td style="background-color: pink " id="dateModified" class="td">
                                <form class="" action="historyIndividual.php" method="post">
                                  <input type="hidden"  id="loanUid" name="loan_uid" value="<?php $orderDetails[$cntAA]->getLoanUid() ?>">
                                  <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($orderDetails[$cntAA]->getDateUpdated()));?></a> </button>
                                  </td>
                                </form>

                              <td style="background-color: pink " class="td">
                                  <form action="editProduct.php" method="POST">
                                      <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                          <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                                          <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                                      </button>
                                  </form>
                              </td>

                          </tr>
                          <?php
                          }}

                        }
                    //}
                    ?>
                </tbody>
            </table><br>


    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
