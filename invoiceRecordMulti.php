<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/MultiInvoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $loanDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getInvoice($conn);
$projectDetails = getProject($conn);
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "WHERE case_status = 'COMPLETED'";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice Record | GIC" />
    <title>Invoice Record-Multi | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Invoice Record-Multi</h1>
    <div class="short-red-border"></div>
    <h3 class="h1-title"> <a style="color: maroon" href="invoiceRecord2.php" class="h1-title">Single Invoice</a> | Multi Invoice</h3>
    <!-- This is a filter for the table result -->
	<div class="clean"></div>
  <div class="float-right section-divider mobile-100">

    <form class="" action="selected.php" method="post">
      <select id="sel_id" name="invoiceRecord2"  onchange="this.form.submit();" class="clean-select">
        <?php if (isset($_GET['name'])) {
          if ($_GET['name'] == 'SHOW ALL') {
            $projectName = "WHERE case_status = 'COMPLETED'";
          }else {
            $type = $_GET['name'];
            $types = urldecode("$type");
            $projectName = "WHERE project_name = '$types'";
          }
          ?><option value=""><?php echo $_GET['name'] ?></option>
          <option value="">Choose Project</option><?php
        }else {
          ?><option value="">Choose Project</option><?php
        } ?>

        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="SHOW ALL">SHOW ALL</option><?php
            } ?>
      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
    </form>

      <?php
          if (isset($_POST['sel_name']))
          {
              $projectName =  $_POST['sel_name'];
          }
          else
          {
              // echo "string";
          }
      ?>


  </div>




    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                        <th class="th">DATE</th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?></th>
                        <th class="th">AMOUNT (RM)</th>
                        <th class="th">SST</th>
                        <th class="th">1ST STATUS</th>
                        <th class="th">RECEIVED DATE</th>
                        <th class="th">CHECK NO.</th>
                        <th class="th">ACTION</th>

                        <!-- <th>STOCK</th> -->


                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $multiInvoiceDetails = getMultiInvoice($conn);

                    if ($multiInvoiceDetails) {
                      for ($cnt=0; $cnt <count($multiInvoiceDetails) ; $cnt++) {
                        $claimStatus = $multiInvoiceDetails[$cnt]->getClaimsStatus();
                        // $claimStatusExplode = explode(",",$claimStatus);
                        ?>
                        <tr>
                        <td class="td"><?php echo $cnt +1; ?>
                        <td class="td"><?php echo $multiInvoiceDetails[$cnt]->getProjectName(); ?></td>
                        <td class="td"><?php echo str_replace(",","<br>",$multiInvoiceDetails[$cnt]->getUnitNo()); ?></td>
                        <td class="td"><?php echo date('d-m-Y', strtotime($multiInvoiceDetails[$cnt]->getBookingDate())); ?></td>
                        <td class="td"><?php echo date('ymd', strtotime($multiInvoiceDetails[$cnt]->getDateCreated())).$multiInvoiceDetails[$cnt]->getID(); ?></td>
                        <td class="td"><?php echo str_replace(",","<br>",$multiInvoiceDetails[$cnt]->getAmount()); ?></td>
                        <td class="td"><?php echo $multiInvoiceDetails[$cnt]->getCharges(); ?></td>
                        <td class="td"><?php echo $multiInvoiceDetails[$cnt]->getReceiveStatus(); ?></td>
                        <td class="td"><?php
                        ?><form class="" action="utilities/checkNoMulti.php" method="post"><?php
                        if (!$multiInvoiceDetails[$cnt]->getReceiveDate()) {
                          ?><input type="date" name="receive_date" value="<?php echo date('Y-m-d'); ?>"> <?php
                        }else {
                           echo date('d-m-Y', strtotime($multiInvoiceDetails[$cnt]->getReceiveDate()));
                        }
                        ?></td>
                        <td class="td">
                          <?php if ($multiInvoiceDetails[$cnt]->getCheckID() && $multiInvoiceDetails[$cnt]->getReceiveDate()) {
                            echo $multiInvoiceDetails[$cnt]->getCheckID();
                            ?><button class="button-remove" type="submit" name="removeCheckID">Edit</button><?php
                          }else {
                            ?><input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $multiInvoiceDetails[$cnt]->getCheckID(); ?>">
                            <button class="button-add" type="submit" name="addCheckID">Submit</button><?php
                          } ?>
                          <?php

                            ?>


                          <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                          <input type="hidden" name="id" value="<?php echo $multiInvoiceDetails[$cnt]->getId() ?>">
                        </form>

                          <td class="td">
                          <form action="invoiceMulti.php" method="POST">
                            <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="id" value="<?php echo $multiInvoiceDetails[$cnt]->getId();?>">Print Invoice
                              <input type="hidden" name="claim_status" value="1">
                              <input type="hidden" name="invoice_no" value="<?php echo date('ymd', strtotime($multiInvoiceDetails[$cnt]->getDateCreated())).$multiInvoiceDetails[$cnt]->getID(); ?>">
                              <input type="hidden" name="booking_date" value="<?php echo date('d-m-Y', strtotime($multiInvoiceDetails[$cnt]->getBookingDate())); ?>">
                                  <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                  <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                              </button></a>
                          </form> </td>
                      </tr> <?php
                      }
                    }


                    ?>
                </tbody>
            </table><br>


    </div>

    <!-- <div class="three-btn-container">
    <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
      <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
    <!-- </div> -->
    <?php $conn->close();?>

</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Edit Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<style>
      body {

      }
      input {

      font-weight: bold;
      /* color: white; */
      text-align: center;
      border-radius: 15px;
      }
      .no-outline:focus {
      outline: none;

      }
      button{
        border-radius: 15px;
        color: white;

      }
      .button-add{
        background-color: green;
      }
      .button-remove{
        background-color: maroon;
      }
    </style>
</body>
</html>
