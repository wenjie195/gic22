<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$agentList = getUser($conn, "WHERE user_type = 3");
$paymentList = getPaymentList($conn);
$bankNameList = getBankName($conn);
$projectList = getProject($conn, "WHERE display = 'Yes'");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add | GIC" />
    <title>Add New Agent | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<div class="yellow-body padding-from-menu same-padding">
<h1 class="username">Add New Agent</h1>

  <form  action="utilities/addAgentFunction.php" method="POST" enctype="multipart/form-data">

      <div class="dual-input-div">
        <p>Full Name <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder="Full Name" id="full_name" name="full_name" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p>Username <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder=" Username" id="username" name="username"  >
      </div>

      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p>IC No. <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder=" IC No." id="ic_no" name="ic_no" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p>Contact <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder=" Contact No." id="contact" name="contact"  >
      </div>

      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p>E-mail <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder=" E-mail" id="email" name="email" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p>Birth Month <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder="Birth Month" id="birth_month" name="birth_month"  required>
      </div>

      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p>Bank Name <a>*</a></p>
        <select class="dual-input clean" name="bank_name">
        <?php    for ($cnt=0; $cnt <count($bankNameList) ; $cnt++) {
              ?>
              <option value="">Select a Option</option>
              <option class="dual-input clean" value="<?php echo $bankNameList[$cnt]->getBankName() ?>"><?php echo $bankNameList[$cnt]->getBankName() ?></option><?php
            } ?>
        </select>
      </div>
      <div class="dual-input-div second-dual-input">
        <p>Bank No. <a>*</a></p>
        <input class="dual-input clean" type="number" placeholder="Bank No." id="bank_no" name="bank_no"  required>
      </div>

      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p>Address</p>
        <input class="dual-input clean" type="text" placeholder="Address" id="address" name="address" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p>Upline1</p>
        <select class="dual-input clean" name="upline1">
        <option value="">Select a Option</option>
            <?php
            for ($cnt=0; $cnt <count($agentList) ; $cnt++) {
              ?>
              <option class="dual-input clean" value="<?php echo $agentList[$cnt]->getUsername() ?>"><?php echo $agentList[$cnt]->getUsername() ?></option><?php
            } ?>
        </select>
      </div>

      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p>Upline2</p>
        <select class="dual-input clean" name="upline2">
        <option value="">Select a Option</option>
            <?php
            for ($cnt=0; $cnt <count($agentList) ; $cnt++) {
              ?>
              <option class="dual-input clean" value="<?php echo $agentList[$cnt]->getUsername() ?>"><?php echo $agentList[$cnt]->getUsername() ?></option><?php
            } ?>
        </select>
      </div>
      <div class="dual-input-div second-dual-input">
        <p>Status</p>
        <select class="dual-input clean" name="status">
            <option value="">Select a Option</option>
            <option value="Active">Active</option>
            <option value="Resign">Resign</option>
        </select>
      </div>

      <div class="clear"></div>
      <div class="tempo-two-input-clear"></div>
      <div class="three-btn-container extra-margin-top">
          <!-- <button class="shipout-btn-a red-button three-btn-a" type="submit" id = "deleteProduct" name = "deleteProduct" ><b>DELETE</b></a></button> -->
          <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "editSubmit" name = "editSubmit" ><b>CONFIRM</b></a></button>
      </div>
    </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Product Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "There is an error to add the new product";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(document).ready(function(){
  var i = 0;
  $("#addBtn").click(function(){
    i++;
    var div = ('<div id="el'+i+'"><div class="dual-input-div"><p>Purchaser Name</p><input class="dual-input clean" type="text" placeholder="Purchaser Name" name="purchaser_name[]" required></div><div class="dual-input-div second-dual-input"><p>Contact</p><input class="dual-input clean" type="text" placeholder="Contact" id="contact" name="contact[]" required></div><div class="tempo-two-input-clear"></div><div class="dual-input-div"><p>IC</p><input class="dual-input clean" type="text" placeholder="IC" id="ic" name="ic[]" required></div><div class="dual-input-div second-dual-input"><p>E-mail</p><input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email[]"></div></div>');

    $(div).hide().appendTo("#addIn").slideDown(1000);
  });
  $("#remBtn").click(function(){
    if ($('#el'+i+' input').length > 1) {
                $('#el'+i+'').slideUp(1000, function(){
                  $(this).remove();
                });
                i--;
            }
  });

  $("#selectAmountType").change( function(){
    var type = $(this).val();

    if (type == '%') {
      $("#loanAmountName").text("Loan Amount (%)");
      $('input[name="loan_amount"]').prop('readonly', false);
    }else if (type == 'RM') {
      $("#loanAmountName").text("Loan Amount (RM)");
      $('input[name="loan_amount"]').prop('readonly', false);
    }
  });

});
</script>
</body>
</html>
