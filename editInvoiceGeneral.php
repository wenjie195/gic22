<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$projectList = getProject($conn);
if (isset($_POST['product_name'])) {
$loanDetails = getLoanStatus($conn, "WHERE project_name = ?", array("project_name"), array($_POST['product_name']), "s");
}
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Invoice | GIC" />
    <title>Edit Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
a{
  color: red;
}

</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">



    <h1 id="invoiceTo" class="username">Invoice : </h1>

    <div class="three-input-div dual-input-div">
    <p>Project <a>*</a> </p>
    <form class="" action="utilities/addNewInvoiceGeneralFunction.php" method="POST">
      <select class="dual-input clean" placeholder="Project" id="product_name" name="project_name">
        <option value="">Select a Project</option>
        <?php for ($cntPro=0; $cntPro <count($projectList) ; $cntPro++)
        {
        ?>
        <option value="<?php echo $projectList[$cntPro]->getProjectName(); ?>">
        <?php echo $projectList[$cntPro]->getProjectName(); ?>
        </option>
        <?php
        }
        ?>
      </select>
    <!-- </form> -->

    </div>

  <div class="three-input-div dual-input-div second-three-input">
    <p>To</p>
    <input required id="toWho" class="dual-input clean" type="text" placeholder="Auto Generated" readonly>
  </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->

    <div class="three-input-div dual-input-div">
      <p>Project Total Claims</p>
      <!-- <select class="dual-input clean" id="claimsNo" name="claims_no" >
        <option value="">Select an Unit</option>
      </select> -->
      <input class="dual-input clean" id="claimsNo" type="text" name="claims_no" value="" placeholder="auto generated" readonly>
    </div>

    <div class="tempo-two-input-clear"></div>

    <!-- <div class="dual-input-div second-dual-input"> -->
    <div class="three-input-div dual-input-div">
      <p>Date</p>
      <input class="dual-input clean" type="date" id="date_of_claims" name="date_of_claims" value="<?php echo date('Y-m-d') ?>" required>
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="three-input-div dual-input-div second-three-input">
      <p>Invoice <a>*</a></p>
      <select class="dual-input clean" name="invoice_name" >
        <option value="">Please Select an Option</option>
        <option value="Proforma">Proforma</option>
        <option value="Invoice">Invoice</option>
      </select>
    </div>
    <!-- <div class="dual-input-div second-dual-input"> -->
    <div class="three-input-div dual-input-div">
      <p>Invoice Type <a>*</a></p>
      <select class="dual-input clean" name="invoice_type" >
        <option value="">Please Select an Option</option>
        <option value="">none</option>
        <option value="Others">Others</option>
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a><img style="cursor: pointer" id="remBtn" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn" width="13px" align="right" src="img/ppp.png"></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" placeholder="Amount (RM)" id="amount" value="" name="amount[]">
  </div>

  <div class="three-input-div dual-input-div">
    <p>Status</p>
    <input class="dual-input clean" type="text" id="status" name="" placeholder="Status" value="">
  </div>
  <div id="input1" style="display: none" >
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit1" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" placeholder="Amount (RM)" id="amount1" value="" name="amount[]">
  </div>

  <div class="three-input-div dual-input-div">
    <p>Status</p>
    <input class="dual-input clean" type="text" id="status1" name="" placeholder="Status" value="" readonly>
  </div>
  </div>

<div id="input2" style="display: none">
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit2" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" placeholder="Amount (RM)" id="amount2" value="" name="amount[]">
  </div>

  <div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status2" name="" placeholder="Status" value="" readonly>
  </div>
</div>

<div id="input3" style="display: none">
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit3" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" placeholder="Amount (RM)" id="amount3" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status3" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input4" style="display: none">
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit4" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" placeholder="Amount (RM)" id="amount4" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status4" name="" placeholder="Status" value="" readonly>
</div>
</div>
  <!-- <p id="addIn"></p> -->

    <div class="dual-input-div">
      <p>Include Service Tax (6%) <a>*</a></p>
      <input style="cursor: pointer" required class="" type="radio" value = "YES" name="charges" >Yes
      <input style="cursor: pointer" required class="" type="radio" value = "NO" name="charges" >No
    </div>

    <div class="tempo-two-input-clear"></div>

    <button input type="submit" name="upload" value="Upload" class="confirm-btn text-center white-text clean black-button">Confirm</button>

  </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<script type="text/javascript">
$(document).ready(function(){
var a = 0;
$("#remBtn").hide();
  $("#addBtn").click( function(){
    a++;
    if (a == 1) {
      $("#input1").fadeIn(500);
      $("#remBtn").show();
    }
    if (a == 2) {
      $("#input2").fadeIn(500);
    }
    if (a == 3) {
      $("#input3").fadeIn(500);
    }
    if (a == 4) {
      $("#input4").fadeIn(500);
    }
    if (a > 3) {
      $("#addBtn").hide();
      $("#remBtn").show();
    }

  });
  // var b = 0;
  $("#remBtn").click( function(){
    a--;
    if (a < 0) {
      a = 1;
    }
    if (a == 0) {
      $("#input1").fadeOut(500);
      $("#addBtn").show();
      $("#remBtn").hide();
    }
    if (a == 1) {
      $("#input2").fadeOut(500);
    }
    if (a == 2) {
      $("#input3").fadeOut(500);
    }
    if (a == 3) {
      $("#input4").fadeOut(500);
      $("#addBtn").show();
    }

  });

  $("#product_name").change(function(){
      var productName = $(this).val();

      $.ajax({
          url: 'getLoanDetails.php',
          type: 'post',
          data: {projectName:productName},
          dataType: 'json',
          success:function(response){

              var len = response.length;

              // $("#unit").empty(); // will remove data before this
              for( var i = 0; i<len; i++){
                  // var id = response[i]['id'];
                  // var name = response[i]['name'];
                  var unit = response[i]['unit_no'];

                  $("#unit").append("<option value='"+unit+"'>"+unit+"</option>");
                  $("#unit1").append("<option value='"+unit+"'>"+unit+"</option>");
                  $("#unit2").append("<option value='"+unit+"'>"+unit+"</option>");
                  $("#unit3").append("<option value='"+unit+"'>"+unit+"</option>");
                  $("#unit4").append("<option value='"+unit+"'>"+unit+"</option>");

              }

              $("#claimsNo").empty();
              // for( var j = 0; j<len; j++){
                  // var id = response[i]['id'];
                  // var name = response[i]['name'];
                  var claims = response[0]['project_claims'];

                  // $("#claimsNo").append("<option value='"+claims+"'>"+claims+"</option>");
                  $("#claimsNo").val(claims);

                  $("#invoiceTo").empty();

                    var invoice = response[0]['add_projectppl'];

                      // $("#claimsNo").append("<option value='"+claims+"'>"+claims+"</option>");
                      $("#invoiceTo").text("Invoice : "+invoice);
                        $("#toWho").val(invoice);
          }
      });
  });

});
</script>
<script>
$("#unit").change(function(){
    var unitNo = $(this).val();

    $.ajax({
        url: 'getTotal.php',
        type: 'post',
        data: {unitt:unitNo},
        dataType: 'json',
        success:function(response){

            var len = response.length;

            $("#amount").empty();

                var tot = response[0]['totaldevelopercomm'];

                $("#amount").val(tot);

                $("#status").empty();

                    var tot = response[0]['claim_status'];

                    $("#status").val(tot);
            // }
        }
    });
});
$("#unit1").change(function(){
    var unitNo = $(this).val();

    $.ajax({
        url: 'getTotal.php',
        type: 'post',
        data: {unitt:unitNo},
        dataType: 'json',
        success:function(response){

            // var len = response.length;

            $("#amount1").empty();

                var tot = response[0]['totaldevelopercomm'];

                $("#amount1").val(tot);

                $("#status1").empty();

                    var tot = response[0]['claim_status'];

                    $("#status1").val(tot);
            // }
        }
    });
});
$("#unit2").change(function(){
    var unitNo = $(this).val();

    $.ajax({
        url: 'getTotal.php',
        type: 'post',
        data: {unitt:unitNo},
        dataType: 'json',
        success:function(response){

            // var len = response.length;

            $("#amount2").empty();

                var tot = response[0]['totaldevelopercomm'];

                $("#amount2").val(tot);

                $("#status2").empty();

                    var tot = response[0]['claim_status'];

                    $("#status2").val(tot);
            // }
        }
    });
});
$("#unit3").change(function(){
    var unitNo = $(this).val();

    $.ajax({
        url: 'getTotal.php',
        type: 'post',
        data: {unitt:unitNo},
        dataType: 'json',
        success:function(response){

            // var len = response.length;

            $("#amount3").empty();

                var tot = response[0]['totaldevelopercomm'];

                $("#amount3").val(tot);

                $("#status3").empty();

                    var tot = response[0]['claim_status'];

                    $("#status3").val(tot);
            // }
        }
    });
});
$("#unit4").change(function(){
    var unitNo = $(this).val();

    $.ajax({
        url: 'getTotal.php',
        type: 'post',
        data: {unitt:unitNo},
        dataType: 'json',
        success:function(response){

            // var len = response.length;

            $("#amount4").empty();

                var tot = response[0]['totaldevelopercomm'];

                $("#amount4").val(tot);

                $("#status4").empty();

                    var tot = response[0]['claim_status'];

                    $("#status4").val(tot);
            // }
        }
    });
});
// $("select").change( function(){
//   $("select").not(this).find("option[value="+ $(this).val()+"]").attr('disabled', true);
// });
</script>
</body>
</html>
