<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    header('Location: ./viewCart.php');
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$asd = $userDetails->getBankAccountNo();

$products = getProduct($conn);

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($products,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($products);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/product.php" />
<meta property="og:title" content="DCK® Engine Oil Booster | DCK Supreme" />
<title>DCK® Engine Oil Booster | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/product.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>-->

<!-- Start Menu -->

<div class="yellow-body padding-from-menu same-padding">
	<?php include 'header-sherry.php'; ?>
    <!-- <img src="img/back.png" class="back-btn" alt="back" title="back"  onclick="goBack()"> -->

    <?php
        if($asd == "")
        {
        ?>

            <div class="edit-profile-div2">
                <h3>Please Update Your Bank Details Before Making Any Purchase !</h3>
                <a href="editProfile.php" >
                    <button class="confirm-btn text-center white-text clean black-button">Click Here !</button>
                </a>
            </div>
            
        <?php
        }
        else
        { ?>
           
           <form method="POST">
                <?php echo $productListHtml; ?>
                <!-- <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button> -->
                <!-- <button class="clean black-button add-to-cart-btn">ADD TO CART</button> -->

                <div class="cart-bottom-div">
                    <div class="left-cart-bottom-div">
                        <!-- <p class="continue-shopping pointer"  onclick="goBack()"><img src="img/back.png" class="back-btn" alt="back" title="back" > Continue Shopping</p> -->
                    </div>
                            <!-- <td>clea  ADD CLEAR CART</td> -->
                    <div class="right-cart-div">
                        <button class="clean black-button add-to-cart-btn checkout-btn">Add To Cart</button>
                    </div>    
                </div>

            </form>

        <?php
        }
        ?>
    
</div>
<script>

function goBack() {
  window.history.back();
}
</script>

<?php include 'js.php'; ?>

</body>
</html>