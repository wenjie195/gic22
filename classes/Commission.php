<?php
class Commission {
    /* Member variables */
    // var $id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated;
    var $id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $projectName, $unitNo, $bookingDate, $dateCreated, $details, $dateUpdated;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $id
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }

    /**
     * @return mixed
     */
    public function getCheckID()
    {
        return $this->check_id;
    }

    /**
     * @param mixed $id
     */
    public function setCheckID($checkID)
    {
        $this->check_id = $checkID;
    }

    /**
     * @return mixed
     */
    public function getReceiveStatus()
    {
        return $this->receive_status;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveStatus($receiveStatus)
    {
        $this->receive_status = $receiveStatus;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $id
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getUpline()
    {
        return $this->upline;
    }

    /**
     * @param mixed $id
     */
    public function setUpline($upline)
    {
        $this->upline = $upline;
    }

    /**
     * @return mixed
     */
    public function getPurchaserName()
    {
        return $this->purchaser_name;
    }

    /**
     * @param mixed $id
     */
    public function setPurchaserName($purchaserName)
    {
        $this->purchaser_name = $purchaserName;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
    return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
    $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unitNo;
    }

    /**
     * @param mixed $unitNo
     */
    public function setUnitNo($unitNo)
    {
        $this->unitNo = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getLoanUid()
    {
        return $this->loan_uid;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setLoanUid($loanUid)
    {
        $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;
    }

}

function getCommission($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","loan_uid","upline","commission","purchaser_name", "check_id", "receive_status", "date_created");
    $dbColumnNames = array("id","loan_uid","upline","commission","purchaser_name", "check_id", "receive_status", "project_name", "unit_no", "booking_date", "date_created","details","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"commission");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated);
        $stmt->bind_result($id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $projectName, $unitNo, $bookingDate, $dateCreated, $details, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Commission();
            $class->setID($id);
            $class->setLoanUid($loanUid);
            $class->setUpline($upline);
            $class->setCommission($commission);
            $class->setPurchaserName($purchaserName);
            $class->setCheckID($checkID);
            $class->setReceiveStatus($receiveStatus);

            $class->setProjectName($projectName);
            $class->setUnitNo($unitNo);
            $class->setBookingDate($bookingDate);
            $class->setDetails($details);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
