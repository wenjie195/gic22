<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();
$post = array();
$start_date = date("Y-m-d");
$end_date = date("Y-m-d");
$username = "";
$fullname = "";
$registerDownlineNo  = "";
$bonus  = "";

$record_per_page = 15;
$page = '';
if(isset($_GET["page"]))
{
 $page = $_GET["page"];
}
else
{
 $page = 1;
}

$start_from = ($page-1)*$record_per_page;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

if (isset($_GET["start_date"]) && isset($_GET["end_date"]) && isset($_GET["username"]) && isset($_GET["full_name"]))
{
	$start_date = $_GET["start_date"];
	$end_date = $_GET["end_date"];
	$username = $_GET["username"];
  $fullname = $_GET["full_name"];
  $registerDownlineNo = $_GET["register_downline_no"];
  $bonus = $_GET["bonus"];
	$post = $_GET;
}

$products = getProduct($conn);
$list = GetList($post, $conn,$start_from,$record_per_page);

//echo json_encode($list);//exit;

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

//$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function GetList($post, $conn,$start_from,$record_per_page)
{
	$sql = "SELECT full_name,date_created,bonus, ";
	$sql .= "username, register_downline_no ";
  $sql .= "FROM user ";
	$sql .= "WHERE user_type = '1' ";

	if (isset($post["reset"])){
	if (isset($post["start_date"]) && strlen($post["start_date"]) < 0)
	{
		$sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
	}

	if (isset($post["end_date"]) && strlen($post["end_date"]) < 0)
	{
		$sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
	}

	if (isset($post["username"]) && strlen($post["username"]) < 0)
	{
    $sql .= "AND username LIKE '%" . $post["username"] . "%' ";
	}
  if (isset($post["full_name"]) && strlen($post["full_name"]) < 0)
	{
    $sql .= "AND full_name LIKE '%" . $post["full_name"] . "%' ";
	}
  if (isset($post["register_downline_no"]) && strlen($post["register_downline_no"]) < 0)
	{
    $sql .= "AND register_downline_no LIKE '%" . $post["register_downline_no"] . "%' ";
	}
  if (isset($post["bonus"]) && strlen($post["bonus"]) < 0)
	{
    $sql .= "AND bonus LIKE '%" . $post["bonus"] . "%' ";
	}
}else {
  if (isset($post["start_date"]))
  {
    $sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
  }

  if (isset($post["end_date"]))
  {
    $sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
  }

  if (isset($post["username"]) && strlen($post["username"]) > 0)
  {
    $sql .= "AND username LIKE '%" . $post["username"] . "%' ";
  }
  if (isset($post["full_name"]) && strlen($post["full_name"]) > 0)
  {
    $sql .= "AND full_name LIKE '%" . $post["full_name"] . "%' ";
  }
  if (isset($post["register_downline_no"]) && strlen($post["register_downline_no"]) > 0)
  {
    $sql .= "AND register_downline_no LIKE '%" . $post["register_downline_no"] . "%' ";
  }
  if (isset($post["bonus"]) && strlen($post["bonus"]) > 0)
  {
    $sql .= "AND bonus LIKE '%" . $post["bonus"] . "%' ";
  }
}

	$sql .= "ORDER BY date_created DESC LIMIT $start_from, $record_per_page";
	//echo $sql;exit;

	$result = $conn->query($sql);
	$output = array();

	if ($result->num_rows > 0)
	{
		// output data of each row
		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}
	}

	return $output;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminMember.php" />
    <meta property="og:title" content="Member | DCK Supreme" />
    <title>Member | DCK Supreme</title>
    <meta property="og:description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminMember.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
  <!-- <h1 class="h1-title h1-before-border shipping-h1">Member</h1> -->
  <h1 class="h1-title h1-before-border shipping-h1"><?php echo _MAINJS_ADMMEM_MEMBER ?></h1>
    <div class="clear"></div>
	<div class="search-container0 payout-search">
		<form action="adminMember.php" type="post">
            <div class="shipping-input clean smaller-text2">
                <!-- <p>Username</p> -->
                <p><?php echo _MAINJS_ADMMEM_USERNAME ?></p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="username" placeholder="Username"  value="<?php echo $username; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <!-- <p>Full Name</p> -->
                <p><?php echo _MAINJS_ADMMEM_FULLNAME ?></p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="full_name" placeholder="Full Name"  value="<?php echo $fullname; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <!-- <p>Start Date</p> -->
                <p><?php echo _MAINJS_ADMMEM_START_DATE ?></p>
                <input class="shipping-input2 clean normal-input" name="start_date" type="date" value="<?php echo $start_date; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <!-- <p>End Date</p> -->
                <p><?php echo _MAINJS_ADMMEM_END_DATE ?></p>
                <input class="shipping-input2 clean normal-input" name="end_date" type="date" value="<?php echo $end_date; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <!-- <p>Total Downline Number</p> -->
                <p><?php echo _MAINJS_ADMMEM_TOT_DOWNLINE_NO ?></p>
                <input class="shipping-input2 clean normal-input" name="register_downline_no" type="number" placeholder="Total Downline Number" value="<?php echo $registerDownlineNo; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <!-- <p>Bonus</p> -->
                <p><?php echo _MAINJS_ADMMEM_BONUS ?></p>
                <input class="shipping-input2 clean normal-input" name="bonus" type="number" placeholder="Bonus" value="<?php echo $bonus; ?>">
            </div>
            <!-- <button type="submit" class="clean black-button shipping-search-btn second-shipping same-height-with-date2">Search</button>
            <button type="submit" name="reset" class="clean black-button shipping-search-btn second-shipping same-height-with-date2">Reset</button> -->
            <button type="submit" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 ow-shipping-btn1"><?php echo _MAINJS_ADMMEM_SEARCH ?></button>
            <button type="submit" name="reset" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 ow-shipping-btn1"><?php echo _MAINJS_ADMMEM_RESET ?></button>
			</form>
    </div>

    <!-- <div class="clear"></div> -->

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
              <thead>
                  <!-- <tr>
                      <th>NO.</th>
                      <th>USERNAME</th>
                      <th>FULLNAME</th>
                      <th>JOINED DATE</th>
                      <th>TOTAL DOWNLINE NO.</th>
                      <th>SPONSOR BONUS</th>
                  </tr> -->
                  <tr>
                      <th>NO.</th>
                      <th><?php echo _MAINJS_ADMMEM_USERNAME_2 ?></th>
                      <th><?php echo _MAINJS_ADMMEM_FULLNAME_2 ?></th>
                      <th><?php echo _MAINJS_ADMMEM_JOINED_DATE ?></th>
                      <th><?php echo _MAINJS_ADMMEM_TOT_DOWNLINE_NO_2 ?></th>
                      <th><?php echo _MAINJS_ADMMEM_SPONSOR_BONUS ?></th>
                  </tr>
              </thead>
                <tbody>
								<?php if ($list):
                  $ind=0; ?>
									<?php foreach ($list AS $ls):
                    $ind++; ?>
										<tr>
											<td><?php echo $ind; ?></td>
											<td><?php echo $ls["username"]; ?></td>
                      <td><?php echo $ls["full_name"]; ?></td>
                      <!-- <td><?php// echo $ls["date_created"]; ?></td> -->
                      <td><?php echo date("Y-m-d", strtotime($ls["date_created"])); ?></td>
											<td><?php echo $ls["register_downline_no"]; ?></td>
                      <td><?php echo $ls["bonus"]; ?></td>
										</tr>
									<?php endforeach; ?>
								<?php else: ?>
									<tr>
										<td colspan="6">No result</td>
								</tr>
								<?php endif; ?>
                  </tbody>
            </table>
        </div>
    </div>
    <div class="clear"></div><br>

    <div class="pagination">
      <div class="width100 text-center">
              <ul>
    <?php
    $page_query = "SELECT * FROM user WHERE user_type = 1 ORDER BY date_created ASC";
    $page_result = mysqli_query($conn, $page_query);
    $total_records = mysqli_num_rows($page_result);
    $total_pages = ceil($total_records/$record_per_page);
    $start_loop = $page;
    $difference = $total_pages - $page;
    if($difference <= 5)
    {
     $start_loop = $total_pages - 5;
    }
    $end_loop = $start_loop + 4;
    if($page > 1)
    {
     echo "<a href='adminMember.php?page=1'><li>First</li></a>";
     echo "<a href='adminMember.php?page=".($page - 1)."'><li><<</li></a>";
    }
    for($i=1; $i<=$total_pages; $i++)
    {
        echo "<a href='adminMember.php?page=".$i."'><li>".$i."</li></a>";
    }
    if($page <= $end_loop)
    {
     echo "<a href='adminMember.php?page=".($page + 1)."'><li>>></li></a>";
     echo "<a href='adminMember.php?page=".$total_pages."'><li>Last</li></a>";
    }


    ?>
          </ul>
    </div>
    </div>
    <div class="clear"></div>
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<style>
.pagination {
  	display: inline-block;
	text-align: center;
	width:100%;
}

.pagination li {
  color: white;
  padding: 8px 16px;
  text-decoration: none;
	text-align: center;
  transition:ease-in-out 0.15s;;
  border: 1px solid #000000;
  margin: 0 4px;
	background-color: #000000;
	display:inline-block;
}
.pagination a {
	display:inline-block;
	margin-bottom:10px;
}
.pagination a.active {
  background-color: white;
  color: black;
  border: 1px solid white;
}

.pagination a:hover:not(.active) {
	background-color: white;
  	color: black;
  	border: 1px solid white;}
</style>

</body>
</html>
