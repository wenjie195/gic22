<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewProject($conn,$projectName,$addProjectPpl,$claimTimes,$projectLeader)
{
     if(insertDynamicData($conn,"project",array("project_name","add_projectppl","claims_no","project_leader"),
     array($projectName,$addProjectPpl,$claimTimes,$projectLeader),"ssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $id = rewrite($_POST['id']);
    $projectName = rewrite($_POST["project_name"]);
    $addProjectPpl = rewrite($_POST["add_by"]);
    $claimTimes = rewrite($_POST["claims_times"]);
    $projectLeader = rewrite($_POST["project_leader"]);

     //   FOR DEBUGGING
     //    echo $name;
     //    echo $price;
     //    echo $type;
if (isset($_POST['loginButton'])) {
  if(addNewProject($conn,$projectName,$addProjectPpl,$claimTimes,$projectLeader))
  {
       $_SESSION['messageType'] = 1;
       header('Location: ../admin1Product.php?type=4');
       // echo "register success";
       // echo "<script>alert('New Project Created Successfully !');window.location='../admin1Product.php'</script>";
  }

}
if (isset($_POST['editButton'])) {

  $display = rewrite($_POST["display"]);

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($projectName)
  {
      array_push($tableName,"project_name");
      array_push($tableValue,$projectName);
      $stringType .=  "s";
  }
  if($addProjectPpl)
  {
      array_push($tableName,"add_projectppl");
      array_push($tableValue,$addProjectPpl);
      $stringType .=  "s";
  }
  if($claimTimes)
  {
      array_push($tableName,"claims_no");
      array_push($tableValue,$claimTimes);
      $stringType .=  "s";
  }
  if($projectLeader)
  {
      array_push($tableName,"project_leader");
      array_push($tableValue,$projectLeader);
      $stringType .=  "s";
  }
  if($display || !$display)
  {
      array_push($tableName,"display");
      array_push($tableValue,$display);
      $stringType .=  "s";
  }

  array_push($tableValue,$id);
  $stringType .=  "i";
  $withdrawUpdated = updateDynamicData($conn,"project"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
      // $_SESSION['messageType'] = 1;
      header('Location: ../adminAddNewProject.php');
  }

}

}
else
{
    //  header('Location: ../index.php');
}
?>
