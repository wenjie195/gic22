<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();
$id = rewrite($_POST['id']);
$status = 'COMPLETED';

if(isset($_POST['sendToAgent']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($status)
    {
        array_push($tableName,"receive_status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }
  }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"commission"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../adminCommission.php?type=1');
  }
 ?>
