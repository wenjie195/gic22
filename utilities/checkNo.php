<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date", "receive_status","details"),
     array($idd, $purchaserName, $loanUid, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd2, $purchaserName, $loanUid, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  $checkID = rewrite($_POST['check_id']);
  // $loanUid = rewrite($_POST['loan_uid']);
  $id = rewrite($_POST['id']);
  $loanDetails = getLoanStatus($conn, "WHERE id =?", array("id"), array($id), "s");
  $purchaserName = $loanDetails[0]->getPurchaserName();
  $sstCharges = $loanDetails[0]->getCharges();
  $loanUid = $loanDetails[0]->getLoanUid();
  $ulOverride = $loanDetails[0]->getUlOverride();
  $uulOverride = $loanDetails[0]->getUulOverride();
  $upline1 = $loanDetails[0]->getUpline1();
  $upline2 = $loanDetails[0]->getUpline2();
  $plName = $loanDetails[0]->getPlName();
  $plOverride = $loanDetails[0]->getPlOverride();
  $hosName = $loanDetails[0]->getHosName();
  $hosOverride = $loanDetails[0]->getHosOverride();
  $listerName = $loanDetails[0]->getListerName();
  $listerOverride = $loanDetails[0]->getListerOverride();
  $projectName = $loanDetails[0]->getProjectName();
  $unitNo = $loanDetails[0]->getUnitNo();
  $bookingDate = $loanDetails[0]->getBookingDate();
  $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
  $totalBalUnclaimAmt = $loanDetails[0]->getTotalBalUnclaimAmt();
  // $totalClaimedDevAmtRestore = $totalClaimedDevAmt;
  // $totalBalUnclaimAmtRestore = $totalBalUnclaimAmt;
  $projectName = $loanDetails[0]->getProjectName();
  $projectDetails = getProject($conn, "WHERE project_name = ?", array("project_name"), array($projectName), "s");
  $claimStatus = $projectDetails[0]->getProjectClaims();

  $agent = $loanDetails[0]->getAgent();
  $agentComm = $loanDetails[0]->getAgentComm();
  $agentAdvanced = 2000;
  $agentComm -= $agentAdvanced; // minus advanced because agent already claimed

  if ($sstCharges == 'NO') {

    $agentCommFinal = ($agentComm / $claimStatus) / 1.06;
    $ulOverrideFinal = ($ulOverride / $claimStatus) / 1.06;
    $uulOverrideFinal = ($uulOverride / $claimStatus) / 1.06;
    $plOverrideFinal = ($plOverride / $claimStatus) / 1.06;
    $hosOverrideFinal = ($hosOverride / $claimStatus) / 1.06;
    $listerOverrideFinal = ($listerOverride / $claimStatus) / 1.06;

  }elseif ($sstCharges == 'YES') {
    $agentCommFinal = $agentComm / $claimStatus;
    $ulOverrideFinal = $ulOverride / $claimStatus;
    $uulOverrideFinal = $uulOverride / $claimStatus;
    $hosOverrideFinal = $hosOverride / $claimStatus;
    $listerOverrideFinal = $listerOverride / $claimStatus;
  }



  $receiveStatus = 'PENDING';

//===============================================================================================

  if (isset($_POST['removeCheckID1'])) { // for remove button
    // $receiveStatus = 'PENDING';
    $checkID1 = null;
    $receiveDate1 = null;

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    // if(!$checkID1)
    // {
    //     array_push($tableName,"check_no1");
    //     array_push($tableValue,$checkID1);
    //     $stringType .=  "s";
    // }
    if(!$receiveDate1)
    {
        array_push($tableName,"receive_date1");
        array_push($tableValue,$receiveDate1);
        $stringType .=  "s";
    }
    // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
    // {
    //     array_push($tableName,"total_claimed_dev_amt");
    //     array_push($tableValue,$totalClaimedDevAmtRestore);
    //     $stringType .=  "s";
    // }
    // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
    // {
    //     array_push($tableName,"total_bal_unclaim_amt");
    //     array_push($tableValue,$totalBalUnclaimAmtRestore);
    //     $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../invoiceRecord2.php');
    }
  }

//=============================================================================================================================

if (isset($_POST['removeCheckID2'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID2 = null;
  $receiveDate2 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID2)
  // {
  //     array_push($tableName,"check_no2");
  //     array_push($tableValue,$checkID2);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate2)
  {
      array_push($tableName,"receive_date2");
      array_push($tableValue,$receiveDate2);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecord2.php');
  }
}

//=============================================================================================================================

if (isset($_POST['removeCheckID3'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID3 = null;
  $receiveDate3 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID3)
  // {
  //     array_push($tableName,"check_no3");
  //     array_push($tableValue,$checkID3);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate3)
  {
      array_push($tableName,"receive_date3");
      array_push($tableValue,$receiveDate3);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecord2.php');
  }
}

//=============================================================================================================================

if (isset($_POST['removeCheckID4'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID4 = null;
  $receiveDate4 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID4)
  // {
  //     array_push($tableName,"check_no4");
  //     array_push($tableValue,$checkID4);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate4)
  {
      array_push($tableName,"receive_date4");
      array_push($tableValue,$receiveDate4);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecord2.php');
  }
}

//=============================================================================================================================

if (isset($_POST['removeCheckID5'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID5 = null;
  $receiveDate5 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID5)
  // {
  //     array_push($tableName,"check_no5");
  //     array_push($tableValue,$checkID5);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate5)
  {
      array_push($tableName,"receive_date5");
      array_push($tableValue,$receiveDate5);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecord2.php');
  }
}

//=============================================================================================================================

  if (isset($_POST['addCheckID1'])) { // for add button

    $details = "1st Commission";
    $receiveDate1 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no1");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date1");
      array_push($tableValue,$receiveDate1);
      $stringType .=  "s";
    }

    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo1()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../invoiceRecord2.php');
    }

if (!$loanDetails[0]->getCheckNo1()) {

    if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord2.php');
              //echo "register success";
         }

   if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if ($plName && $plOverrideFinal) {
     if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($hosName && $hosOverrideFinal) {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($listerName && $listerOverrideFinal) {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }


}
  }


//=============================================================================================================================

  if (isset($_POST['addCheckID2'])) { // for add button

    $details = "2nd Commission";
    $receiveDate2 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no2");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date2");
      array_push($tableValue,$receiveDate2);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo2() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo2()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo2()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../invoiceRecord2.php');
    }

  if (!$loanDetails[0]->getCheckNo2()) {

    if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord2.php');
              //echo "register success";
         }

   if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if ($plName && $plOverrideFinal) {
     if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($hosName && $hosOverrideFinal) {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($listerName && $listerOverrideFinal) {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
  }
}


//=============================================================================================================================

  if (isset($_POST['addCheckID3'])) { // for add button

    $details = "3rd Commission";
    $receiveDate3 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no3");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date3");
      array_push($tableValue,$receiveDate3);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo3() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo3()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo3()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../invoiceRecord2.php');
    }

    if (!$loanDetails[0]->getCheckNo3()) {

    if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord2.php');
              //echo "register success";
         }

   if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDat, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if ($plName && $plOverrideFinal) {
     if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($hosName && $hosOverrideFinal) {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($listerName && $listerOverrideFinal) {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
  }
}

//=============================================================================================================================

  if (isset($_POST['addCheckID4'])) { // for add button

    $details = "4th Commission";
    $receiveDate4 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no4");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date4");
      array_push($tableValue,$receiveDate4);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo4() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo4()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo4()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../invoiceRecord2.php');
    }

    if (!$loanDetails[0]->getCheckNo4()) {

    if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord2.php');
              //echo "register success";
         }

   if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if ($plName && $plOverrideFinal) {
     if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($hosName && $hosOverrideFinal) {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($listerName && $listerOverrideFinal) {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
  }
}

//=============================================================================================================================

  if (isset($_POST['addCheckID5'])) { // for add button

    $details = "5th Commission";
    $receiveDate5 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no5");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date5");
      array_push($tableValue,$receiveDate5);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo5() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo5()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo5()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../invoiceRecord2.php');
    }

    if (!$loanDetails[0]->getCheckNo5()) {

    if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord2.php');
              //echo "register success";
         }

   if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
   {
     header('Location: ../invoiceRecord2.php');
   }
   if ($plName && $plOverrideFinal) {
     if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($hosName && $hosOverrideFinal) {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
   if ($listerName && $listerOverrideFinal) {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecord2.php');
     }
   }
  }
}
}
