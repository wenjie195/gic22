<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE id = ? ",array("id"),array($uid),"s");
$userDetails = $userRows[0];

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add Project | GIC" />
    <title>Add Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: red;
  }
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Add New Project</h1>
    <div class="short-red-border"></div>

    <form method="POST" action="utilities/addProjectFunction.php">
        <label class="labelSize">Project Name : <a>*</a></label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Project Name" name="project_name" id="project_name"><br>

        <label class="labelSize">Person Incharge:</label>
        <input  class="inputSize input-pattern" type="text" placeholder="Person Incharge" name="add_by" id="add_by"><br>

        <label class="labelSize">Project Leader: <a>*</a></label>
        <input class="inputSize input-pattern" type="text" placeholder="Project Leader" name="project_leader" id="claims_times"><br>

        <label class="labelSize">No. of Claim Stage: <a>*</a></label>
        <input class="inputSize input-pattern" type="number" placeholder="numbers of claims stages" name="claims_times" id="claims_times"><br>

        <!-- <input type="hidden" name="add_by" id="add_by" value="<?php //echo $userDetails->getUsername(); ?>"> -->

        <button class="button" type="submit" name="loginButton">Add Project</button><br>
    </form>

<?php $projectDetails = getProject($conn); ?>
<?php if ($projectDetails) {

 ?>
    <h2>Project List</h2>

    <table class="shipping-table">
    <tr>
        <th>No.</th>
        <th>Project Name</th>
        <th>Person In Charge (PIC)</th>
        <th>Project Leader</th>
        <th>Project Claim</th>
        <th>Display</th>
        <th>Date Created</th>
        <th>Date Updated</th>
        <th>Action</th>
        <!-- <th>Invoice</th> -->
    </tr>
    <?php for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {

     ?>
    <tr>
    <?php
    ?>  <td class="td"><?php echo $cnt+1 ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectName() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getAddProjectPpl() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectLeader() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectClaims() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getDisplay() ?></td>
        <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateCreated())) ?></td>
        <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateUpdated())) ?></td>
        <td class="td">  <form action="editProject.php" method="POST">
              <button class="clean edit-anc-btn hover1" type="submit" name="project_id" value="<?php echo $projectDetails[$cnt]->getId();?>">
                  <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                  <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
              </button>
          </form></td>
    </tr>
    <?php
     } ?>
    </table>
<?php } ?>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
</body>
</html>
